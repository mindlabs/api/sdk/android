Mind Android SDK is an Android library that includes everything you need to add
[Mind API-powered](https://api.mind.com) real-time voice, video and text communication to your Android-applications.

* [Introduction](#introduction)
* [Supported Devices](#supported-devices)
* [Example](#example)
* [Getting the SDK](#getting-the-sdk)
* [API Reference](#api-reference)

# Introduction

[Mind API](https://api.mind.com) is a cloud media platform (aka PaaS) which allows you to create applications for
conducting online meeting where participants can communicate to each other in real-time. A meeting in Mind API is
called a conference. Anyone who is participating in a conference using participant token is called a participant.
Usually, it is either a client part of your application (based on Mind Web/Android/iOS SDK) or SIP/H.323-enabled phone.
The responsibility for arranging/ending conferences and inviting/expelling participants into/from them lies with a
server part of your application which interacts with Mind API using application token. Also, the application token
allows the server part of your application to join any conference it created and stay informed of all actions of the
participants.

To communicate to each other participants use text, audio and video. Text is transmitted as messages each of which is
just a string, while audio and video are transmitted as media streams, each of which can contain one audio and one
video. Any participant (if permitted) can send two independent media streams simultaneously: primary and secondary. The
primary media stream is intended for sending video and audio taken from a camera and a microphone, respectively,
whereas the secondary media stream can be used for sending an additional audio/video content (e.g. screen sharing).
Depending on their capabilities and desires participants can receive video/audio which are sent by other participants
in SFU or MCU mode. The SFU mode assumes that primary and secondary media streams of all participant are received as is
— each media stream separately, while in MCU mode audio and video of all participants are mixed inside Mind API
(according to the desired layout) and received as a single media stream.

Mind Android SDK allows Android-applications to [join](../5.12.0/reference/MindSDK.md#static-join2uri-token-listener)
conferences on behalf of participants only. A successful joining attempt results in getting an object of
[Session](../5.12.0/reference/Session.md) class, which represents a participation session. The conference (which
Android-application is participating in) is represented with an object of
[Conference](../5.12.0/reference/Conference.md) class, whereas participants can be represented as objects of either
[Participant](../5.12.0/reference/Participant.md) or [Me](../5.12.0/reference/Me.md) class. The latter is used for
representing the participant on behalf of whom Android-application is participating in the conference (aka the local
participant), the former — for all other participants (aka the remote participants). Messages are represented with
standard [String](https://developer.android.com/reference/java/lang/String) objects, whereas media streams are
represented with instances of [MediaStream](../5.12.0/reference/MediaStream.md) class.

# Supported Devices

Our SDK is always compatible with the latest stable version of Android, but older versions of Android — down to
Android 5.0 (API level 21), inclusively — are supported as well. Mind Android SDK can run on devices with ARM, ARM64,
x86, and x86-64 architectures and on the Android Emulator.

# Example

To demonstrate what you can do with Mind Android SDK, we created a simple open-source Android-application for video
conferencing named [Confy](https://gitlab.com/mindlabs/api/demo/android). Here is a summary of it which shows how to
participate in conference in MCU mode:

```java
public class ConferenceActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MindSDKOptions mindSdkOptions = new MindSDKOptions(this);
        MindSDK.initialize(mindSdkOptions);
        DeviceRegistry deviceRegistry = MindSDK.getDeviceRegistry();
        Microphone microphone = deviceRegistry.getMicrophone();
        Camera camera = deviceRegistry.getCamera();
        SessionOptions options = new SessionOptions();
        MindSDK.join2("<CONFERENCE_URI>", "<PARTICIPANT_TOKEN>", options).thenAccept((session) -> {
            SessionListener listener = new SessionListener();
            session.setListener(listener);
            Conference conference = session.getConference();
            Video conferenceVideo = findViewById(R.id.conferenceVideo);
            MediaStream conferenceStream = conference.getMediaStream();
            conferenceVideo.setMediaStream(conferenceStream);
            Me me = conference.getMe();
            MediaStream myStream = MindSDK.createMediaStream(microphone, camera);
            me.setMediaStream(myStream);
            CompletableFuture.allOf(microphone.acquire(), camera.acquire()).exceptionally((exception) -> {
                Log.e("ConferenceActivity", "Can't acquire camera or microphone: ", exception);
                return null;
            });
        });
    }

}
```

The summary above is missing two mandatory identifiers: `<CONFERENCE_URI>` and `<PARTICIPANT_TOKEN>`. The former has to
be replaced with an URI of an existent conference, the later with a token of an existent participant. Both of them
should be created on behalf of server part of your application using application token.

# Getting the SDK

Mind Android SDK is an open-source library which is distributed as an
[AAR](https://developer.android.com/studio/projects/android-library) through
[MavenCentral](https://search.maven.org/artifact/com.mind.api/mind-android-sdk). To plug in the latest version of the
SDK to your application, just add the following dependency to the `build.gradle`:

```groovy
dependencies {
    implementation 'com.mind.api:mind-android-sdk:5.12.0'
}
```

Mind Android SDK includes pre-built binaries of [WebRTC library (milestone 111)](https://webrtc.googlesource.com/src/) and
the [script](webrtc/build.sh) which we used for building it, so yu can rebuild the library on your own, if you want.

# API Reference

This is an index of all components (classes, interfaces and enumerations) of Mind Android SDK.

  * [Camera](../5.12.0/reference/Camera.md)
  * [CameraFacing](../5.12.0/reference/CameraFacing.md)
  * [Conference](../5.12.0/reference/Conference.md)
  * [ConferenceLayout](../5.12.0/reference/ConferenceLayout.md)
  * [DeviceRegistry](../5.12.0/reference/DeviceRegistry.md)
  * [Me](../5.12.0/reference/Me.md)
  * [MediaStream](../5.12.0/reference/MediaStream.md)
  * [MediaStreamAudioConsumer](../5.12.0/reference/MediaStreamAudioConsumer.md)
  * [MediaStreamAudioStatistics](../5.12.0/reference/MediaStreamAudioStatistics.md)
  * [MediaStreamAudioSupplier](../5.12.0/reference/MediaStreamAudioSupplier.md)
  * [MediaStreamVideoConsumer](../5.12.0/reference/MediaStreamVideoConsumer.md)
  * [MediaStreamVideoStatistics](../5.12.0/reference/MediaStreamVideoStatistics.md)
  * [MediaStreamVideoSupplier](../5.12.0/reference/MediaStreamVideoSupplier.md)
  * [Microphone](../5.12.0/reference/Microphone.md)
  * [MindSDK](../5.12.0/reference/MindSDK.md)
  * [MindSDKOptions](../5.12.0/reference/MindSDKOptions.md)
  * [Participant](../5.12.0/reference/Participant.md)
  * [ParticipantRole](../5.12.0/reference/ParticipantRole.md)
  * [Session](../5.12.0/reference/Session.md)
  * [SessionListener](../5.12.0/reference/SessionListener.md)
  * [SessionOptions](../5.12.0/reference/SessionOptions.md)
  * [SessionState](../5.12.0/reference/SessionState.md)
  * [SessionStatistics](../5.12.0/reference/SessionStatistics.md)
