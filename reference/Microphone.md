# `class` Microphone

Microphone class is used for representing the microphone of the Android device. The instance of Microphone class that
represent the built-in microphone can be got with [getMicrophone](DeviceRegistry.md#static-getmicrophone) method of
[DeviceRegistry](DeviceRegistry.md) class. Microphone class implements
[MediaStreamAudioSupplier](MediaStreamAudioSupplier.md) interface, so it can be used as a source of audio for local
[MediaStream](MediaStream.md).

```java
DeviceRegistry deviceRegistry = MindSDK.getDeviceRegistry();
Microphone microphone = deviceRegistry.getMicrophone();
MediaStream myStream = MindSDK.createMediaStream(microphone, null);
me.setMediaStream(myStream);
microphone.acquire().exceptionally((exception) -> {
    Log.e("MyApplication", "Microphone can't be acquired", exception);
    return null;
});
```

## setMuted(muted)

Sets the muted state of the microphone. The muted state of the microphone determines whether the microphone produces an
actual audio (if it is unmuted) or silence (if it is muted). The muted state can be changed at any moment regardless
whether the microphone is acquired or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;muted – The muted state of the microphone.

## acquire()

Starts microphone recording. This is an asynchronous operation which assumes acquiring the underlying microphone device
and distributing microphone's audio among all [consumers](MediaStream.md). This method returns a `CompletableFuture`
that completes (on the main thread) with either no value (if the microphone recording starts successfully) or an
exception (if there is no permission to access the microphone or if the microphone was unplugged). If the microphone
recording has been already started, this method returns already completed `CompletableFuture`.

**Returns:**

&nbsp;&nbsp;&nbsp;&nbsp;The completable future that completes (on the main thread) with either no value or an exception.

## release()

Stops microphone recording. This is a synchronous operation which assumes revoking the previously distributed
microphone's audio and releasing the underlying microphone device. The stopping is idempotent: the method does nothing
if the microphone is not acquired, but it would fail if it was called in the middle of acquisition.
