package com.mind.api.sdk;

import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Conference class is used for representing a conferences from point of view of the {@link Me local participant}. You
 * can get a representation of the conference with {@link Session#getConference() getConference} method of {@link
 * Session} class. Conference class contains methods for getting and setting parameters of the conference, for {@link
 * Conference#startRecording() starting} and {@link Conference#stopRecording() stopping} conference recording, for
 * getting {@link Conference#getMe() local} and {@link Conference#getParticipants() remote} participants, and for
 * getting {@link Conference#getMediaStream() conference media stream}:
 *
 * <pre>
 * Conference conference = session.getConference();
 * MediaStream conferenceStream = conference.getMediaStream();
 * Video conferenceVideo = findViewById(R.id.conferenceVideo);
 * conferenceVideo.setMediaStream(conferenceStream);
 * </pre>
 */
@MainThread
public class Conference {

    static Conference fromDTO(Session session, JSONObject dto) throws JSONException {
        JSONArray participantDTOs = dto.getJSONArray("participants");
        Me me = Me.fromDTO(session, participantDTOs.getJSONObject(0));
        List<Participant> participants = new ArrayList<>();
        for (int i = 1; i < participantDTOs.length(); i++) {
            JSONObject participantDTO = participantDTOs.getJSONObject(i);
            if (participantDTO.getBoolean("online")) {
                Participant participant = Participant.fromDTO(session, participantDTO);
                participants.add(participant);
            }
        }
        return new Conference(session, dto.getString("id"), dto.getString("name"), ConferenceLayout.fromString(dto.getString("layout")), dto.getJSONObject("recording").getBoolean("started"), me, participants);
    }

    // Thread-Safety: all methods of `session` except `executeOnMainThread` are accessed on the main thread only
    private final Session session;

    private final String id;
    private String name;
    private ConferenceLayout layout;
    private boolean recordingStarted;
    private final Me me;
    private final List<Participant> participants;
    private final MediaStream mediaStream;
    private final Map<String, Participant> index;

    private Conference(Session session, String id, String name, ConferenceLayout layout, boolean recordingStarted, Me me, List<Participant> participants) {
        this.session = session;
        this.id = id;
        this.name = name;
        this.layout = layout;
        this.recordingStarted = recordingStarted;
        this.me = me;
        this.participants = participants;
        this.mediaStream = new MediaStream("conference", session.getWebRtcConnection(), session.getWebRtcConnection());
        this.index = new HashMap<>();
        this.index.put(me.getId(), me);
        for (Participant participant : participants) {
            this.index.put(participant.getId(), participant);
        }
    }

    /**
     * Returns the ID of the conference. The ID is unique and never changes.
     *
     * @return The ID of the conference.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the current name of the conference. The name of the conference can be shown above the video in the
     * conference media stream and recording.
     *
     * @return The current name of the conference.
     */
    public String getName() {
        return name;
    }

    /**
     * Changes the name of the conference. The name of the conference can be shown above the video in the conference
     * media stream and recording. The name changing is an asynchronous operation, that's why this method returns a
     * {@link CompletableFuture} that completes (on the main thread) with either no value (if the operation succeeds)
     * or an exception (if the operation fails). The operation can succeed only if the {@link Me local participant}
     * plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param name The new name for the conference.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> setName(String name) {
        try {
            JSONObject requestDTO = new JSONObject().put("name", name);
            return session.newHttpPatch("/", requestDTO).thenAccept((responseDTO) -> {
                Conference.this.name = name;
            });
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    /**
     * Returns the current {@link ConferenceLayout layout} of the conference. The layout determines arrangement of
     * videos in {@link Conference#getMediaStream() conference media stream} which the participants receive, and {@link
     * Conference#getRecordingURL() recording}.
     *
     * @return The current layout of the conference.
     */
    public ConferenceLayout getLayout() {
        return this.layout;
    }

    /**
     * Returns the {@link Me local participant}.
     *
     * @return The local participant.
     */
    public Me getMe() {
        return me;
    }

    /**
     * Returns the list of all online {@link Participant remote participants}.
     *
     * @return The list of all online remote participants.
     */
    public List<Participant> getParticipants() {
        return Collections.unmodifiableList(participants);
    }

    /**
     * Returns {@link Participant remote participant} with the specified ID or `null` value, if it doesn't exist or
     * if it is offline.
     *
     * @return The remote participant or `null` value, if it doesn't exist or if it is offline.
     */
    public Participant getParticipantById(String participantId) {
        return index.get(participantId);
    }

    /**
     * Checks whether the conference is being recorded or not.
     *
     * @return The boolean value which indicates if the conference is being recorded or not.
     */
    public boolean isRecording() {
        return recordingStarted;
    }

    /**
     * Starts recording of the conference. This is an asynchronous operation, that's why this method returns a {@link
     * CompletableFuture} that completes (on the main thread) with either no value (if the operation succeeds) or an
     * exception (if the operation fails). The operation can succeed only if the {@link Me local participant} plays a
     * role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> startRecording() {
        JSONObject requestDTO = new JSONObject();
        return session.newHttpPost("/recording/start", requestDTO).thenAccept((responseDTO) -> {
            recordingStarted = true;
        });
    }

    /**
     * Stops recording of the conference. This is an asynchronous operation, that's why this method returns a {@link
     * CompletableFuture} that completes (on the main thread) with either no value (if the operation succeeds) or an
     * exception (if the operation fails). The operation can succeed only if the {@link Me local participant} plays a
     * role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> stopRecording() {
        JSONObject requestDTO = new JSONObject();
        return session.newHttpPost("/recording/stop", requestDTO).thenAccept((responseDTO) -> {
            recordingStarted = false;
        });
    }

    /**
     * Returns a URL for downloading the recording of the conference. The returned URL can be used for downloading only
     * if the {@link Me local participant} plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @return The URL for downloading the recording of the conference.
     */
    public String getRecordingURL() {
        return session.getRecordingURL();
    }

    /**
     * Returns the {@link MediaStream media stream} of the conference. The returned media stream is a mix of all audios
     * and videos (excluding only audio of the local participant) that participants are streaming at the moment. The
     * videos in the media stream are arranged using {@link Conference#getLayout() current layout} of the conference.
     * You can get and play the media stream of the conference at any time.
     *
     * @return The media steam of the conference.
     */
    public MediaStream getMediaStream() {
        return mediaStream;
    }

    void addParticipant(Participant participant) {
        if (index.put(participant.getId(), participant) == null) {
            participants.add(participant);
            session.fireOnParticipantJoined(participant);
        }
    }

    void removeParticipant(Participant participant) {
        if (index.remove(participant.getId()) != null) {
            participants.remove(participant);
            session.fireOnParticipantExited(participant);
        }
    }

    void update(JSONObject dto) throws JSONException {
        if (!Objects.equals(this.name, dto.getString("name"))) {
            this.name = dto.getString("name");
            session.fireOnConferenceNameChanged(this);
        }
        if (this.recordingStarted != dto.getJSONObject("recording").getBoolean("started")) {
            this.recordingStarted = dto.getJSONObject("recording").getBoolean("started");
            if (this.recordingStarted) {
                session.fireOnConferenceRecordingStarted(this);
            } else {
                session.fireOnConferenceRecordingStopped(this);
            }
        }
    }

    void destroy() {
        me.destroy();
        for (Participant participant : participants) {
            participant.destroy();
        }
        mediaStream.setAudioSupplier(null);
        mediaStream.setVideoSupplier(null);
    }

}
