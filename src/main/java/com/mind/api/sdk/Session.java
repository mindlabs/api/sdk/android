package com.mind.api.sdk;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.AnyThread;
import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import java9.util.function.Consumer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.RTCStats;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Session class is used for representing a participation session of the {@link Me local participant}. You can get a
 * representation of the participation session only as a result of {@link MindSDK#join(String, String, SessionOptions)
 * joining} a conference on behalf of one of the participants. It stays valid till you leave the conference in one of
 * three ways: {@link MindSDK#exit2(Session) exit the conference at your own will}, {@link
 * SessionListener#onMeExpelled(Me) being expelled from the conference} or {@link
 * SessionListener#onConferenceEnded(Conference) witness the end of the conference}. Session class contains methods for
 * getting {@link #getState() state} and {@link #getStatistics() statistics} of the participation session, and a method
 * for getting the {@link #getConference() conference} which the {@link Me local participant} is participating in:
 *
 * <pre>
 * String conferenceURI = "https://api.mind.com/&lt;APPLICATION_ID&gt;/&lt;CONFERENCE_ID&gt;";
 * String participantToken = "&lt;PARTICIPANT_TOKEN&gt;";
 * SessionOptions options = new SessionOptions();
 * MindSDK.join(conferenceURI, participantToken, options).thenAccept(session -> {
 *     SessionListener sessionListener = new SessionListener();
 *     session.setListener(sessionListener);
 *     Conference conference = session.getConference();
 *     ...
 * });
 * </pre>
 */
public class Session {

    private static final String LOGGER_TAG = "MindSDK";
    private static final int HTTP_CONNECTION_TIMEOUT_SECONDS = 10;
    private static int LAST_SESSION_ID = 0;

    // Thread-Safety: `mainLooperHandler` can be accessed on any thread
    private final Handler mainLooperHandler = new Handler(Looper.getMainLooper());

    private final int id;
    private final String uri;
    private final String token;
    private final SessionOptions options;
    private SessionListener listener;

    private ExecutorService httpExecutorService = Executors.newSingleThreadExecutor();
    private SessionState state;
    private boolean destroyed;

    private WebRtcConnection wrc;
    private int recoveryDelay;
    private Future<?> recoveryFuture;

    private Conference conference;

    private final List<String> notifications;

    private SessionStatistics statistics;

    @MainThread
    Session(String uri, String token, SessionListener listener, SessionOptions options) {
        LAST_SESSION_ID += 1;
        this.id = LAST_SESSION_ID;
        this.uri = uri.replaceFirst("/+$", "");
        this.token = token;
        this.listener = listener;
        this.options = new SessionOptions(options);
        this.state = SessionState.NORMAL;
        this.destroyed = true;
        this.notifications = new LinkedList<>();
        this.statistics = new SessionStatistics("none", "none", 0, "none", 0);
    }

    /**
     * Returns the ID of the session. The ID is unique and never changes.
     *
     * @return The ID of the session.
     */
    @MainThread
    public int getId() {
        return id;
    }

    /**
     * Returns the current {@link SessionState state} of the session.
     *
     * @return The current state of the session.
     */
    @MainThread
    public SessionState getState() {
        return state;
    }

    /**
     * Returns the latest {@link SessionStatistics statistics} of the session. The statistics consists of instant
     * measures of the underlying network connection of the session.
     *
     * @return The latest statistics of the session.
     */
    @MainThread
    public SessionStatistics getStatistics() {
        return statistics;
    }

    /**
     * Returns the {@link Conference conference}.
     *
     * @return The conference.
     */
    @MainThread
    public Conference getConference() {
        return conference;
    }

    /**
     * Sets the listener which should be notified of all events related to the conference session. The listener can be
     * set at any moment.
     *
     * @param listener The listener which should be notified of all events related to the conference session.
     */
    @MainThread
    public void setListener(SessionListener listener) {
        this.listener = listener;
    }

    @MainThread
    String getApplicationId() {
        return uri.split("/")[3];

    }

    @MainThread
    String getConferenceId() {
        return uri.split("/")[4];
    }

    @MainThread
    String getRecordingURL() {
        return uri + "/recording?access_token=" + token;
    }

    @MainThread
    SessionOptions getOptions() {
        return options;
    }

    @MainThread
    WebRtcConnection getWebRtcConnection() {
        return wrc;
    }

    @MainThread
    CompletableFuture<Session> open() {
        destroyed = false;
        final CompletableFuture<Session> result = new CompletableFuture<>();
        Consumer<Throwable> recover = (exception) -> {
            setState(SessionState.FAILED);
            if (conference != null) {
                Log.w(LOGGER_TAG, "Conference session will be reopened in " + recoveryDelay + " seconds after a failure", exception);
                notifications.clear();
                recoveryFuture = scheduleOnMainThread(() -> wrc.open(), recoveryDelay, TimeUnit.SECONDS);
                recoveryDelay = Math.min(recoveryDelay + 1, 10);
            } else {
                result.completeExceptionally(exception);
            }
        };
        wrc = new WebRtcConnection(this) {

            @Override
            public void onOpened() {
                newHttpGet("/?detailed=true").thenAccept((responseDTO) -> {
                    try {
                        if (conference == null) {
                            conference = Conference.fromDTO(Session.this, responseDTO);
                            result.complete(Session.this);
                        } else {
                            updateEntireModel(responseDTO);
                        }
                        processNotifications();
                        recoveryDelay = 0;
                        setState(SessionState.NORMAL);
                    } catch (Exception exception) {
                        recover.accept(exception);
                    }
                }).exceptionally((exception) -> {
                    recover.accept(exception);
                    return null;
                });
            }

            @Override
            public void onMessageReceived(String notification) {
                notifications.add(notification);
                if (conference != null) {
                    processNotifications();
                }
            }

            @Override
            void onStartedLagging() {
                setState(SessionState.LAGGING);
            }

            @Override
            void onStoppedLagging() {
                setState(SessionState.NORMAL);
            }

            @Override
            public void onFailed(Throwable exception) {
                recover.accept(exception);
            }

            @Override
            public void onClosed(int code) {
                switch (code) {
                    case 4000:
                        notifications.add("{\"type\":\"deleted\",\"location\":\"/\"}");
                        if (conference != null) {
                            processNotifications();
                        }
                        break;
                    case 4001:
                        notifications.add("{\"type\":\"deleted\",\"location\":\"/participants/me\"}"); // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
                        if (conference != null) {
                            processNotifications();
                        }
                        break;
                    default:
                        recover.accept(new IOException("WebRTC connection closed: " + code));
                }
            }

        };
        wrc.open();
        return result;
    }

    @MainThread
    void close() {
        statistics = new SessionStatistics("none", "none", 0, "none", 0);
        if (recoveryFuture != null) {
            recoveryFuture.cancel(false);
            recoveryFuture = null;
        }
        if (conference != null) {
            conference.destroy();
            conference = null;
        }
        if (wrc != null) {
            wrc.close();
            wrc = null;
        }
        httpExecutorService.shutdownNow();
        destroyed = true;
    }

    @AnyThread
    void executeOnMainThread(Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (!destroyed) {
                runnable.run();
            }
        } else {
            mainLooperHandler.post(() -> {
                if (!destroyed) {
                    runnable.run();
                }
            });
        }
    }

    @MainThread
    Future<?> scheduleOnMainThread(Runnable runnable, long delay, TimeUnit unit) {
        final AtomicReference<Runnable> runnableReference = new AtomicReference<>();
        final CompletableFuture<Void> result = new CompletableFuture<>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                mainLooperHandler.removeCallbacks(runnableReference.get());
                return true;
            }

        };
        runnableReference.set(() -> {
            try {
                if (!destroyed) {
                    runnable.run();
                }
                result.complete(null);
            } catch (Exception exception) {
                result.completeExceptionally(exception);
            }
        });
        mainLooperHandler.postDelayed(runnableReference.get(), unit.toMillis(delay));
        return result;
    }

    @MainThread
    Future<?> scheduleOnMainThreadWithFixedDelay(Runnable runnable, long delay, TimeUnit unit) {
        final CompletableFuture<Void> result = new CompletableFuture<>();
        final AtomicReference<Runnable> runnableReference = new AtomicReference<>();
        runnableReference.set(() -> {
            try {
                if (!destroyed && !result.isCancelled()) {
                    runnable.run();
                    mainLooperHandler.postDelayed(runnableReference.get(), unit.toMillis(delay));
                }
            } catch (Exception exception) {
                result.completeExceptionally(exception);
            }
        });
        mainLooperHandler.postDelayed(runnableReference.get(), unit.toMillis(delay));
        return result;
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpGet(String relativeURI) {
        return newHttpGet(relativeURI, null);
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpGet(String relativeURI, Consumer<Future<?>> cancellation) {
        return newHttpRequest("GET", uri + relativeURI, null, cancellation);
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpPost(String relativeURI, JSONObject dto) {
        return newHttpPost(relativeURI, dto, null);
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpPost(String relativeURI, JSONObject dto, Consumer<Future<?>> cancellation) {
        return newHttpRequest("POST", uri + relativeURI, dto, cancellation);
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpPatch(String relativeURI, JSONObject dto) {
        return newHttpPatch(relativeURI, dto, null);
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpPatch(String relativeURI, JSONObject dto, Consumer<Future<?>> cancellation) {
        return newHttpRequest("PATCH", uri + relativeURI, dto, cancellation);
    }

    @MainThread
    CompletableFuture<JSONObject> newHttpRequest(String method, String url, JSONObject dto, Consumer<Future<?>> cancellation) {
        final CompletableFuture<JSONObject> result = new CompletableFuture<>();
        final AtomicReference<Future<?>> future = new AtomicReference<>();
        future.set(httpExecutorService.submit(() -> {
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) new URL(url).openConnection();
                connection.setRequestMethod(method);
                connection.setConnectTimeout(HTTP_CONNECTION_TIMEOUT_SECONDS * 1000);
                connection.setReadTimeout(HTTP_CONNECTION_TIMEOUT_SECONDS * 1000);
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Authorization", "Bearer " + token);
                connection.setRequestProperty("Mind-SDK", "Mind Android SDK " + BuildConfig.VERSION_NAME);
                connection.setRequestProperty("User-Agent", "Android " + Build.VERSION.SDK_INT);
                if (dto != null) {
                    connection.setDoOutput(true);
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(dto.toString());
                    writer.close();
                }
                int responseCode = connection.getResponseCode();
                if (responseCode >= 200 && responseCode < 300) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
                    for (String line = reader.readLine(); !future.get().isCancelled() && line != null; line = reader.readLine()) {
                        sb.append(line).append("\n");
                    }
                    reader.close();
                    if (!future.get().isCancelled()) {
                        String content = sb.toString().trim();
                        JSONObject responseDTO = content.length() > 0 ? new JSONObject(content) : new JSONObject();
                        executeOnMainThread(() -> {
                            if (!future.get().isCancelled()) {
                                result.complete(responseDTO);
                            }
                        });
                    }
                } else {
                    if (responseCode == 404 && conference != null) {
                        executeOnMainThread(() -> {
                            notifications.add("{\"type\":\"deleted\",\"location\":\"/participants/me\"}"); // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
                            processNotifications();
                        });
                    } else {
                        throw new IOException("HTTP response status code: " + connection.getResponseCode());
                    }
                }
            } catch (Exception exception) {
                executeOnMainThread(() -> {
                    result.completeExceptionally(exception);
                });
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }));
        if (cancellation != null) {
            cancellation.accept(future.get());
        }
        return result;
    }

    @MainThread
    void fireOnConferenceNameChanged(Conference conference) {
        if (listener != null) {
            listener.onConferenceNameChanged(conference);
        }
    }

    @MainThread
    void fireOnConferenceRecordingStarted(Conference conference) {
        if (listener != null) {
            listener.onConferenceRecordingStarted(conference);
        }
    }

    @MainThread
    void fireOnConferenceRecordingStopped(Conference conference) {
        if (listener != null) {
            listener.onConferenceRecordingStopped(conference);
        }
    }

    @MainThread
    void fireOnConferenceEnded(Conference conference) {
        if (listener != null) {
            listener.onConferenceEnded(conference);
        }
    }

    @MainThread
    void fireOnParticipantJoined(Participant participant) {
        if (listener != null) {
            listener.onParticipantJoined(participant);
        }
    }

    @MainThread
    void fireOnParticipantExited(Participant participant) {
        if (listener != null) {
            listener.onParticipantExited(participant);
        }
    }

    @MainThread
    void fireOnParticipantNameChanged(Participant participant) {
        if (listener != null) {
            listener.onParticipantNameChanged(participant);
        }
    }

    @MainThread
    void fireOnParticipantPriorityChanged(Participant participant) {
        if (listener != null) {
            listener.onParticipantPriorityChanged(participant);
        }
    }

    @MainThread
    void fireOnParticipantRoleChanged(Participant participant) {
        if (listener != null) {
            listener.onParticipantRoleChanged(participant);
        }
    }

    @MainThread
    void fireOnParticipantMediaChanged(Participant participant) {
        if (listener != null) {
            listener.onParticipantMediaChanged(participant);
            }
    }

    @MainThread
    void fireOnParticipantSecondaryMediaChanged(Participant participant) {
        if (listener != null) {
            listener.onParticipantSecondaryMediaChanged(participant);
        }
    }

    @MainThread
    void fireOnMeExpelled(Me me) {
        if (listener != null) {
            listener.onMeExpelled(me);
        }
    }

    @MainThread
    void fireOnMeNameChanged(Me me) {
        if (listener != null) {
            listener.onMeNameChanged(me);
        }
    }

    @MainThread
    void fireOnMePriorityChanged(Me me) {
        if (listener != null) {
            listener.onMePriorityChanged(me);
        }
    }

    @MainThread
    void fireOnMeRoleChanged(Me me) {
        if (listener != null) {
            listener.onMeRoleChanged(me);
        }
    }

    @MainThread
    void fireOnMeReceivedMessageFromApplication(Me me, String message) {
        if (listener != null) {
            listener.onMeReceivedMessageFromApplication(me, message);
        }
    }

    @MainThread
    void fireOnMeReceivedMessageFromParticipant(Me me, String message, Participant participant) {
        if (listener != null) {
            listener.onMeReceivedMessageFromParticipant(me, message, participant);
        }
    }

    void updateStatistics(List<RTCStats> report) {
        if (report != null) {
            RTCStats selectedCandidatePair = null;
            RTCStats localCandidate = null;
            RTCStats remoteCandidate = null;
            for (RTCStats stats : report) {
                switch (stats.getType()) {
                    case "candidate-pair":
                        selectedCandidatePair = stats;
                        break;
                    case "local-candidate":
                        localCandidate = stats;
                        break;
                    case "remote-candidate":
                        remoteCandidate = stats;
                        break;
                }
            }
            if (selectedCandidatePair != null && localCandidate != null && remoteCandidate != null) {
                this.statistics = new SessionStatistics((String) localCandidate.getMembers().get("protocol"),
                                                        (String) localCandidate.getMembers().get("ip"),
                                                        ((Number) localCandidate.getMembers().get("port")).intValue(),
                                                        (String) remoteCandidate.getMembers().get("ip"),
                                                        ((Number) remoteCandidate.getMembers().get("port")).intValue());
            }
        }
    }

    private void updateEntireModel(JSONObject dto) throws JSONException {
        updateModelItem("/", dto);
        JSONArray participantDTOs = dto.getJSONArray("participants");
        // Build a set of IDs of online participants
        Set<String> onlineParticipantIDs = new HashSet<>();
        for (int i = 0; i < participantDTOs.length(); i++) {
            JSONObject participantDTO = participantDTOs.getJSONObject(i);
            if (participantDTO.getBoolean("online")) {
                onlineParticipantIDs.add(participantDTO.getString("id"));
            }
        }
        // Remove missing and offline participants
        List<Participant> participants = conference.getParticipants();
        for (int i = participants.size() - 1; i >= 0; i--) {
            if (!onlineParticipantIDs.contains(participants.get(i).getId())) {
                deleteModelItem("/participants/" + participants.get(i).getId());
            }
        }
        // Update existent participants and add new participants
        for (int i = 0; i < participantDTOs.length(); i++) {
            JSONObject participantDTO = participantDTOs.getJSONObject(i);
            if (participantDTO.getBoolean("online")) {
                Participant participant = conference.getParticipantById(participantDTO.getString("id"));
                if (participant != null) {
                    updateModelItem("/participants/" + participantDTO.getString("id"), participantDTO);
                } else {
                    createModelItem("/participants/" + participantDTO.getString("id"), participantDTO);
                }
            }
        }
    }

    private void createModelItem(String location, JSONObject dto) throws JSONException {
        if (location.startsWith("/participants/")) {
            if (dto.getBoolean("online")) {
                Participant participant = Participant.fromDTO(this, dto);
                conference.addParticipant(participant);
            }
        } else if (location.startsWith("/messages/")) {
            String sentBy = dto.getString("sentBy");
            if (Objects.equals(sentBy, getApplicationId())) {
                fireOnMeReceivedMessageFromApplication(conference.getMe(), dto.getString("text"));
            } else {
                fireOnMeReceivedMessageFromParticipant(conference.getMe(), dto.getString("text"), conference.getParticipantById(sentBy));
            }
        }
    }

    private void updateModelItem(String location, JSONObject dto) throws JSONException {
        if (location.equals("/")) {
            conference.update(dto);
        } else if (location.startsWith("/participants/")) {
            if (dto.getBoolean("online")) {
                Participant participant = conference.getParticipantById(location.substring("/participants/".length()));
                if (participant != null) {
                    participant.update(dto);
                } else {
                    createModelItem(location, dto);
                }
            } else {
                deleteModelItem(location);
            }
        }
    }

    private void deleteModelItem(String location) {
        if (location.equals("/")) {
            Conference conference = this.conference;
            MindSDK.exit2(this);
            fireOnConferenceEnded(conference);
        } else if (location.startsWith("/participants/me")) { // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
            Conference conference = this.conference;
            MindSDK.exit2(this);
            fireOnMeExpelled(conference.getMe());
        } else if (location.startsWith("/participants/")) {
            Participant participant = conference.getParticipantById(location.substring("/participants/".length()));
            if (participant != null) {
                conference.removeParticipant(participant);
                participant.destroy();
            }
        }
    }

    private void processNotifications() {
        for (String notification : notifications) {
            try {
                JSONObject json = new JSONObject(notification);
                switch (json.getString("type")) {
                    case "created":
                        createModelItem(json.getString("location"), json.getJSONObject("resource"));
                        break;
                    case "updated":
                        updateModelItem(json.getString("location"), json.getJSONObject("resource"));
                        break;
                    case "deleted":
                        deleteModelItem(json.getString("location"));
                        break;
                }
            } catch (JSONException exception) {
                Log.w(LOGGER_TAG, "Can't parse notification `" + notification + "`", exception);
            }
        }
        notifications.clear();
    }

    private void setState(SessionState state) {
        if (this.state != state) {
            this.state = state;
            if (listener != null) {
                listener.onSessionStateChanged(this);
            }
        }
    }

}
