package com.mind.api.sdk;

import org.webrtc.DataChannel;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class DistortionRegistry {

    private final Map<String, Long> mtids = new HashMap<>();
    private final Map<String, Distortion> distortions = new HashMap<>();
    private final List<String> messages = new LinkedList<>();

    DistortionRegistry() {}

    void addMtid(String mtid) {
        mtids.put(mtid, System.currentTimeMillis());
    }

    void removeMtid(String mtid) {
        mtids.remove(mtid);
    }

    void registerDistortion(String mtid) {
        if (mtids.containsKey(mtid) && System.currentTimeMillis() - mtids.get(mtid) > 3000) {
            Distortion distortion = distortions.get(mtid);
            if (distortion != null) {
                if (distortion.prolong()) {
                    return;
                } else {
                    messages.add(distortion.toString());
                }
            }
            distortions.put(mtid, new Distortion(mtid));
        }
    }

    void report(DataChannel dataChannel) {
        for (Iterator<Map.Entry<String, Distortion>> iterator = distortions.entrySet().iterator(); iterator.hasNext();) {
            Distortion distortion = iterator.next().getValue();
            if (distortion.isEnded()) {
                messages.add(distortion.toString());
                iterator.remove();
            }
        }
        for (String message : messages) {
            dataChannel.send(new DataChannel.Buffer(ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8)), false));
        }
        messages.clear();
    }

}
