package com.mind.api.sdk;

/**
 * SessionOptions class represents a set of configuration options for a {@link Session participation session}. The
 * default constructor creates an instance of SessionOptions class with the default values for all configuration
 * options. If necessary, you can change any default value before passing the instance to the static
 * {@link MindSDK#join(String, String, SessionOptions) join} method of {@link MindSDK} class:
 *
 * <pre>
 * String conferenceURI = "https://api.mind.com/&lt;APPLICATION_ID&gt;/&lt;CONFERENCE_ID&gt;";
 * String participantToken = "&lt;PARTICIPANT_TOKEN&gt;";
 * SessionOptions options = new SessionOptions();
 * options.setStunServer("stun:stun.l.google.com:19302");
 * MindSDK.join(conferenceURI, participantToken, options).thenAccept(session -> {
 *     ...
 * });
 * </pre>
 */
public class SessionOptions {

    // TODO: Replace `Boolean` with `boolean` in Mind Android SDK 6.0.0
    private Boolean useVp9ForSendingVideo;
    private String stunServerURL;
    private String turnServerURL;
    private String turnServerUsername;
    private String turnServerPassword;

    public SessionOptions() {
        this.useVp9ForSendingVideo = false;
        this.stunServerURL = null;
        this.turnServerURL = null;
        this.turnServerUsername = null;
        this.turnServerPassword = null;
    }

    SessionOptions(SessionOptions options) {
        this.useVp9ForSendingVideo = options.isUseVp9ForSendingVideo();
        this.stunServerURL = options.getStunServerURL();
        this.turnServerURL = options.getTurnServerURL();
        this.turnServerUsername = options.getTurnServerUsername();
        this.turnServerPassword = options.getTurnServerPassword();
    }

    /**
     * Sets whether VP9 codec should be used for sending video or not. If `true` then any outgoing video will be
     * encoded with VP9 in SVC mode, otherwise — with VP8 in simulcast mode. The default value is `false`.
     *
     * @param useVp9ForSendingVideo Whether VP9 codec should be used for sending video or not.
     *
     * TODO: Replace `Boolean` with `boolean` in Mind Android SDK 6.0.0
     */
    public void setUseVp9ForSendingVideo(Boolean useVp9ForSendingVideo) {
        this.useVp9ForSendingVideo = useVp9ForSendingVideo;
    }

    /**
     * Sets a STUN server which should be used for establishing a participation session. If it is set and if it is not
     * `null`, then Mind Android SDK will try to gather and use a reflexive Ice candidates for establishing the
     * participant session.
     *
     * @param stunServerURL The URL for connecting to the STUN server.
     */
    public void setStunServer(String stunServerURL) {
        this.stunServerURL = stunServerURL;
    }

    /**
     * Sets a TURN server which should be used for establishing a participation session. If it is set and if it is not
     * `null`, then Mind Android SDK will try to gather and use a relay Ice candidates for establishing the participant
     * session.
     *
     * @param turnServerURL The URL for connecting to the TURN server.
     * @param turnServerUsername The username for connecting to the TURN server.
     * @param turnServerPassword The password for connecting to the TURN server.
     */
    public void setTurnServer(String turnServerURL, String turnServerUsername, String turnServerPassword) {
        this.turnServerURL = turnServerURL;
        this.turnServerUsername = turnServerUsername;
        this.turnServerPassword = turnServerPassword;
    }

    Boolean isUseVp9ForSendingVideo() {
        return useVp9ForSendingVideo;
    }

    String getStunServerURL() {
        return stunServerURL;
    }

    String getTurnServerURL() {
        return turnServerURL;
    }

    String getTurnServerUsername() {
        return turnServerUsername;
    }

    String getTurnServerPassword() {
        return turnServerPassword;
    }

}
