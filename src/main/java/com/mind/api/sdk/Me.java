package com.mind.api.sdk;

import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

/**
 * Me class is used for representing participant on behalf of whom Android-application is participating in the
 * conference (aka the local participant). All other participants are considered to be remote and represented with
 * {@link Participant} class. You can get a representation of the local participant with {@link Conference#getMe()
 * getMe} method of {@link Conference} class. Me is a subclass of {@link Participant} class, so that it inherits all
 * public methods of the superclass and adds methods for setting primary and secondary media streams that should be
 * sent on behalf of the local participant, and for sending messages from the local participant to other participant(s)
 * or to the server part of your application:
 *
 * <pre>
 * DeviceRegistry deviceRegistry = MindSDK.getDeviceRegistry();
 * Microphone microphone = deviceRegistry.getMicrophone();
 * Camera camera = deviceRegistry.getCamera();
 * MediaStream myStream = MindSDK.createMediaStream(microphone, camera);
 * Me me = conference.getMe();
 * me.setMediaStream(myStream);
 * // Acquire camera or/and microphone to start streaming video or/and audio on behalf of `me`
 * CompletableFuture.allOf(microphone.acquire(), camera.acquire()).exceptionally((exception) -> {
 *     Log.e("MyApplication", "Can't acquire camera or microphone", exception);
 *     return null;
 * });
 *
 * ...
 *
 * me.sendMessageToAll("Hello, everybody!");
 * me.sendMessageToApplication("Hello, the server part of the application!");
 * Participant participant = conference.getParticipantById("&lt;PARTICIPANT_ID&gt;");
 * if (participant != null) {
 *     me.sendMessageToParticipant("Hello, " + participant.getName(), participant);
 * }
 * </pre>
 */
@MainThread
public class Me extends Participant {

    static Me fromDTO(Session session, JSONObject dto) throws JSONException {
        return new Me(session, dto.getString("id"), dto.getString("name"), dto.getDouble("priority"), ParticipantRole.fromString(dto.getString("role")));
    }

    private Me(Session session, String id, String name, double priority, ParticipantRole role) {
        super(session, id, name, priority, role, false, false, false, false);
    }

    /**
     * Return the ID of the local participant. The ID is unique and never changes.
     *
     * @return The ID of the local participant.
     */
    @Override
    public String getId() {
        return super.getId();
    }

    /**
     * Returns the current name of the local participant. The name of the local participant can be shown above his
     * video in the conference media stream and recording.
     *
     * @return The current name of the local participant.
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     * Changes the name of the local participant. The name of the local participant can be shown above his video in the
     * conference media stream and recording. The name changing is an asynchronous operation, that's why this method
     * returns a {@link CompletableFuture} that completes (on the main thread) with either no value (if the operation
     * succeeds) or an exception (if the operation fails).
     *
     * @param name The new name for the local participant.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    @Override
    public CompletableFuture<Void> setName(String name) {
        return super.setName(name);
    }

    /**
     * Returns the current priority of the local participant. The priority defines a place which local participant
     * takes in conference media stream and recording.
     *
     * @return The current priority of the local participant.
     */
    @Override
    public double getPriority() {
        return super.getPriority();
    }

    /**
     * Changes the priority of the local participant. The priority defines a place which local participant takes in
     * conference media stream and recording. The priority changing is an asynchronous operation, that's why this
     * method returns a {@link CompletableFuture} that completes (on the main thread) with either no value (if the
     * operation succeeds) or an exception (if the operation fails). The operation can succeed only if the {@link Me
     * local participant} plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param priority The new priority for the local participant.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    @Override
    public CompletableFuture<Void> setPriority(double priority) {
        return super.setPriority(priority);
    }

    /**
     * Returns the current {@link ParticipantRole role} of the local participant. The role defines a set of permissions
     * which the local participant is granted.
     *
     * @return The current role of the local participant.
     */
    @Override
    public ParticipantRole getRole() {
        return super.getRole();
    }

    /**
     * Changes the {@link ParticipantRole role} of the local participant. The role defines a set of permissions which
     * the local participant is granted. The role changing is an asynchronous operation, that's why this method returns
     * a {@link CompletableFuture} that completes (on the main thread) with either no value (if the operation succeeds)
     * or an exception (if the operation fails). The operation can succeed only if the {@link Me local participant}
     * plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param role The new role for the local participant.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    @Override
    public CompletableFuture<Void> setRole(ParticipantRole role) {
        return super.setRole(role);
    }

    /**
     * Checks whether the local participant is streaming primary audio (i.e. audio taken from his microphone). If both
     * this method and {@link Me#isStreamingVideo() isStreamingVideo} return `false` then the participant is not
     * streaming the primary media stream at all.
     *
     * @return The boolean value which indicates if the local participant is streaming primary audio or not.
     */
    @Override
    public boolean isStreamingAudio() {
        return session.getWebRtcConnection().isSendingPrimaryAudio();
    }

    /**
     * Checks whether the local participant is streaming primary video (i.e. video taken from his camera). If both
     * this method and {@link Me#isStreamingAudio() isStreamingAudio} return `false` then the participant is not
     * streaming the primary media stream at all.
     *
     * @return The boolean value which indicates if the local participant is streaming primary video or not.
     */
    @Override
    public boolean isStreamingVideo() {
        return session.getWebRtcConnection().isSendingPrimaryVideo();
    }

    /**
     * Returns {@link MediaStream media stream} which is being streamed on behalf of the local participant as the
     * primary media stream or `null` value if the local participant is not streaming the primary media stream at the
     * moment. The primary media stream is intended for streaming video and audio taken from a camera and a microphone
     * of the Android device, respectively.
     *
     * @return The current primary media stream of the local participant.
     */
    @Override
    public MediaStream getMediaStream() {
        return session.getWebRtcConnection().getPrimaryMediaStream();
    }

    /**
     * Sets {@link MediaStream media stream} for streaming on behalf of the local participant as the primary media
     * stream. The primary media stream is intended for streaming video and audio taken from a camera and a microphone
     * of the Android device, respectively. If the primary media stream is already being streamed, then it will be
     * replaced with the passed one. Set `null` value to stop streaming the primary media stream on behalf of the local
     * participant.
     *
     * @param stream The new primary media stream of the local participant.
     */
    public void setMediaStream(MediaStream stream) {
        session.getWebRtcConnection().setPrimaryMediaStream(stream);
    }

    /**
     * Checks whether the local participant is streaming secondary audio (i.e. an arbitrary content with audio). If
     * both this method and {@link Me#isStreamingSecondaryVideo() isStreamingSecondaryVideo} return `false` then the
     * participant is not streaming secondary media stream at all.
     *
     * @return The boolean value which indicates if the local participant is streaming secondary audio or not.
     */
    @Override
    public boolean isStreamingSecondaryAudio() {
        return session.getWebRtcConnection().isSendingSecondaryAudio();
    }

    /**
     * Checks whether the local participant is streaming secondary video (i.e. an arbitrary content with video). If
     * both this method and {@link Me#isStreamingSecondaryAudio() isStreamingSecondaryAudio} return `false` then the
     * participant is not streaming secondary media stream at all.
     *
     * @return The boolean value which indicates if the local participant is streaming secondary video or not.
     */
    @Override
    public boolean isStreamingSecondaryVideo() {
        return session.getWebRtcConnection().isSendingSecondaryVideo();
    }

    /**
     * Returns {@link MediaStream media stream} which is being streamed on behalf of the local participant as the
     * secondary media stream or `null` value if the local participant is not streaming the secondary media stream at
     * the moment. The secondary media stream is intended for streaming an arbitrary audio/video content, e.g. for
     * sharing a screen of the Android device.
     *
     * @return The current secondary media stream of the local participant.
     */
    @Override
    public MediaStream getSecondaryMediaStream() {
        return session.getWebRtcConnection().getSecondaryMediaStream();
    }

    /**
     * Sets {@link MediaStream media stream} for streaming on behalf of the local participant as the secondary media
     * stream. The secondary media stream is intended for streaming an arbitrary audio/video content, e.g. for sharing
     * a screen of the Android device. If the secondary media stream is already being streamed, then it will be
     * replaced with the passed one. Set `null` value to stop streaming the secondary media stream on behalf of the
     * local participant.
     *
     * @param stream The new secondary media stream of the local participant.
     */
    public void setSecondaryMediaStream(MediaStream stream) {
        session.getWebRtcConnection().setSecondaryMediaStream(stream);
    }

    /**
     * Sends a text message on behalf of the local participant to the server part of your application. The message
     * sending is an asynchronous operation, that's why this method returns a {@link CompletableFuture} that completes
     * (on the main thread) with either no value (if the operation succeeds) or an exception (if the operation fails).
     *
     * @param message The text of the message.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> sendMessageToApplication(String message) {
        try {
            JSONObject requestDTO = new JSONObject().put("sendTo", session.getApplicationId()).put("text", message).put("persistent", false);
            return session.newHttpPost("/messages", requestDTO).thenAccept((responseDTO) -> {});
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    /**
     * Sends a text message on behalf of the local participant to the specified participant. The message sending is an
     * asynchronous operation, that's why this method returns a {@link CompletableFuture} that completes (on the main
     * thread) with either no value (if the operation succeeds) or an exception (if the operation fails).
     *
     * @param message The text of the message.
     * @param participant The participant which the message should be sent to.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> sendMessageToParticipant(String message, Participant participant) {
        try {
            JSONObject requestDTO = new JSONObject().put("sendTo", participant.getId()).put("text", message).put("persistent", false);
            return session.newHttpPost("/messages", requestDTO).thenAccept((responseDTO) -> {});
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    /**
     * Sends a text message on behalf of the local participant to all in the conference, i.e. to the server part of
     * your application and to all participants at once. The message sending is an asynchronous operation, that's why
     * this method returns a {@link CompletableFuture} that completes (on the main thread) with either no value (if the
     * operation succeeds) or an exception (if the operation fails).
     *
     * @param message The text of the message.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> sendMessageToAll(String message) {
        try {
            JSONObject requestDTO = new JSONObject().put("sendTo", session.getConferenceId()).put("text", message).put("persistent", false);
            return session.newHttpPost("/messages", requestDTO).thenAccept((responseDTO) -> {});
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    @Override
    void update(JSONObject dto) throws JSONException {
        String name = dto.getString("name");
        if (!Objects.equals(this.name, name)) {
            this.name = name;
            session.fireOnMeNameChanged(this);
        }
        double priority = dto.getDouble("priority");
        if (this.priority != priority) {
            this.priority = priority;
            session.fireOnMePriorityChanged(this);
        }
        ParticipantRole role = ParticipantRole.fromString(dto.getString("role"));
        if (this.role != role) {
            this.role = role;
            session.fireOnMeRoleChanged(this);
        }
    }

    @Override
    void destroy() {
        super.destroy();
    }

}
