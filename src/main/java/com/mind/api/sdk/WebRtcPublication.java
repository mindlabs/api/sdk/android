package com.mind.api.sdk;

class WebRtcPublication {

    private WebRtcTransceiver audioTransceiver;
    private WebRtcTransceiver videoTransceiver;
    private MediaStreamAudioBuffer audioBuffer;
    private MediaStreamVideoBuffer videoBuffer;
    private MediaStream stream;

    WebRtcPublication() {}

    WebRtcTransceiver getAudioTransceiver() {
        return audioTransceiver;
    }

    void setAudioTransceiver(WebRtcTransceiver audioTransceiver) {
        this.audioTransceiver = audioTransceiver;
    }

    WebRtcTransceiver getVideoTransceiver() {
        return videoTransceiver;
    }

    void setVideoTransceiver(WebRtcTransceiver videoTransceiver) {
        this.videoTransceiver = videoTransceiver;
    }

    MediaStreamAudioBuffer getAudioBuffer() {
        return audioBuffer;
    }

    void setAudioBuffer(MediaStreamAudioBuffer audioBuffer) {
        this.audioBuffer = audioBuffer;
    }

    MediaStreamVideoBuffer getVideoBuffer() {
        return videoBuffer;
    }

    void setVideoBuffer(MediaStreamVideoBuffer videoBuffer) {
        this.videoBuffer = videoBuffer;
    }

    MediaStream getStream() {
        return stream;
    }

    void setStream(MediaStream stream) {
        this.stream = stream;
    }

}
