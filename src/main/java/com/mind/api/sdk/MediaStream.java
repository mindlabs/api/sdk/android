package com.mind.api.sdk;

import androidx.annotation.MainThread;
import org.webrtc.RTCStats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * MediaStream class is used for representing audio/video content which participants can send and receive. It can
 * contain one audio and one video only. There are two types of media streams: local and remote. The distinction is
 * nominal — still there is only one class to represent both of them.
 * <p>
 * Local media streams are created explicitly with a help of {@link MindSDK#createMediaStream(MediaStreamAudioSupplier,
 * MediaStreamVideoSupplier) createMediaStream} method of {@link MindSDK} class. They can contain audio/video from
 * local suppliers only (such as {@link Microphone} and {@link Camera}). Local media streams are intended to be {@link
 * Me#setMediaStream(MediaStream) streamed on behalf of the local participant} only.
 * <p>
 * Remote media streams contain audio/video of remote participants or the conference. The primary and the secondary
 * media streams of any remote participant can be got with help of {@link Participant#getMediaStream() getMediaStream}
 * or {@link Participant#getSecondaryMediaStream() getSecondaryMediaStream} methods of {@link Participant} class,
 * respectively. The media stream of the conference can be got with {@link Conference#getMediaStream() getMediaStream}
 * method of {@link Conference} class.
 * <p>
 * Any media stream can be played with {@link Video} or {@link Audio} classes. Keep in mind that {@link Video} tries to
 * play (tries to consume) audio and video of the media stream, whereas {@link Audio} tries to play (tries to consume)
 * audio only of the media stream. This means that if you want to play only audio of a remote media stream, it is
 * better to play it with {@link Audio} instead of playing it with invisible {@link Video} because the latter will
 * force underlying WebRTC connection to receive and decode both audio and video.
 *
 * <pre>
 * Video myVideo = findViewById(R.id.myVideo);
 * Video participantVideo = findViewById(R.id.participantVideo);
 *
 * Me me = conference.getMe();
 *
 * // We assume that microphone and camera have been already acquired or will be acquired later
 * MediaStream myStream = MindSDK.createMediaStream(microphone, camera);
 * me.setMediaStream(myStream);
 * myVideo.setMediaStream(myStream);
 *
 * Participant participant = conference.getParticipantById("&lt;PARTICIPANT_ID&gt;");
 * if (participant != null) {
 *     participantVideo.setMediaStream(participant.getMediaStream());
 * }
 *
 * ...
 *
 * Audio conferenceAudio = new Audio();
 * conferenceAudio.setMediaStream(conference.getMediaStream());
 * </pre>
 */
@MainThread
public class MediaStream {

    private final Map<MediaStreamAudioConsumer, Double> audioConsumers = new HashMap<>();
    private final Set<MediaStreamVideoConsumer> videoConsumers = new HashSet<>();

    private final String label;
    private MediaStreamAudioSupplier audioSupplier;
    private MediaStreamVideoSupplier videoSupplier;

    private MediaStreamAudioBuffer audioBuffer;
    private MediaStreamVideoBuffer videoBuffer;

    private MediaStreamAudioStatistics audioStatistics;
    private MediaStreamVideoStatistics videoStatistics;

    private int maxVideoFrameArea;
    private int maxVideoFrameRate;
    private int maxVideoBitrate;

    private Map<String, Long> oldStatistics;

    MediaStream(String label, MediaStreamAudioSupplier audioSupplier, MediaStreamVideoSupplier videoSupplier) {
        this.label = label;
        this.audioSupplier = audioSupplier;
        this.videoSupplier = videoSupplier;
        this.audioStatistics = new MediaStreamAudioStatistics(0, 0, 0, 0, 0, 0);
        this.videoStatistics = new MediaStreamVideoStatistics(0, 0, 0, 0, 0, 0, 0);
        this.maxVideoFrameArea = Integer.MAX_VALUE;
        this.maxVideoFrameRate = Integer.MAX_VALUE;
        this.maxVideoBitrate = Integer.MAX_VALUE;
    }

    /**
     * Returns the current maximum video frame area of the media stream. The maximum video frame area of the media
     * stream is a product of the width and height which the video should not exceed while being transmitting over the
     * network.
     *
     * @return The current maximum video frame area of the media stream.
     */
    public int getMaxVideoFrameArea() {
        return maxVideoFrameArea;
    }

    /**
     * Sets the maximum video frame area of the media stream. The maximum video frame area of the media stream is a
     * product of the width and height which the video should not exceed while being transmitting over the network.
     * This method can be used for limiting the video frame area of any media stream, but the limiting effect depends
     * on the type of the media stream: setting the maximum video frame area for a local media stream (the video of
     * which usually consist of multiple encodings) leads to filtering out from the transmission all the encodings with
     * the higher video frame areas; setting the maximum video frame area for a remote media stream (the video of which
     * always consists of only one encoding) leads to decreasing the frame area of the transmitted video to make it fit
     * into the limit. Though this method cannot be used for stopping the transmission of the stream's video completely
     * — if the limit cannot be satisfied, the encoding with the lowest video frame area will be transmitted anyway. By
     * default, the maximum video frame area of any media stream is unlimited.
     *
     * @param maxVideoFrameArea The maximum video frame area of the media stream.
     */
    public void setMaxVideoFrameArea(int maxVideoFrameArea) {
        if (maxVideoFrameArea <= 0) {
            throw new Error("Can't change max video frame area to `" + maxVideoFrameArea + "`");
        }
        if (this.maxVideoFrameArea != maxVideoFrameArea) {
            this.maxVideoFrameArea = maxVideoFrameArea;
            if (videoConsumers.size() > 0 && videoSupplier != null) {
                videoSupplier.addVideoConsumer(this);
            }
        }
    }

    /**
     * Returns the current maximum video frame rate of the media stream. The maximum video frame rate of the media
     * stream is a frame rate which the video should not exceed while being transmitting over the network.
     *
     * @return The current maximum video frame rate of the media stream.
     */
    public int getMaxVideoFrameRate() {
        return maxVideoFrameRate;
    }

    /**
     * Sets the maximum video frame rate of the media stream. The maximum video frame rate of the media stream is a
     * frame rate which the video should not exceed while being transmitting over the network. This method can be used
     * for limiting the video frame rate of any media stream, but the limiting effect depends on the type of the media
     * stream: setting the maximum video frame rate for a local media stream (the video of which usually consist of
     * multiple encodings) leads to filtering out from the transmission all the encodings with the higher video frame
     * rates; setting the maximum video frame rate for a remote media stream (the video of which always consists of
     * only one encoding) leads to decreasing the frame rate of the transmitted video to make it fit into the limit.
     * Though this method cannot be used for stopping the transmission of the stream's video completely — if the limit
     * cannot be satisfied, the encoding with the lowest video frame rate will be transmitted anyway. By default, the
     * maximum video frame rate of any media stream is unlimited.
     *
     * @param maxVideoFrameRate The maximum video frame rate of the media stream.
     */
    public void setMaxVideoFrameRate(int maxVideoFrameRate) {
        if (maxVideoFrameRate <= 0) {
            throw new Error("Can't change max video frame rate to `" + maxVideoFrameRate + "`");
        }
        if (this.maxVideoFrameRate != maxVideoFrameRate) {
            this.maxVideoFrameRate = maxVideoFrameRate;
            if (videoConsumers.size() > 0 && videoSupplier != null) {
                videoSupplier.addVideoConsumer(this);
            }
        }
    }

    /**
     * Returns the current maximum video bitrate of the media stream. The maximum video bitrate of the media stream is
     * a number of bits which each second of stream's video should not exceed while being transmitting over the network.
     *
     * @return The current maximum video bitrate of the media stream.
     */
    public int getMaxVideoBitrate() {
        return maxVideoBitrate;
    }

    /**
     * Sets the maximum video bitrate of the media stream. The maximum video bitrate of the media stream is a number of
     * bits which each second of stream's video should not exceed while being transmitting over the network. This
     * method can be used for limiting the maximum video bitrate of any media stream, but the limiting effect depends
     * on the type of the media stream: setting the maximum video bitrate for a local media stream (the video of which
     * usually consist of multiple encodings) leads to filtering out (from the transmission) all the encodings which do
     * not fit into the limit (starting from the most voluminous one); setting the maximum video bitrate for a remote
     * media stream (the video of which always consists of only one encoding) leads to decreasing the maximum quality
     * of the transmitted video to make it fit into the limit. Though, this method cannot be used for stopping the
     * transmission of the stream's video completely — if the limit cannot be satisfied, the video in the least
     * voluminous encoding (in case of local stream) or with the poorest quality (in case of remote stream) will be
     * transmitted anyway. By default, the maximum video bitrate of any media stream is unlimited.
     *
     * @param maxVideoBitrate The maximum video bitrate of the media stream.
     */
    public void setMaxVideoBitrate(int maxVideoBitrate) {
        if (maxVideoBitrate <= 0) {
            throw new IllegalArgumentException("Can't change max video bitrate to `" + maxVideoBitrate + "`");
        }
        if (this.maxVideoBitrate != maxVideoBitrate) {
            this.maxVideoBitrate = maxVideoBitrate;
            if (videoConsumers.size() > 0 && videoSupplier != null) {
                videoSupplier.addVideoConsumer(this);
            }
        }
    }

    /**
     * Returns the latest {@link MediaStreamAudioStatistics audio statistics} of the media stream. The statistics
     * consists of instant measures of the network connection which is currently used for transmitting the audio of the
     * media stream.
     *
     * @return The latest audio statistics of the media stream.
     */
    public MediaStreamAudioStatistics getAudioStatistics() {
        return audioStatistics;
    }

    /**
     * Returns the latest {@link MediaStreamVideoStatistics video statistics} of the media stream. The statistics
     * consists of instant measures of the network connection which is currently used for transmitting the video of the
     * media stream.
     *
     * @return The latest video statistics of the media stream.
     */
    public MediaStreamVideoStatistics getVideoStatistics() {
        return videoStatistics;
    }

    String getLabel() {
        return label;
    }

    boolean hasAudioSupplier() {
        return audioSupplier != null;
    }

    void setAudioSupplier(MediaStreamAudioSupplier audioSupplier) {
        if (this.audioSupplier != audioSupplier) {
            if (audioConsumers.size() > 0) {
                if (this.audioSupplier != null) {
                    MediaStreamAudioSupplier oldAudioSupplier = this.audioSupplier;
                    this.audioSupplier = null;
                    oldAudioSupplier.removeAudioConsumer(this);
                }
                this.audioSupplier = audioSupplier;
                if (audioSupplier != null) {
                    audioSupplier.addAudioConsumer(this);
                } else {
                    resetAudioStatistics();
                }
            } else {
                this.audioSupplier = audioSupplier;
            }
        }
    }

    boolean hasVideoSupplier() {
        return videoSupplier != null;
    }

    void setVideoSupplier(MediaStreamVideoSupplier videoSupplier) {
        if (this.videoSupplier != videoSupplier) {
            if (videoConsumers.size() > 0) {
                if (this.videoSupplier != null) {
                    MediaStreamVideoSupplier oldVideoSupplier = this.videoSupplier;
                    this.videoSupplier = null;
                    oldVideoSupplier.removeVideoConsumer(this);
                }
                this.videoSupplier = videoSupplier;
                if (videoSupplier != null) {
                    videoSupplier.addVideoConsumer(this);
                } else {
                    resetVideoStatistics();
                }
            } else {
                this.videoSupplier = videoSupplier;
            }
        }
    }

    void onAudioBuffer(MediaStreamAudioBuffer audioBuffer) {
        if (this.audioBuffer != audioBuffer) {
            this.audioBuffer = audioBuffer;
            if (this.audioBuffer != null) {
                applyVolume();
            }
            for (MediaStreamAudioConsumer consumer : audioConsumers.keySet()) {
                consumer.onAudioBuffer(audioBuffer, this);
            }
        }
    }

    void onVideoBuffer(MediaStreamVideoBuffer videoBuffer) {
        if (this.videoBuffer != videoBuffer) {
            this.videoBuffer = videoBuffer;
            for (MediaStreamVideoConsumer consumer : videoConsumers) {
                consumer.onVideoBuffer(videoBuffer, this);
            }
        }
    }

    void addAudioConsumer(MediaStreamAudioConsumer consumer, double volume) {
        if (audioConsumers.put(consumer, volume) == null) {
            if (audioConsumers.size() == 1 && audioSupplier != null) {
                audioSupplier.addAudioConsumer(this);
            } else {
                consumer.onAudioBuffer(audioBuffer, this);
            }
        }
        if (audioBuffer != null) {
            applyVolume();
        }
    }

    void removeAudioConsumer(MediaStreamAudioConsumer consumer) {
        if (audioConsumers.remove(consumer) != null) {
            consumer.onAudioBuffer(null, this);
            if (audioConsumers.size() == 0 && audioSupplier != null) {
                audioSupplier.removeAudioConsumer(this);
                resetAudioStatistics();
            }
        }
    }

    void addVideoConsumer(MediaStreamVideoConsumer consumer) {
        if (videoConsumers.add(consumer)) {
            if (videoConsumers.size() == 1 && videoSupplier != null) {
                videoSupplier.addVideoConsumer(this);
            } else {
                consumer.onVideoBuffer(videoBuffer, this);
            }
        }
    }

    void removeVideoConsumer(MediaStreamVideoConsumer consumer) {
        if (videoConsumers.remove(consumer)) {
            consumer.onVideoBuffer(null, this);
            if (videoConsumers.size() == 0 && videoSupplier != null) {
                videoSupplier.removeVideoConsumer(this);
                resetVideoStatistics();
            }
        }
    }

    void resetStatistics() {
        oldStatistics = null;
        updateStatistics(null);
    }

    void updateStatistics(List<RTCStats> report) {
        Map<String, Long> newStatistics = new HashMap<>();
        newStatistics.put("roundTripTime", 0L);
        newStatistics.put("audioBytesSent", 0L);
        newStatistics.put("audioBytesReceived", 0L);
        newStatistics.put("audioPacketsSent", 0L);
        newStatistics.put("audioPacketsReceived", 0L);
        newStatistics.put("audioPacketsLost", 0L);
        newStatistics.put("audioPacketSendDelay", 0L);
        newStatistics.put("audioJitterBufferDelay", 0L);
        newStatistics.put("audioJitterBufferEmittedCount", 0L);
        newStatistics.put("audioLevel", 0L);
        newStatistics.put("audioSamplesSent", 0L);
        newStatistics.put("audioSamplesPlayedOut", 0L);
        newStatistics.put("audioSilenceDuration", 0L);
        newStatistics.put("videoBytesSent", 0L);
        newStatistics.put("videoBytesReceived", 0L);
        newStatistics.put("videoPacketsSent", 0L);
        newStatistics.put("videoPacketsReceived", 0L);
        newStatistics.put("videoPacketsLost", 0L);
        newStatistics.put("videoPacketSendDelay", 0L);
        newStatistics.put("videoJitterBufferDelay", 0L);
        newStatistics.put("videoJitterBufferEmittedCount", 0L);
        newStatistics.put("videoWidth", 0L);
        newStatistics.put("videoHeight", 0L);
        newStatistics.put("videoFramesSent", 0L);
        newStatistics.put("videoFramesPlayedOut", 0L);
        if (oldStatistics == null) {
            long currentTimestamp = System.currentTimeMillis();
            newStatistics.put("timestamp", currentTimestamp);
            oldStatistics = newStatistics;
        } else {
            long currentTimestamp = System.currentTimeMillis();
            newStatistics.put("timestamp", currentTimestamp);
            if (report != null) {
                RTCStats selectedCandidatePair = null;
                RTCStats audioSource = null;
                RTCStats videoSource = null;
                RTCStats audioOutboundRtp = null;
                List<RTCStats> videoOutboundRtps = new ArrayList<>();
                RTCStats audioRemoteInboundRtp = null;
                List<RTCStats> videoRemoteInboundRtps = new ArrayList<>();
                RTCStats audioInboundRtp = null;
                RTCStats videoInboundRtp = null;
                for (RTCStats stats : report) {
                    switch (stats.getMembers().get("kind") + "-" + stats.getType()) {
                        case "null-candidate-pair":
                            selectedCandidatePair = stats;
                            break;
                        case "audio-media-source":
                            audioSource = stats;
                            break;
                        case "video-media-source":
                            videoSource = stats;
                            break;
                        case "audio-outbound-rtp":
                            audioOutboundRtp = stats;
                            break;
                        case "video-outbound-rtp":
                            videoOutboundRtps.add(stats);
                            break;
                        case "audio-remote-inbound-rtp":
                            audioRemoteInboundRtp = stats;
                            break;
                        case "video-remote-inbound-rtp":
                            videoRemoteInboundRtps.add(stats);
                            break;
                        case "audio-inbound-rtp":
                            audioInboundRtp = stats;
                            break;
                        case "video-inbound-rtp":
                            videoInboundRtp = stats;
                            break;
                    }
                }
                if (selectedCandidatePair != null) {
                    newStatistics.put("roundTripTime", (long) (((Number) selectedCandidatePair.getMembers().get("currentRoundTripTime")).doubleValue() * 1000));
                }
                if (Objects.equals(label, "local")) { // FIXME: We should use more robust approach for detecting local media streams
                    if (audioOutboundRtp != null) {
                        newStatistics.put("audioBytesSent", ((Number) audioOutboundRtp.getMembers().get("bytesSent")).longValue());
                        newStatistics.put("audioPacketSendDelay", 0L); // TODO: Replace with `totalPacketSendDelay` when it is added
                        newStatistics.put("audioPacketsSent", ((Number) audioOutboundRtp.getMembers().get("packetsSent")).longValue());
                    }
                    if (audioRemoteInboundRtp != null) {
                        newStatistics.put("audioPacketsLost", (long) ((Number) audioRemoteInboundRtp.getMembers().get("packetsLost")).intValue());
                    }
                    if (audioSource != null) {
                        if (audioSource.getMembers().containsKey("audioLevel")) { // FIXME: During recovery `audioSource` might have no `audioLevel` property
                            newStatistics.put("audioLevel", (long) (((Number) audioSource.getMembers().get("audioLevel")).doubleValue() * 100));
                        }
                        if (audioSource.getMembers().containsKey("totalSamplesDuration")) { // FIXME: During recovery `audioSource` might have no `totalSamplesDuration` property
                            newStatistics.put("audioSamplesSent", (long) (((Number) audioSource.getMembers().get("totalSamplesDuration")).doubleValue() * 48000));
                        }
                    }
                    for (int i = 0; i < videoOutboundRtps.size(); i++) {
                        RTCStats videoOutboundRtp = videoOutboundRtps.get(i);
                        newStatistics.put("videoBytesSent", newStatistics.get("videoBytesSent") + ((Number) videoOutboundRtp.getMembers().get("bytesSent")).longValue());
                        newStatistics.put("videoPacketSendDelay", newStatistics.get("videoPacketSendDelay") + (long) (((Number) videoOutboundRtp.getMembers().get("totalPacketSendDelay")).doubleValue() * 1000));
                        newStatistics.put("videoPacketsSent", newStatistics.get("videoPacketsSent") + ((Number) videoOutboundRtp.getMembers().get("packetsSent")).longValue());
                        newStatistics.put("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc"), ((Number) videoOutboundRtp.getMembers().get("framesSent")).longValue());
                        if (!oldStatistics.containsKey("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc"))) {
                            oldStatistics.put("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc"), 0L);
                        }
                        if (newStatistics.get("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc")) - oldStatistics.get("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc")) > 0) {
                            if (videoOutboundRtp.getMembers().containsKey("frameWidth") && videoOutboundRtp.getMembers().containsKey("frameHeight") && newStatistics.get("videoWidth") < (long) ((Number) videoOutboundRtp.getMembers().get("frameWidth")).intValue() && newStatistics.get("videoHeight") < (long) ((Number) videoOutboundRtp.getMembers().get("frameHeight")).intValue()) {
                                newStatistics.put("videoWidth", (long) ((Number) videoOutboundRtp.getMembers().get("frameWidth")).intValue());
                                newStatistics.put("videoHeight", (long) ((Number) videoOutboundRtp.getMembers().get("frameHeight")).intValue());
                                newStatistics.put("videoFramesSent", newStatistics.get("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc")));
                                oldStatistics.put("videoFramesSent", oldStatistics.get("videoFramesSent" + videoOutboundRtp.getMembers().get("ssrc")));
                            }
                        }
                    }
                    for (RTCStats videoRemoteInboundRtp : videoRemoteInboundRtps) {
                        newStatistics.put("videoPacketsLost", newStatistics.get("videoPacketsLost") + (long) ((Number) videoRemoteInboundRtp.getMembers().get("packetsLost")).intValue());
                    }
                } else { // !Objects.equals(label, "local")
                    if (audioInboundRtp != null) {
                        newStatistics.put("audioBytesReceived", ((Number) audioInboundRtp.getMembers().get("bytesReceived")).longValue());
                        newStatistics.put("audioPacketsReceived", ((Number) audioInboundRtp.getMembers().get("packetsReceived")).longValue());
                        newStatistics.put("audioPacketsLost", (long) ((Number) audioInboundRtp.getMembers().get("packetsLost")).intValue());
                        newStatistics.put("audioJitterBufferDelay", (long) (((Number) audioInboundRtp.getMembers().get("jitterBufferDelay")).doubleValue() * 1000));
                        newStatistics.put("audioJitterBufferEmittedCount", ((Number) audioInboundRtp.getMembers().get("jitterBufferEmittedCount")).longValue());
                        newStatistics.put("audioLevel", (long) (((Number) audioInboundRtp.getMembers().get("audioLevel")).doubleValue() * 100));
                        newStatistics.put("audioSamplesPlayedOut", ((Number) audioInboundRtp.getMembers().get("totalSamplesReceived")).longValue() - ((Number) audioInboundRtp.getMembers().get("concealedSamples")).longValue());
                        newStatistics.put("audioSilenceDuration", ((Number) audioInboundRtp.getMembers().get("silentConcealedSamples")).longValue() / 48);
                    }
                    if (videoInboundRtp != null) {
                        newStatistics.put("videoBytesReceived", ((Number) videoInboundRtp.getMembers().get("bytesReceived")).longValue());
                        newStatistics.put("videoPacketsReceived", ((Number) videoInboundRtp.getMembers().get("packetsReceived")).longValue());
                        newStatistics.put("videoPacketsLost", (long) ((Number) videoInboundRtp.getMembers().get("packetsLost")).intValue());
                        newStatistics.put("videoJitterBufferDelay", (long) (((Number) videoInboundRtp.getMembers().get("jitterBufferDelay")).doubleValue() * 1000));
                        newStatistics.put("videoJitterBufferEmittedCount", ((Number) videoInboundRtp.getMembers().get("jitterBufferEmittedCount")).longValue());
                        if (videoInboundRtp.getMembers().containsKey("frameWidth")) {
                            newStatistics.put("videoWidth", (long) ((Number) videoInboundRtp.getMembers().get("frameWidth")).intValue());
                        }
                        if (videoInboundRtp.getMembers().containsKey("frameHeight")) {
                            newStatistics.put("videoHeight", (long) ((Number) videoInboundRtp.getMembers().get("frameHeight")).intValue());
                        }
                        newStatistics.put("videoFramesPlayedOut", ((Number) videoInboundRtp.getMembers().get("framesReceived")).longValue() - ((Number) videoInboundRtp.getMembers().get("framesDropped")).longValue());
                    }
                }
            }
            double dT = (newStatistics.get("timestamp") - oldStatistics.get("timestamp")) / 1000.0;
            MediaStreamAudioStatistics audioStatistics = new MediaStreamAudioStatistics(0, 0, 0, 0, 0, 0);
            MediaStreamVideoStatistics videoStatistics = new MediaStreamVideoStatistics(0, 0, 0, 0, 0, 0, 0);
            if (audioBuffer != null) {
                if (Objects.equals(label, "local")) { // FIXME: We should use more robust approach for detecting local media streams
                    audioStatistics = new MediaStreamAudioStatistics(newStatistics.get("roundTripTime").intValue(),
                                                                     (newStatistics.get("audioBytesSent") - oldStatistics.get("audioBytesSent")) * 8.0 / dT,
                                                                     (newStatistics.get("audioPacketSendDelay") - oldStatistics.get("audioPacketSendDelay")) * 1.0 / (newStatistics.get("audioPacketsSent") - oldStatistics.get("audioPacketsSent")),
                                                                     (newStatistics.get("audioPacketsLost") - oldStatistics.get("audioPacketsLost")) * 100.0 / (newStatistics.get("audioPacketsSent") - oldStatistics.get("audioPacketsSent") + newStatistics.get("audioPacketsLost") - oldStatistics.get("audioPacketsLost")),
                                                                     newStatistics.get("audioLevel").intValue(),
                                                                     (newStatistics.get("audioSamplesSent") - oldStatistics.get("audioSamplesSent")) / dT);
                } else {
                    audioStatistics = new MediaStreamAudioStatistics(newStatistics.get("roundTripTime").intValue(),
                                                                     (newStatistics.get("audioBytesReceived") - oldStatistics.get("audioBytesReceived")) * 8.0 / dT,
                                                                     (newStatistics.get("audioJitterBufferDelay") - oldStatistics.get("audioJitterBufferDelay")) * 1.0 / (newStatistics.get("audioJitterBufferEmittedCount") - oldStatistics.get("audioJitterBufferEmittedCount")),
                                                                     (newStatistics.get("audioPacketsLost") - oldStatistics.get("audioPacketsLost")) * 100.0 / (newStatistics.get("audioPacketsReceived") - oldStatistics.get("audioPacketsReceived") + newStatistics.get("audioPacketsLost") - oldStatistics.get("audioPacketsLost")),
                                                                     newStatistics.get("audioLevel").intValue(),
                                                                     (newStatistics.get("audioSamplesPlayedOut") - oldStatistics.get("audioSamplesPlayedOut")) / dT);
                }
            }
            if (videoBuffer != null) {
                if (Objects.equals(label, "local")) {  // FIXME: We should use more robust approach for detecting local media streams
                    videoStatistics = new MediaStreamVideoStatistics(newStatistics.get("roundTripTime").intValue(),
                                                                     (newStatistics.get("videoBytesSent") - oldStatistics.get("videoBytesSent")) * 8.0 / dT,
                                                                     (newStatistics.get("videoPacketSendDelay") - oldStatistics.get("videoPacketSendDelay")) * 1.0 / (newStatistics.get("videoPacketsSent") - oldStatistics.get("videoPacketsSent")),
                                                                     (newStatistics.get("videoPacketsLost") - oldStatistics.get("videoPacketsLost")) * 100.0 / (newStatistics.get("videoPacketsSent") - oldStatistics.get("videoPacketsSent") + newStatistics.get("videoPacketsLost") - oldStatistics.get("videoPacketsLost")),
                                                                     newStatistics.get("videoWidth").intValue(),
                                                                     newStatistics.get("videoHeight").intValue(),
                                                                     (newStatistics.get("videoFramesSent") - oldStatistics.get("videoFramesSent")) / dT);
                } else {
                    videoStatistics = new MediaStreamVideoStatistics(newStatistics.get("roundTripTime").intValue(),
                                                                     (newStatistics.get("videoBytesReceived") - oldStatistics.get("videoBytesReceived")) * 8.0 / dT,
                                                                     (newStatistics.get("videoJitterBufferDelay") - oldStatistics.get("videoJitterBufferDelay")) * 1.0 / (newStatistics.get("videoJitterBufferEmittedCount") - oldStatistics.get("videoJitterBufferEmittedCount")),
                                                                     (newStatistics.get("videoPacketsLost") - oldStatistics.get("videoPacketsLost")) * 100.0 / (newStatistics.get("videoPacketsReceived") - oldStatistics.get("videoPacketsReceived") + newStatistics.get("videoPacketsLost") - oldStatistics.get("videoPacketsLost")),
                                                                     newStatistics.get("videoWidth").intValue(),
                                                                     newStatistics.get("videoHeight").intValue(),
                                                                     (newStatistics.get("videoFramesPlayedOut") - oldStatistics.get("videoFramesPlayedOut")) / dT);
                }
            }
            if (audioSupplier != null && audioConsumers.size() > 0) {
                this.audioStatistics = audioStatistics;
            }
            if (videoSupplier != null && videoConsumers.size() > 0) {
                this.videoStatistics = videoStatistics;
            }
            oldStatistics = newStatistics;
        }
    }

    private void resetAudioStatistics() {
        this.audioStatistics = new MediaStreamAudioStatistics(0, 0, 0, 0, 0, 0);
    }

    private void resetVideoStatistics() {
        this.videoStatistics = new MediaStreamVideoStatistics(0, 0, 0, 0, 0, 0, 0);
    }

    private void applyVolume() {
        double maxVolume = 0.0;
        for (double volume : audioConsumers.values()) {
            if (volume > maxVolume) {
                maxVolume = volume;
            }
        }
        audioBuffer.getTrack().setVolume(maxVolume);
    }

}
