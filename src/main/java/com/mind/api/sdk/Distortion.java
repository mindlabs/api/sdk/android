package com.mind.api.sdk;

class Distortion {

    private final String mtid;
    private final long timestamp;
    private int duration;

    Distortion(String mtid) {
        this.mtid = mtid;
        this.timestamp = System.currentTimeMillis() - 1000; // A distortion is always reported with one-second delay
        this.duration = 1000;
    }

    boolean isEnded() {
        return System.currentTimeMillis() - (timestamp + duration) > 1500;
    }

    boolean prolong() {
        if (!isEnded()) {
            duration = (int) (System.currentTimeMillis() - timestamp);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "1:" + mtid + ":" + timestamp + ":" + Math.round(duration / 1000.0);
    }

}
