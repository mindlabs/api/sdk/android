package com.mind.api.sdk;

class WebRtcSubscription {

    private WebRtcTransceiver audioTransceiver;
    private WebRtcTransceiver videoTransceiver;
    private MediaStream audioConsumer;
    private MediaStream videoConsumer;

    WebRtcSubscription() {}

    WebRtcTransceiver getAudioTransceiver() {
        return audioTransceiver;
    }

    void setAudioTransceiver(WebRtcTransceiver audioTransceiver) {
        this.audioTransceiver = audioTransceiver;
    }

    WebRtcTransceiver getVideoTransceiver() {
        return videoTransceiver;
    }

    void setVideoTransceiver(WebRtcTransceiver videoTransceiver) {
        this.videoTransceiver = videoTransceiver;
    }

    MediaStream getAudioConsumer() {
        return audioConsumer;
    }

    void setAudioConsumer(MediaStream audioConsumer) {
        this.audioConsumer = audioConsumer;
    }

    MediaStream getVideoConsumer() {
        return videoConsumer;
    }

    void setVideoConsumer(MediaStream videoConsumer) {
        this.videoConsumer = videoConsumer;
    }

}
