package com.mind.api.sdk;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import androidx.annotation.UiThread;
import org.webrtc.EglBase;
import org.webrtc.GlRectDrawer;
import org.webrtc.RendererCommon;
import org.webrtc.SurfaceEglRenderer;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSink;

public class Video extends SurfaceView implements SurfaceHolder.Callback, VideoSink, RendererCommon.RendererEvents, MediaStreamAudioConsumer, MediaStreamVideoConsumer {

    private final RendererCommon.VideoLayoutMeasure videoLayoutMeasure = new RendererCommon.VideoLayoutMeasure();
    private final SurfaceEglRenderer eglRenderer;

    private int rotatedFrameWidth;
    private int rotatedFrameHeight;

    private MediaStream stream;
    private MediaStreamVideoBuffer videoBuffer;
    private double volume = 1.0;

    @UiThread
    public Video(Context context) {
        this(context, null);
    }

    @UiThread
    public Video(Context context, AttributeSet attributes) {
        super(context, attributes);
        eglRenderer = new SurfaceEglRenderer(getId() == View.NO_ID ? "" : getResources().getResourceEntryName(getId()));
        getHolder().addCallback(this);
        getHolder().addCallback(eglRenderer);
    }

    /**
     * Sets {@code MediaStream} that should be used as a source of audio and video for the {@code Video}. The
     * {@code null} value can be used for stopping playing previously set {@code MediaStream}.
     */
    @UiThread
    public void setMediaStream(MediaStream stream) {
        if (this.stream != stream) {
            if (this.stream != null) {
                this.stream.removeAudioConsumer(this);
                this.stream.removeVideoConsumer(this);
            }
            if (stream == null) {
                if (this.stream != null) {
                    release();
                    this.stream = null;
                }
            } else {
                if (this.stream == null) {
                    prepare();
                }
                this.stream = stream;
                stream.addAudioConsumer(this, volume);
                stream.addVideoConsumer(this);
            }
        }
    }

    /**
     * Sets the volume of the audio. The volume can be in range between 0.0 and 1.0, inclusively, where 0.0 means the
     * absolute silence, and 1.0 means the highest volume. The default volume is 1.0.
     *
     * @param volume The volume of the audio.
     */
    @UiThread
    public void setVolume(double volume) {
        this.volume = volume;
        if (stream != null) {
            stream.addAudioConsumer(this, volume);
        }
    }

    /**
     * Enables or disables video mirroring.
     */
    @UiThread
    public void setMirror(boolean mirror) {
        eglRenderer.setMirror(mirror);
    }

    /**
     * Configures how the video will fill the allowed layout area.
     */
    @UiThread
    public void setScalingType(RendererCommon.ScalingType scalingType) {
        videoLayoutMeasure.setScalingType(scalingType);
        requestLayout();
    }

    @Override
    public void onAudioBuffer(MediaStreamAudioBuffer audioBuffer, MediaStream supplier) {}

    @Override
    public void onVideoBuffer(MediaStreamVideoBuffer videoBuffer, MediaStream supplier) {
        if (this.videoBuffer != videoBuffer) {
            if (this.videoBuffer != null) {
                this.videoBuffer.getTrack().removeSink(this);
            }
            this.videoBuffer = videoBuffer;
            if (this.videoBuffer != null) {
                this.videoBuffer.getTrack().addSink(this);
            }
        }
    }

    @Override
    public void onFrame(VideoFrame frame) {
        eglRenderer.onFrame(frame);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        Point size = videoLayoutMeasure.measure(widthSpec, heightSpec, rotatedFrameWidth, rotatedFrameHeight);
        setMeasuredDimension(size.x, size.y);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        eglRenderer.setLayoutAspectRatio((right - left) / (float) (bottom - top));
        getHolder().setSizeFromLayout();
    }

    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        getHolder().setSizeFromLayout();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {}

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    @Override
    public void onFirstFrameRendered() {}

    @Override
    public void onFrameResolutionChanged(int videoWidth, int videoHeight, int rotation) {
        post(() -> {
            rotatedFrameWidth = rotation == 0 || rotation == 180 ? videoWidth : videoHeight;
            rotatedFrameHeight = rotation == 0 || rotation == 180 ? videoHeight : videoWidth;
            getHolder().setSizeFromLayout();
            requestLayout();
        });
    }

    private void prepare() {
        rotatedFrameWidth = 0;
        rotatedFrameHeight = 0;
        eglRenderer.init(MindSDK.getEglBaseContext(), this, EglBase.CONFIG_PLAIN, new GlRectDrawer());
    }

    private void release() {
        eglRenderer.release();
    }

}
