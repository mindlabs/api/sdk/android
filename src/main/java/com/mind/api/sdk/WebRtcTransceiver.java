package com.mind.api.sdk;

import org.webrtc.MediaStreamTrack;
import org.webrtc.RtpCapabilities;
import org.webrtc.RtpParameters;
import org.webrtc.RtpTransceiver;

import java.util.List;

class WebRtcTransceiver {

    private final RtpTransceiver transceiver;
    private final MediaStreamTrack.MediaType mediaType;
    private final int sdpSectionIndex;

    WebRtcTransceiver(RtpTransceiver transceiver, MediaStreamTrack.MediaType mediaType, int sdpSectionIndex) {
        this.transceiver = transceiver;
        this.mediaType = mediaType;
        this.sdpSectionIndex = sdpSectionIndex;
    }

    MediaStreamTrack.MediaType getMediaType() {
        return mediaType;
    }

    int getSdpSectionIndex() {
        return sdpSectionIndex;
    }

    RtpTransceiver.RtpTransceiverDirection getDirection() {
        return transceiver.getDirection();
    }

    void setDirection(RtpTransceiver.RtpTransceiverDirection direction) {
        transceiver.setDirection(direction);
    }

    MediaStreamTrack getReceivingTrack() {
        return transceiver.getReceiver().track();
    }

    MediaStreamTrack getSendingTrack() {
        return transceiver.getSender().track();
    }

    void setSendingTrack(MediaStreamTrack track) {
        transceiver.getSender().setTrack(track, false);
    }

    RtpParameters getSendingParameters() {
        return transceiver.getSender().getParameters();
    }

    void setSendingParameters(RtpParameters parameters) {
        transceiver.getSender().setParameters(parameters);
    }

    void setCodecPreferences(List<RtpCapabilities.CodecCapability> codecs) {
        transceiver.setCodecPreferences(codecs);
    }

}
