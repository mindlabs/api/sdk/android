package com.mind.api.sdk;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import org.webrtc.Camera2Capturer;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerationAndroid.CaptureFormat;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.EglBase;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Camera class is used for representing all cameras of the Android device. Android OS doesn't allow capturing multiple
 * cameras simultaneously, that's why Mind Android SDK represents all cameras with a single {@link
 * Camera#setFacing(CameraFacing) multi-facing} instance of Camera class that can be got with {@link
 * DeviceRegistry#getCamera() getCamera} method of {@link DeviceRegistry} class. Camera class implements {@link
 * MediaStreamVideoSupplier} interface, so it can be used as a source of video for local {@link MediaStream}.
 *
 * <pre>
 * DeviceRegistry deviceRegistry = MindSDK.getDeviceRegistry();
 * Camera camera = deviceRegistry.getCamera();
 * camera.setFacing(CameraFacing.USER);
 * camera.setResolution(1280, 720);
 * camera.setFps(25);
 * camera.setBitrate(2500000);
 * camera.setAdaptivity(3);
 * MediaStream myStream = MindSDK.createMediaStream(null, camera);
 * me.setMediaStream(myStream);
 * camera.acquire().exceptionally((exception) -> {
 *     Log.e("MyApplication", "Camera can't be acquired", exception);
 *     return null;
 * });
 * </pre>
 */
@MainThread
public class Camera implements MediaStreamVideoSupplier {

    private final Set<MediaStream> consumers = new HashSet<>();

    private final Application application;
    private final PeerConnectionFactory peerConnectionFactory;
    private final EglBase.Context eglBaseContext;
    private final Camera2Enumerator cameraEnumerator;

    private SurfaceTextureHelper surfaceTextureHelper;
    private VideoSource videoSource;
    private Camera2Capturer cameraCapturer;
    private VideoTrack videoTrack;
    private CompletableFuture<Void> acquiringFeature;

    private CameraFacing facing = CameraFacing.USER;
    private int width = 640;
    private int height = 360;
    private int fps = 30;
    private int bitrate = 1000000;
    private int adaptivity = 2;
    private double scale = 1.0;

    Camera(Application application, PeerConnectionFactory peerConnectionFactory, EglBase.Context eglBaseContext) {
        this.application = application;
        this.peerConnectionFactory = peerConnectionFactory;
        this.eglBaseContext = eglBaseContext;
        this.cameraEnumerator = new Camera2Enumerator(application);
    }

    /**
     * Sets the facing of the camera. The facing of the camera is a direction which the camera can be pointed to. Front
     * and back cameras on smartphones (and other mobile devices) are usually combined into a single multi-facing
     * camera which is represented with a single instance of `Camera` class. The facing of any multi-facing camera can
     * be changed at any moment regardless whether the camera is acquired or not.
     *
     * @param facing The facing of the camera.
     */
    public void setFacing(CameraFacing facing) {
        if (facing != CameraFacing.USER && facing != CameraFacing.ENVIRONMENT) {
            throw new IllegalArgumentException("Invalid facing value: " + facing);
        }
        if (this.facing != facing) {
            this.facing = facing;
            if (videoTrack != null) {
                cameraCapturer.switchCamera(new CameraVideoCapturer.CameraSwitchHandler() {

                    @Override
                    public void onCameraSwitchDone(boolean facingFront) {
                        new Handler(Looper.getMainLooper()).post(() -> { // TODO: Main looper shouldn't be used directly
                            if (cameraCapturer != null) {
                                if (Camera.this.facing == (facingFront ? CameraFacing.USER : CameraFacing.ENVIRONMENT)) {
                                    CaptureFormat format = chooseCaptureFormat();
                                    cameraCapturer.changeCaptureFormat(format.width, format.height, fps);
                                    fireVideoBuffer();
                                } else {
                                    cameraCapturer.switchCamera(this);
                                }
                            }
                        });
                    }

                    @Override
                    public void onCameraSwitchError(String errorDescription) {}

                });
            }
        }
    }

    /**
     * Sets the resolution of the camera. The resolution of the camera is a resolution which the camera should capture
     * the video in. The video from the camera can be transmitted over the network in {@link Camera#setAdaptivity(int)
     * multiple encodings (e.g. resolutions) simultaneously}. The resolution can be changed at any moment regardless
     * whether the camera is acquired or not.
     *
     * @param width The horizontal resolution of the camera.
     * @param height The vertical resolution of the camera.
     */
    public void setResolution(int width, int height) {
        if (width <= 0 || width % 4 != 0) {
            throw new IllegalArgumentException("Invalid width value: " + width);
        }
        if (height <= 0 || height % 4 != 0) {
            throw new IllegalArgumentException("Invalid height value: " + height);
        }
        if (this.width != width || this.height != height) {
            this.width = width;
            this.height = height;
            if (videoTrack != null) {
                CaptureFormat format = chooseCaptureFormat();
                cameraCapturer.changeCaptureFormat(format.width, format.height, fps);
                fireVideoBuffer();
            }
        }
    }

    /**
     * Sets the frame rate of the camera. The frame rate of the camera is a rate which the camera should capture the
     * video at. The frame rate can be changed at any moment regardless whether the camera is acquired or not.
     *
     * @param fps The frame rate of the camera.
     */
    public void setFps(int fps) {
        if (fps <= 0) {
            throw new IllegalArgumentException("Invalid fps value: " + fps);
        }
        if (this.fps != fps) {
            this.fps = fps;
            if (videoTrack != null) {
                CaptureFormat format = chooseCaptureFormat();
                cameraCapturer.changeCaptureFormat(format.width, format.height, fps);
                fireVideoBuffer();
            }
        }
    }

    /**
     * Sets the bitrate of the camera. The bitrate of the camera is a number of bits which each second of video from
     * the camera should not exceed while being transmitting over the network. The bitrate is shared among all
     * {@link Camera#setAdaptivity(int) encodings} proportionally and can be changed at any moment regardless whether
     * the camera is acquired or not.
     *
     * @param bitrate The bitrate of the camera.
     */
    public void setBitrate(int bitrate) {
        if (bitrate <= 100000) {
            throw new Error("Can't change bitrate to `" + bitrate + "`");
        }
        if (this.bitrate != bitrate) {
            this.bitrate = bitrate;
            if (videoTrack != null) {
                fireVideoBuffer();
            }
        }
    }

    /**
     * Sets the adaptivity of the camera. The adaptivity of the camera is an integer (in range between 1 and 3,
     * inclusively) which defines a number of encodings in which the video from the camera should be transmitted over
     * the network. The encodings differ in resolution: the resolution of the first encoding equals the {@link
     * Camera#setResolution(int, int) resolution of the camera}, the resolution of the second encoding is half (in each
     * dimension) of the resolution of the first one, the resolution of the third encoding is half (in each dimension)
     * of the resolution of the second one. For example, if the resolution of the camera is 1280x720 and adaptivity is
     * 3, the video from the camera would be transmitted over the network in 3 encoding: 1280x720, 640x360 and 320x180.
     * The adaptivity can be changed at any moment regardless whether the camera is acquired or not.
     *
     * @param adaptivity The adaptivity of the camera.
     */
    public void setAdaptivity(int adaptivity) {
        if (adaptivity < 1 || adaptivity > 3) {
            throw new Error("Can't change adaptivity to `" + adaptivity + "`");
        }
        if (this.adaptivity != adaptivity) {
            this.adaptivity = adaptivity;
            if (videoTrack != null) {
                fireVideoBuffer();
            }
        }
    }

    /**
     * Starts camera capturing. This is an asynchronous operation which assumes acquiring the underlying camera device
     * and distributing camera's video among all {@link MediaStream consumers}. This method returns a {@link
     * CompletableFuture} that completes (on the main thread) with either no value (if the camera capturing starts
     * successfully) or an exception (if there is no permission to access the camera or if the camera was unplugged).
     * If the camera capturing has been already started, this method returns already completed {@link
     * CompletableFuture}.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> acquire() {
        if (acquiringFeature == null) {
            if (application.checkPermission(Manifest.permission.CAMERA, android.os.Process.myPid(), Process.myUid()) != PackageManager.PERMISSION_GRANTED) {
                return CompletableFuture.failedFuture(new SecurityException("Can't acquire camera: permission denied"));
            }
            surfaceTextureHelper = SurfaceTextureHelper.create(UUID.randomUUID().toString(), eglBaseContext);
            videoSource = peerConnectionFactory.createVideoSource(false);
            cameraCapturer = new Camera2Capturer(application, getCameraName(), null);
            cameraCapturer.initialize(surfaceTextureHelper, application, videoSource.getCapturerObserver());
            CaptureFormat captureFormat = chooseCaptureFormat();
            cameraCapturer.startCapture(captureFormat.width, captureFormat.height, fps);
            videoTrack = peerConnectionFactory.createVideoTrack(UUID.randomUUID().toString(), videoSource);
            fireVideoBuffer();
            acquiringFeature = CompletableFuture.completedFuture(null);
        }
        return acquiringFeature;
    }

    /**
     * Stops camera capturing. This is a synchronous operation which assumes revoking the previously distributed
     * camera's video and releasing the underlying camera device. The stopping is idempotent: the method does nothing
     * if the camera is not acquired, but it would fail if it was called in the middle of acquisition.
     */
    public void release() {
        if (acquiringFeature != null && videoTrack == null) {
            throw new IllegalStateException("Can't release camera in the middle of acquisition");
        }
        if (videoTrack != null) {
            for (MediaStream consumer : consumers) {
                consumer.onVideoBuffer(null);
            }
            videoTrack.dispose();
            videoTrack = null;
            cameraCapturer.dispose();
            cameraCapturer = null;
            videoSource.dispose();
            videoSource = null;
            surfaceTextureHelper.dispose();
            surfaceTextureHelper = null;
            scale = 1.0;
        }
        acquiringFeature = null;
    }

    @Override
    public void addVideoConsumer(MediaStream consumer) {
        consumers.add(consumer);
        if (videoTrack != null) {
            consumer.onVideoBuffer(new MediaStreamVideoBuffer(videoTrack, false, width, height, bitrate, adaptivity, scale));
        } else {
            consumer.onVideoBuffer(null);
        }
    }

    @Override
    public void removeVideoConsumer(MediaStream consumer) {
        if (consumers.remove(consumer)) {
            consumer.onVideoBuffer(null);
        }
    }

    private void fireVideoBuffer() {
        for (MediaStream consumer : consumers) {
            consumer.onVideoBuffer(new MediaStreamVideoBuffer(videoTrack, false, width, height, bitrate, adaptivity, scale));
        }
    }

    private String getCameraName() {
        String[] cameraNames = cameraEnumerator.getDeviceNames();
        for (String cameraName : cameraNames) {
            if (facing == CameraFacing.USER && cameraEnumerator.isFrontFacing(cameraName) || facing == CameraFacing.ENVIRONMENT && cameraEnumerator.isBackFacing(cameraName)) {
                return cameraName;
            }
        }
        return cameraEnumerator.getDeviceNames()[0];
    }

    private CaptureFormat chooseCaptureFormat() {
        List<CaptureFormat> formats = cameraEnumerator.getSupportedFormats(getCameraName());
        CaptureFormat chosenFormat = formats.get(0);
        for (CaptureFormat format : formats) {
            if ((double) format.width / format.height == (double) width / height && width <= format.width && height <= format.height && format.framerate.min <= fps && fps <= format.framerate.max) {
                // FIXME: We can choose a format only if its width and height exactly match `this.width` and
                // `this.height` or if its width and height are dividable by (2 ^ number_of_simulcast_layers) (i.e. by 8
                // in our case) without a remainder. Otherwise, WebRTC library could fail silently during initialization
                // of VP8 encoder, because `EncoderStreamFactory::CreateEncoderStreams` would produce streams with
                // wrong resolutions, so that `SimulcastUtility::ValidSimulcastParameters` would fail at
                // <https://webrtc.googlesource.com/src/+/master/modules/video_coding/utility/simulcast_utility.cc#47>
                // line.
                if (format.width == width && format.height == height || format.width % 8 == 0 && format.height % 8 == 0) {
                    chosenFormat = format;
                }
            }
        }
        scale = (double) chosenFormat.height / height;
        return chosenFormat;
    }

}
