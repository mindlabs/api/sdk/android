package com.mind.api.sdk;

import android.app.Application;

/**
 * MindSDKOptions class represents all available configuration options for Mind Android SDK. The default constructor
 * creates an instance of MindSDKOptions class with the default values for all configuration options. If necessary, you
 * can change any default value before passing the instance to the static {@link MindSDK#initialize(MindSDKOptions)
 * initialize} method of {@link MindSDK} class:
 *
 * <pre>
 * public class MyApplication extends Application {
 *
 *     &#064;Override
 *     public void onCreate() {
 *         super.onCreate();
 *         MindSDKOptions options = new MindSDKOptions(this);
 *         options.setUseVp9ForSendingVideo(true);
 *         MindSDK.initialize(options);
 *     }
 *
 * }
 * </pre>
 */
public class MindSDKOptions {

    private final Application application;
    private boolean useVp9ForSendingVideo;

    public MindSDKOptions(Application application) {
        this.application = application;
        this.useVp9ForSendingVideo = false;
    }

    MindSDKOptions(MindSDKOptions options) {
        this.application = options.getApplication();
        this.useVp9ForSendingVideo = options.isUseVp9ForSendingVideo();
    }

    Application getApplication() {
        return application;
    }

    boolean isUseVp9ForSendingVideo() {
        return useVp9ForSendingVideo;
    }

    /**
     * Sets whether VP9 codec should be used for sending video or not. If `true` then any outgoing video will be
     * encoded with VP9 in SVC mode, otherwise — with VP8 in simulcast mode. The default value is `false`.
     *
     * @param useVp9ForSendingVideo Whether VP9 codec should be used for sending video or not.
     */
    public void setUseVp9ForSendingVideo(boolean useVp9ForSendingVideo) {
        this.useVp9ForSendingVideo = useVp9ForSendingVideo;
    }

}
