package com.mind.api.sdk;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Process;
import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnectionFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Microphone class is used for representing the microphone of the Android device. The instance of Microphone class
 * that represent the built-in microphone can be got with {@link DeviceRegistry#getMicrophone() getMicrophone}
 * method of {@link DeviceRegistry} class. Microphone class implements {@link MediaStreamAudioSupplier} interface,
 * so it can be used as a source of audio for local {@link MediaStream}.
 *
 * <pre>
 * DeviceRegistry deviceRegistry = MindSDK.getDeviceRegistry();
 * Microphone microphone = deviceRegistry.getMicrophone();
 * MediaStream myStream = MindSDK.createMediaStream(microphone, null);
 * me.setMediaStream(myStream);
 * microphone.acquire().exceptionally((exception) -> {
 *     Log.e("MyApplication", "Microphone can't be acquired", exception);
 *     return null;
 * });
 * </pre>
 */
@MainThread
public class Microphone implements MediaStreamAudioSupplier {

    private final Set<MediaStream> consumers = new HashSet<>();

    private final Application application;
    private final PeerConnectionFactory peerConnectionFactory;

    private AudioSource audioSource;
    private AudioTrack audioTrack;
    private CompletableFuture<Void> acquiringFeature;

    private boolean muted;

    Microphone(Application application, PeerConnectionFactory peerConnectionFactory) {
        this.application = application;
        this.peerConnectionFactory = peerConnectionFactory;
    }

    /**
     * Sets the muted state of the microphone. The muted state of the microphone determines whether the microphone
     * produces an actual audio (if it is unmuted) or silence (if it is muted). The muted state can be changed at any
     * moment regardless whether the microphone is acquired or not.
     *
     * @param muted The muted state of the microphone.
     */
    public void setMuted(boolean muted) {
        if (this.muted != muted) {
            this.muted = muted;
            if (audioTrack != null) {
                audioTrack.setEnabled(!muted);
            }
        }
    }

    /**
     * Starts microphone recording. This is an asynchronous operation which assumes acquiring the underlying microphone
     * device and distributing microphone's audio among all {@link MediaStream consumers}. This method returns a {@link
     * CompletableFuture} that completes (on the main thread) with either no value (if the microphone recording starts
     * successfully) or an exception (if there is no permission to access the microphone or if the microphone was
     * unplugged). If the microphone recording has been already started, this method returns already completed {@link
     * CompletableFuture}.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> acquire() {
        if (acquiringFeature == null) {
            if (application.checkPermission(Manifest.permission.RECORD_AUDIO, android.os.Process.myPid(), Process.myUid()) != PackageManager.PERMISSION_GRANTED) {
                return CompletableFuture.failedFuture(new SecurityException("Can't acquire microphone: permission denied"));
            }
            audioSource = peerConnectionFactory.createAudioSource(new MediaConstraints());
            audioTrack = peerConnectionFactory.createAudioTrack(UUID.randomUUID().toString(), audioSource);
            audioTrack.setEnabled(!muted);
            fireAudioBuffer();
            acquiringFeature = CompletableFuture.completedFuture(null);
        }
        return acquiringFeature;
    }

    /**
     * Stops microphone recording. This is a synchronous operation which assumes revoking the previously distributed
     * microphone's audio and releasing the underlying microphone device. The stopping is idempotent: the method does
     * nothing if the microphone is not acquired, but it would fail if it was called in the middle of acquisition.
     */
    public void release() {
        if (acquiringFeature != null && audioTrack == null) {
            throw new IllegalStateException("Can't release microphone in the middle of acquisition");
        }
        if (audioTrack != null) {
            for (MediaStream consumer : consumers) {
                consumer.onAudioBuffer(null);
            }
            audioTrack.dispose();
            audioTrack = null;
            audioSource.dispose();
            audioSource = null;
        }
        acquiringFeature = null;
    }

    @Override
    public void addAudioConsumer(MediaStream consumer) {
        consumers.add(consumer);
        if (audioTrack != null) {
            consumer.onAudioBuffer(new MediaStreamAudioBuffer(audioTrack, false));
        } else {
            consumer.onAudioBuffer(null);
        }
    }

    @Override
    public void removeAudioConsumer(MediaStream consumer) {
        if (consumers.remove(consumer)) {
            consumer.onAudioBuffer(null);
        }
    }

    private void fireAudioBuffer() {
        for (MediaStream consumer : consumers) {
            consumer.onAudioBuffer(new MediaStreamAudioBuffer(audioTrack, false));
        }
    }

}
