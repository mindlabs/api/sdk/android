package com.mind.api.sdk;

import androidx.annotation.AnyThread;

/**
 * MediaStreamAudioStatistics class is used for representing audio statistics of a {@link MediaStream media stream}.
 * The statistics consists of instant measures of the network connection which is currently used for transmitting the
 * audio. If no audio is transmitted at the moment or if the media stream does not have an audio at all or yet, the
 * values of all measures are reset to zeros, otherwise the values of all measures are updated about once a second. You
 * can always get the latest audio statistics for any media stream with a help of
 * {@link MediaStream#getAudioStatistics() getAudioStatistics} method:
 *
 * <pre>
 * // We assume that microphone and camera have been already acquired
 * MediaStream myStream = MindSDK.createMediaStream(microphone, camera);
 * me.setMediaStream(myStream);
 * MediaStreamAudioStatistics myStreamAudioStatistics = myStream.getAudioStatistics();
 *
 * ...
 *
 * Video participantVideo = findViewById(R.id.participantVideo);
 * Participant participant = conference.getParticipantById("&lt;PARTICIPANT_ID&gt;");
 * if (participant != null) {
 *     MediaStream participantStream = participant.getMediaStream();
 *     participantVideo.setMediaStream(participantStream);
 *     MediaStreamAudioStatistics participantStreamAudioStatistics = participantStream.getAudioStatistics();
 * }
 *
 * ...
 *
 * Video conferenceVideo = findViewById(R.id.conferenceVideo);
 * MediaStream conferenceStream = conference.getMediaStream();
 * conferenceVideo.setMediaStream(conferenceStream);
 * MediaStreamAudioStatistics conferenceStreamAudioStatistics = conferenceStream.getAudioStatistics();
 * </pre>
 */
@AnyThread
public class MediaStreamAudioStatistics {

    private final long timestamp;
    private final int rtt;
    private final int bitrate;
    private final int delay;
    private final int losses;

    private final int level;
    private final int rate;

    MediaStreamAudioStatistics(int rtt, double bitrate, double delay, double losses, int level, double rate) {
        this.timestamp = System.currentTimeMillis();
        this.rtt = rtt;
        this.bitrate = integerify(bitrate);
        this.delay = integerify(delay);
        this.losses = integerify(losses);
        this.level = level;
        this.rate = integerify(rate);
    }

    /**
     * Returns the creation timestamp of the statistics. The creation timestamp of the statistics is the number of
     * milliseconds that have elapsed between 1 January 1970 00:00:00 UTC and the time at which the statistics was
     * created.
     *
     * @return The creation timestamp of the statistics.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the instant round-trip time of the audio transmission. The instant round-trip time of the audio
     * transmission is an average sum of transmission delays (in milliseconds) of the network connection in both
     * directions (from the Android device to Mind API and vice versa) during the latest observed second.
     *
     * @return The instant round-trip time of the audio transmission.
     */
    public int getRtt() {
        return rtt;
    }

    /**
     * Returns the instant bitrate of the audio transmission. The instant bitrate of the audio transmission is a size
     * (in bits) of all audio packets that have been transmitted over the network connection during the latest observed
     * second.
     *
     * @return The instant bitrate of the audio transmission.
     */
    public int getBitrate() {
        return bitrate;
    }

    /**
     * Returns the instant delay of the audio transmission. The instant delay of the audio transmission is an average
     * number of milliseconds that audio samples have spent buffered locally (on the Android device) during the latest
     * observed second before (in case of sending a local media stream) or after (in case of receiving a remote media
     * stream) being transmitted over the network connection.
     *
     * @return The instant delay of the audio transmission.
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Returns the instant loss rate of the audio transmission. The instant loss rate of the audio transmission is a
     * percentage (i.e. an integer between 0 and 100 inclusively) of audio packets that have been sent over the network
     * connection during the latest observed second but have not been received (i.e. have been lost).
     *
     * @return The instant loss rate of the audio transmission.
     */
    public int getLosses() {
        return losses;
    }

    /**
     * Returns the instant volume level of the audio transmission. The instant volume level of the audio transmission
     * is an average volume of samples that have been transmitted over the network connection during the latest
     * observed second. The volume is an integer between 0 and 100 inclusively, where 0 represents silence, 100
     * represents 0 dBov (maximum volume), and 50 represents approximately 6 dB of sound pressure level change in the
     * sound pressure level from 0 dBov.
     *
     * @return The instant volume level of the audio transmission.
     */
    public int getLevel() {
        return level;
    }

    /**
     * Returns the instant sample rate of the audio transmission. The instant sample rate of the audio transmission is
     * a number of audio samples that have been successfully transmitted over the network connection during the latest
     * observed second.
     *
     * @return The instant sample rate of the audio transmission.
     */
    public int getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  timestamp: " + timestamp + ",\n" +
                "  rtt: " + rtt + "ms,\n" +
                "  bitrate: " + (bitrate / 1024) + "kbit/s,\n" +
                "  delay: " + delay + "ms,\n" +
                "  losses: " + losses + "%,\n" +
                "  level: " + level + "%,\n" +
                "  rate: " + rate + "sps\n" +
                "}";
    }

    private static int integerify(double value) {
        return Double.isNaN(value) || Double.isInfinite(value) ? 0 : (int) value;
    }

}