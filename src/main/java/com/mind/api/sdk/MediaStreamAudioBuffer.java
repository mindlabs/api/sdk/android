package com.mind.api.sdk;

import org.webrtc.AudioTrack;

class MediaStreamAudioBuffer {

    private final AudioTrack track;
    private final boolean remote;

    MediaStreamAudioBuffer(AudioTrack track, boolean remote) {
        this.track = track;
        this.remote = remote;
    }

    AudioTrack getTrack() {
        return track;
    }

    boolean isRemote() {
        return remote;
    }

}
