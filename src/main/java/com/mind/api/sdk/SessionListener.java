package com.mind.api.sdk;

import android.util.Log;
import androidx.annotation.MainThread;

/**
 * SessionListener {@link Session#setListener(SessionListener)} can be used} for getting notifications of all events
 * related to the {@link Session}. These include all changes in the {@link Conference conference}, {@link Participant
 * participants} and {@link Me me} made by the server part of your application and remote participants. For example, if
 * one of the remote participants who played a role of a {@link ParticipantRole#MODERATOR moderator} changed the name
 * of the conference, then {@link SessionListener#onConferenceNameChanged(Conference) onConferenceNameChanged} method
 * of the listener would be called.
 */
@MainThread
public interface SessionListener {

    String LOGGER_TAG = "MindSDK";

    /**
     * This method is called when the state of the session is changed.
     *
     * @param session The session whose state was changed.
     */
    default void onSessionStateChanged(Session session) {
        Log.i(LOGGER_TAG, "Session " + session.getId() + " state changed: " + session.getState());
    }

    /**
     * This method is called when the name of the conference is changed by either the server part of your application
     * or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param conference The conference which name was changed.
     */
    default void onConferenceNameChanged(Conference conference) {
        Log.i(LOGGER_TAG, "Conference " + conference.getId() + " name changed: " + conference.getName());
    }

    /**
     * This method is called when the recording of the conference is started/resumed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param conference The conference the recording of which was started or resumed.
     */
    default void onConferenceRecordingStarted(Conference conference) {
        Log.i(LOGGER_TAG, "Conference " + conference.getId() + " recording started");
    }

    /**
     * This method is called when the recording of the conference is paused/stopped by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param conference The conference the recording of which was paused or stopped.
     */
    default void onConferenceRecordingStopped(Conference conference) {
        Log.i(LOGGER_TAG, "Conference " + conference.getId() + " recording stopped");
    }

    /**
     * This method is called when the conference is ended by the server part of your application. By the time of
     * calling the conference object itself and all other objects related to the conference aren't functional (like
     * after a call of {@link MindSDK#exit2(Session) exit} method of {@link MindSDK} class).
     *
     * @param conference The conference that was ended.
     */
    default void onConferenceEnded(Conference conference) {
        Log.i(LOGGER_TAG, "Conference " + conference.getId() + " ended");
    }

    /**
     * This method is called when a remote participant joins the conference.
     *
     * @param participant The remote participant who joined the conference.
     */
    default void onParticipantJoined(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " joined");
    }

    /**
     * This method is called when a remote participant leaves the conference because of his own will or because the
     * server part of your application expelled him.
     *
     * @param participant The remote participant who left the conference.
     */
    default void onParticipantExited(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " exited");
    }

    /**
     * This method is called when the name of a remote participant is changed by the server part of your application or
     * by one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator} or by the
     * participant himself.
     *
     * @param participant The remote participant whose name was changed.
     */
    default void onParticipantNameChanged(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " name changed: " + participant.getName());
    }

    /**
     * This method is called when the priority of a remote participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param participant The remote participant whose priority was changed.
     */
    default void onParticipantPriorityChanged(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " priority changed: " + participant.getPriority());
    }

    /**
     * This method is called when the role of a remote participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param participant The remote participant whose role was changed.
     */
    default void onParticipantRoleChanged(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " role changed: " + participant.getRole());
    }

    /**
     * This method is called when a remote participant starts or stops streaming his primary audio or/and video.
     *
     * @param participant The remote participant who started or stopped streaming his primary audio or/and video.
     */
    default void onParticipantMediaChanged(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " media changed: " + participant.isStreamingAudio() + ", " + participant.isStreamingVideo());
    }

    /**
     * This method is called when a remote participant starts or stops streaming his secondary audio or/and video.
     *
     * @param participant The remote participant who started or stopped streaming his secondary audio or/and video.
     */
    default void onParticipantSecondaryMediaChanged(Participant participant) {
        Log.i(LOGGER_TAG, "Participant " + participant.getId() + " secondary media changed: " + participant.isStreamingSecondaryAudio() + ", " + participant.isStreamingSecondaryVideo());
    }

    /**
     * This method is called when the local participant is expelled from the conference by the server part of your
     * application. By the time of calling the conference object itself and all other objects related to the conference
     * aren't functional (like after a call of {@link MindSDK#exit2(Session) exit} method of {@link MindSDK} class).
     *
     * @param me The local participant who was expelled from the conference.
     */
    default void onMeExpelled(Me me) {
        Log.i(LOGGER_TAG, "Me " + me.getId() + " expelled");
    }

    /**
     * This method is called when the name of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param me The local participant whose name was changed.
     */
    default void onMeNameChanged(Me me) {
        Log.i(LOGGER_TAG, "Me " + me.getId() + " name changed: " + me.getName());
    }

    /**
     * This method is called when the priority of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param me The local participant whose priority was changed.
     */
    default void onMePriorityChanged(Me me) {
        Log.i(LOGGER_TAG, "Me " + me.getId() + " priority changed: " + me.getPriority());
    }

    /**
     * This method is called when the role of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param me The local participant whose role was changed.
     */
    default void onMeRoleChanged(Me me) {
        Log.i(LOGGER_TAG, "Me " + me.getId() + " role changed: " + me.getRole());
    }

    /**
     * This method is called when the local participant receives a message from the server part of your application.
     *
     * @param me The local participant whose received the message.
     * @param message The text of the message.
     */
    default void onMeReceivedMessageFromApplication(Me me, String message) {
        Log.i(LOGGER_TAG, "Me " + me.getId() + " received message from the application: " + message);
    }

    /**
     * This method is called when the local participant receives a message from a remote participant.
     *
     * @param me The local participant whose received the message.
     * @param message The text of the message.
     * @param participant The remote participant who sent the message.
     */
    default void onMeReceivedMessageFromParticipant(Me me, String message, Participant participant) {
        Log.i(LOGGER_TAG, "Me " + me.getId() + " received message from participant " + participant.getId() + ": " + message);
    }

}
