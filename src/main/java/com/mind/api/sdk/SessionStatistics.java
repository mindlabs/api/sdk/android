package com.mind.api.sdk;

import androidx.annotation.AnyThread;

/**
 * SessionStatistics class is used for representing statistics of a {@link Session participation session}. The
 * statistics consists of instant measures of the underlying network connection of the session. The values of all
 * measures are updated about once a second. You can always get the latest statistics with a help of {@link
 * Session#getStatistics() getStatistics} method of {@link Session} class:
 *
 * <pre>
 * String conferenceURI = "https://api.mind.com/&lt;APPLICATION_ID&gt;/&lt;CONFERENCE_ID&gt;";
 * String participantToken = "&lt;PARTICIPANT_TOKEN&gt;";
 * SessionOptions options = new SessionOptions();
 * MindSDK.join(conferenceURI, participantToken, options).thenAccept(session -> {
 *     SessionStatistics sessionStatistics = session.getStatistics();
 * });
 * </pre>
 */
@AnyThread
public class SessionStatistics {

    private final long timestamp;
    private final String protocol;
    private final String localAddress;
    private final int localPort;
    private final String remoteAddress;
    private final int remotePort;

    public SessionStatistics(String protocol, String localAddress, int localPort, String remoteAddress, int remotePort) {
        this.timestamp = System.currentTimeMillis();
        this.protocol = protocol;
        this.localAddress = localAddress;
        this.localPort = localPort;
        this.remoteAddress = remoteAddress;
        this.remotePort = remotePort;
    }

    /**
     * Returns the creation timestamp of the statistics. The creation timestamp of the statistics is the number of
     * milliseconds that have elapsed between 1 January 1970 00:00:00 UTC and the time at which the statistics was
     * created.
     *
     * @return The creation timestamp of the statistics.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the protocol of the session. The protocol of the session is a protocol which currently is used for
     * transmitting data of the session over the network. There are two possible values for the protocol: "udp" and
     * "tcp".
     *
     * @return The protocol of the session.
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Returns the local address of the session. The local address of the session is an IP address or a FQDN of the
     * client (Mind Android SDK) which currently is used for transmitting data of the session over the network.
     *
     * @return The local address of the session.
     */
    public String getLocalAddress() {
        return localAddress;
    }

    /**
     * Returns the local port of the session. The local port of the session is a port number on the client (Mind
     * Android SDK) which currently is used for transmitting data of the session over the network.
     *
     * @return The local port of the session.
     */
    public int getLocalPort() {
        return localPort;
    }

    /**
     * Returns the remote address of the session. The remote address of the session is an IP address or a FQDN of the
     * server (Mind API) which currently is used for transmitting data of the session over the network.
     *
     * @return The remote address of the session.
     */
    public String getRemoteAddress() {
        return remoteAddress;
    }

    /**
     * Returns the remote port of the session. The remote port of the session is a port number on the server (Mind API)
     * which currently is used for transmitting data of the session over the network.
     *
     * @return The remote port of the session.
     */
    public int getRemotePort() {
        return remotePort;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  timestamp: " + timestamp + ",\n" +
                "  protocol: " + protocol + ",\n" +
                "  localAddress: " + localAddress + ",\n" +
                "  localPort: " + localPort + ",\n" +
                "  remoteAddress: " + remoteAddress + ",\n" +
                "  remotePort: " + remotePort + ",\n" +
                "}";
    }

}
