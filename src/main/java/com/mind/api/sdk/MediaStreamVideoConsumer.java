package com.mind.api.sdk;

import androidx.annotation.MainThread;

/**
 * Any video consumer of {@link MediaStream} is required to implement MediaStreamVideoConsumer interface. The interface
 * defines only one method: {@link MediaStreamVideoConsumer#onVideoBuffer(MediaStreamVideoBuffer, MediaStream)
 * onVideoBuffer}. {@link MediaStream} uses it to supply a new video to the consumer or cancel the previously supplied
 * one. The consumer should use {@link MediaStream#addVideoConsumer(MediaStreamVideoConsumer) addVideoConsumer} and
 * {@link MediaStream#removeVideoConsumer(MediaStreamVideoConsumer) removeVideoConsumer} methods of {@link MediaStream}
 * class to register and unregister itself as a consumer of video from the {@link MediaStream}, respectively.
 */
@MainThread
public interface MediaStreamVideoConsumer {

    /**
     * Supplies a new video to the consumer or cancels the previously supplied one in which case `null` is passed as a
     * value for `videoBuffer` argument. It is guaranteed that during
     * {@link MediaStream#removeVideoConsumer(MediaStreamVideoConsumer) unregistration} this method is always called
     * with `null` as a value for `videoBuffer` argument regardless whether video has been supplied to the consumer or
     * not.
     *
     * @param videoBuffer The new video buffer or `null` if the previously supplied video buffer should be canceled.
     * @param supplier The media stream which supplies or cancels the video buffer.
     */
    void onVideoBuffer(MediaStreamVideoBuffer videoBuffer, MediaStream supplier);

}
