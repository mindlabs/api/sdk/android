package com.mind.api.sdk;

import android.text.TextUtils;
import androidx.annotation.MainThread;
import com.mind.api.sdk.MindSDK.PeerConnection;
import java9.util.concurrent.CompletableFuture;
import java9.util.function.Consumer;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStreamTrack;
import org.webrtc.PeerConnection.IceGatheringState;
import org.webrtc.PeerConnection.IceServer;
import org.webrtc.PeerConnection.PeerConnectionState;
import org.webrtc.RTCStats;
import org.webrtc.RTCStatsReport;
import org.webrtc.RtpParameters;
import org.webrtc.RtpTransceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoTrack;

import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@MainThread
class WebRtcConnection implements MediaStreamAudioConsumer, MediaStreamVideoConsumer, MediaStreamAudioSupplier, MediaStreamVideoSupplier {

    private static final ExecutorService BACKGROUND_EXECUTOR = Executors.newSingleThreadExecutor();
    private static final Pattern VP8_RTPMAP_PATTERN = Pattern.compile("\r\na=rtpmap:([0-9]+) VP8/90000");
    private static final Pattern VP9_RTPMAP_PATTERN = Pattern.compile("\r\na=rtpmap:([0-9]+) VP9/90000");

    // Thread-Safety: all methods of `session` except `executeOnMainThread` are accessed on the main thread only
    private final Session session;
    private final boolean useVP9;

    private PeerConnection pc; // Thread-Safety: accessed on the background thread and on the main thread
    private CompletableFuture<?> iceCandidatesGatheringCompleteFuture;
    private Future<?> negotiationFuture;
    private int negotiations;
    private Future<?> httpPost;
    private DataChannel dataChannel;
    private boolean dataChannelOpened;
    private final WebRtcPublication primaryPublication;
    private final WebRtcPublication secondaryPublication;
    private boolean lagging;
    private boolean destroyed;

    private Future<?> statisticsUpdatingFuture;
    private Map<String, Long> oldStatistics;

    private int webRtcTransceiverLastSdpSectionIndex = 1;
    private final LinkedList<WebRtcTransceiver> inactiveAudioTransceivers = new LinkedList<>();
    private final LinkedList<WebRtcTransceiver> inactiveVideoTransceivers = new LinkedList<>();

    private final Map<MediaStream, WebRtcSubscription> subscriptions = new HashMap<>();

    private DistortionRegistry distortionRegistry;

    WebRtcConnection(Session session) {
        this.session = session;
        this.useVP9 = session.getOptions().isUseVp9ForSendingVideo() != null ? session.getOptions().isUseVp9ForSendingVideo() : MindSDK.getOptions().isUseVp9ForSendingVideo();
        this.primaryPublication = new WebRtcPublication();
        this.secondaryPublication = new WebRtcPublication();
        this.distortionRegistry = new DistortionRegistry();
    }

    void onOpened() {}

    void onMessageReceived(String message) {}

    void onStartedLagging() {}

    void onStoppedLagging() {}

    void onFailed(Throwable exception) {}

    void onClosed(int code) {}

    void open() {
        Consumer<Throwable> onFailed = (exception) -> {
            abort();
            onFailed(exception);
        };
        try {
            if (pc == null) {
                List<IceServer> iceServers = new ArrayList<>();
                Set<String> desiredIceCandidateTypes = new HashSet<>();
                if (session.getOptions().getStunServerURL() != null) {
                    desiredIceCandidateTypes.add("srflx");
                    iceServers.add(IceServer.builder(session.getOptions().getStunServerURL()).createIceServer());
                }
                if (session.getOptions().getTurnServerURL() != null) {
                    desiredIceCandidateTypes.add("relay");
                    iceServers.add(IceServer.builder(session.getOptions().getTurnServerURL())
                                            .setUsername(Objects.toString(session.getOptions().getTurnServerUsername(), ""))
                                            .setPassword(Objects.toString(session.getOptions().getTurnServerPassword(), "")).createIceServer());
                }
                final AtomicReference<PeerConnection> PC = new AtomicReference<>();
                pc = new PeerConnection(iceServers) {

                    @Override
                    public void onConnectionChange(PeerConnectionState connectionState) {
                        session.executeOnMainThread(() -> {
                            if (pc == PC.get()) {
                                switch (connectionState) {
                                    case FAILED:
                                        onFailed.accept(new RuntimeException("Connection failed"));
                                        break;
                                }
                            }
                        });
                    }

                    @Override
                    public void onRenegotiationNeeded() {
                        session.executeOnMainThread(() -> {
                            if (negotiationFuture == null) {
                                negotiationFuture = session.scheduleOnMainThread(() -> {
                                    if (pc == PC.get()) {
                                        negotiationFuture = null;
                                        negotiations++;
                                        if (negotiations == 1) {
                                            AtomicReference<String> localSdp = new AtomicReference<>();
                                            pc.createOffer(new SdpObserver() {

                                                @Override
                                                public void onCreateSuccess(SessionDescription description) {
                                                    session.executeOnMainThread(() -> {
                                                        if (pc == PC.get()) {
                                                            localSdp.set(fixLocalSdpBeforeSetting(description.description));
                                                            BACKGROUND_EXECUTOR.execute(() -> {
                                                                if (pc == PC.get()) {
                                                                    pc.setLocalDescription(this, new SessionDescription(SessionDescription.Type.OFFER, localSdp.get()));
                                                                }
                                                            });
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onSetSuccess() {
                                                    session.executeOnMainThread(() -> {
                                                        if (pc == PC.get()) {
                                                            iceCandidatesGatheringCompleteFuture.thenRun(() -> {
                                                                if (pc == PC.get()) {
                                                                    try {
                                                                        localSdp.set(fixLocalSdpBeforeSending(pc.getLocalDescription().description));
                                                                        JSONObject requestDTO = new JSONObject().put("sdp", localSdp.get());
                                                                        session.newHttpPost("/", requestDTO, httpPost -> WebRtcConnection.this.httpPost = httpPost).thenAccept((responseDTO) -> {
                                                                            if (pc == PC.get()) {
                                                                                try {
                                                                                    // It is important to pass to `fixRemoteSdp` method the same local SDP, which was set into `PeerConnection` object
                                                                                    // (but not the one that was modified with `fixLocalSdpBeforeSending` method afterward), because otherwise the simulcast
                                                                                    // attributes for inactive video sections wouldn't be removed properly from the remote SDP.
                                                                                    String remoteSdp = fixRemoteSdp(responseDTO.getString("sdp"), pc.getLocalDescription().description);
                                                                                    SessionDescription description = new SessionDescription(SessionDescription.Type.ANSWER, remoteSdp);
                                                                                    BACKGROUND_EXECUTOR.execute(() -> {
                                                                                        if (pc == PC.get()) {
                                                                                            pc.setRemoteDescription(new SdpObserver() {

                                                                                                @Override
                                                                                                public void onCreateSuccess(SessionDescription description) {}

                                                                                                @Override
                                                                                                public void onSetSuccess() {
                                                                                                    session.executeOnMainThread(() -> {
                                                                                                        if (pc == PC.get()) {
                                                                                                            negotiations--;
                                                                                                            if (negotiations > 0) {
                                                                                                                negotiations = 0;
                                                                                                                pc.onRenegotiationNeeded();
                                                                                                            } else {
                                                                                                                if (lagging) {
                                                                                                                    lagging = false;
                                                                                                                    onStoppedLagging();
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    });
                                                                                                }

                                                                                                @Override
                                                                                                public void onCreateFailure(String error) {
                                                                                                    session.executeOnMainThread(() -> {
                                                                                                        if (pc == PC.get()) {
                                                                                                            onFailed.accept(new RuntimeException("Can't create remote session description: " + error));
                                                                                                        }
                                                                                                    });
                                                                                                }

                                                                                                @Override
                                                                                                public void onSetFailure(String error) {
                                                                                                    session.executeOnMainThread(() -> {
                                                                                                        if (pc == PC.get()) {
                                                                                                            onFailed.accept(new RuntimeException("Can't set remote session description: " + error));
                                                                                                        }
                                                                                                    });
                                                                                                }

                                                                                            }, description);
                                                                                        }
                                                                                    });
                                                                                } catch (Exception exception) {
                                                                                    session.executeOnMainThread(() -> {
                                                                                        onFailed.accept(exception);
                                                                                    });
                                                                                }
                                                                            }
                                                                        }).exceptionally((exception) -> {
                                                                            if (pc == PC.get()) {
                                                                                if (dataChannelOpened && exception.getCause() instanceof SocketTimeoutException) {
                                                                                    if (!lagging) {
                                                                                        lagging = true;
                                                                                        onStartedLagging();
                                                                                    }
                                                                                    negotiations = 0;
                                                                                    pc.onRenegotiationNeeded();
                                                                                } else {
                                                                                    onFailed.accept(exception.getCause());
                                                                                }
                                                                            }
                                                                            return null;
                                                                        });
                                                                    } catch (JSONException exception) {
                                                                        onFailed.accept(exception);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onCreateFailure(String error) {
                                                    session.executeOnMainThread(() -> {
                                                        if (pc == PC.get()) {
                                                            onFailed.accept(new RuntimeException("Can't create local session description: " + error));
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onSetFailure(String error) {
                                                    session.executeOnMainThread(() -> {
                                                        if (pc == PC.get()) {
                                                            onFailed.accept(new RuntimeException("Can't set local session description: " + error));
                                                        }
                                                    });
                                                }

                                            }, new MediaConstraints());
                                        }
                                    }
                                }, 0, TimeUnit.MILLISECONDS);
                            }
                        });
                    }

                    @Override
                    public void onIceCandidate(IceCandidate iceCandidate) {
                        session.executeOnMainThread(() -> {
                            if (pc == PC.get()) {
                                desiredIceCandidateTypes.remove(iceCandidate.sdp.replaceFirst(".* typ ([^ ]+).*", "$1"));
                                if (desiredIceCandidateTypes.isEmpty()) {
                                    iceCandidatesGatheringCompleteFuture.complete(null);
                                }
                            }
                        });

                    }

                    @Override
                    public void onIceGatheringChange(IceGatheringState iceGatheringState) {
                        session.executeOnMainThread(() -> {
                            if (pc == PC.get()) {
                                if (iceGatheringState == IceGatheringState.COMPLETE) {
                                    iceCandidatesGatheringCompleteFuture.complete(null);
                                }
                            }
                        });
                    }

                };
                PC.set(pc);
                iceCandidatesGatheringCompleteFuture = new CompletableFuture<>();
                dataChannel = pc.createDataChannel("", createDataChannelInit(), new DataChannel.Observer() {

                    @Override
                    public void onStateChange() {
                        session.executeOnMainThread(() -> {
                            if (pc == PC.get()) {
                                switch (dataChannel.state()) {
                                    case OPEN:
                                        dataChannelOpened = true;
                                        open();
                                        onOpened();
                                        break;
                                    case CLOSED:
                                        if (pc != null) {
                                            onFailed.accept(new Error("Data channel closed without closing code"));
                                        }
                                        break;
                                }
                            }
                        });
                    }

                    @Override
                    public void onMessage(DataChannel.Buffer buffer) {
                        if (!buffer.binary) {
                            String message = StandardCharsets.UTF_8.decode(buffer.data).toString();
                            session.executeOnMainThread(() -> {
                                if (pc == PC.get()) {
                                    if (Objects.equals(message, "4000") || Objects.equals(message, "4001")) {
                                        onClosed(Integer.parseInt(message));
                                    } else {
                                        onMessageReceived(message);
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onBufferedAmountChange(long previousAmount) {}

                });
                if (statisticsUpdatingFuture == null) {
                    if (primaryPublication.getStream() != null) {
                        primaryPublication.getStream().resetStatistics();
                    }
                    if (secondaryPublication.getStream() != null) {
                        secondaryPublication.getStream().resetStatistics();
                    }
                    for (MediaStream stream : subscriptions.keySet()) {
                        stream.resetStatistics();
                    }
                    oldStatistics = null;
                    updateStatistics();
                    statisticsUpdatingFuture = session.scheduleOnMainThreadWithFixedDelay(this::updateStatistics, 1, TimeUnit.SECONDS);
                }
            }
            if (dataChannelOpened) {
                if (primaryPublication.getAudioTransceiver() == null) {
                    webRtcTransceiverLastSdpSectionIndex += 1;
                    primaryPublication.setAudioTransceiver(new WebRtcTransceiver(pc.addTransceiver(MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO, createAudioTransceiverInit()), MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO, webRtcTransceiverLastSdpSectionIndex));
                    primaryPublication.getAudioTransceiver().setCodecPreferences(MindSDK.getAudioCodecs());
                }
                if (primaryPublication.getVideoTransceiver() == null) {
                    webRtcTransceiverLastSdpSectionIndex += 1;
                    primaryPublication.setVideoTransceiver(new WebRtcTransceiver(pc.addTransceiver(MediaStreamTrack.MediaType.MEDIA_TYPE_VIDEO, createVideoTransceiverInit()), MediaStreamTrack.MediaType.MEDIA_TYPE_VIDEO, webRtcTransceiverLastSdpSectionIndex));
                    primaryPublication.getVideoTransceiver().setCodecPreferences(MindSDK.getVideoCodecs());
                }
                if (secondaryPublication.getAudioTransceiver() == null) {
                    webRtcTransceiverLastSdpSectionIndex += 1;
                    secondaryPublication.setAudioTransceiver(new WebRtcTransceiver(pc.addTransceiver(MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO, createAudioTransceiverInit()), MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO, webRtcTransceiverLastSdpSectionIndex));
                    secondaryPublication.getAudioTransceiver().setCodecPreferences(MindSDK.getAudioCodecs());
                }
                if (secondaryPublication.getVideoTransceiver() == null) {
                    webRtcTransceiverLastSdpSectionIndex += 1;
                    secondaryPublication.setVideoTransceiver(new WebRtcTransceiver(pc.addTransceiver(MediaStreamTrack.MediaType.MEDIA_TYPE_VIDEO, createVideoTransceiverInit()), MediaStreamTrack.MediaType.MEDIA_TYPE_VIDEO, webRtcTransceiverLastSdpSectionIndex));
                    secondaryPublication.getVideoTransceiver().setCodecPreferences(MindSDK.getVideoCodecs());
                }
                if (isSendingPrimaryAudio()) {
                    primaryPublication.getAudioTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.SEND_ONLY);
                    primaryPublication.getAudioTransceiver().setSendingTrack(primaryPublication.getAudioBuffer().getTrack());
                    configureAudioTransceiver(primaryPublication.getAudioTransceiver(), primaryPublication.getAudioBuffer(), primaryPublication.getStream());
                } else {
                    primaryPublication.getAudioTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.INACTIVE);
                    primaryPublication.getAudioTransceiver().setSendingTrack(null);
                }
                if (isSendingPrimaryVideo()) {
                    primaryPublication.getVideoTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.SEND_ONLY);
                    primaryPublication.getVideoTransceiver().setSendingTrack(primaryPublication.getVideoBuffer().getTrack());
                    configureVideoTransceiver(primaryPublication.getVideoTransceiver(), primaryPublication.getVideoBuffer(), primaryPublication.getStream());
                } else {
                    primaryPublication.getVideoTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.INACTIVE);
                    primaryPublication.getVideoTransceiver().setSendingTrack(null);
                }
                if (isSendingSecondaryAudio()) {
                    secondaryPublication.getAudioTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.SEND_ONLY);
                    secondaryPublication.getAudioTransceiver().setSendingTrack(secondaryPublication.getAudioBuffer().getTrack());
                    configureAudioTransceiver(secondaryPublication.getAudioTransceiver(), secondaryPublication.getAudioBuffer(), secondaryPublication.getStream());
                } else {
                    secondaryPublication.getAudioTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.INACTIVE);
                    secondaryPublication.getAudioTransceiver().setSendingTrack(null);
                }
                if (isSendingSecondaryVideo()) {
                    secondaryPublication.getVideoTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.SEND_ONLY);
                    secondaryPublication.getVideoTransceiver().setSendingTrack(secondaryPublication.getVideoBuffer().getTrack());
                    configureVideoTransceiver(secondaryPublication.getVideoTransceiver(), secondaryPublication.getVideoBuffer(), secondaryPublication.getStream());
                } else {
                    secondaryPublication.getVideoTransceiver().setDirection(RtpTransceiver.RtpTransceiverDirection.INACTIVE);
                    secondaryPublication.getVideoTransceiver().setSendingTrack(null);
                }
                for (WebRtcSubscription subscription : subscriptions.values()) {
                    if (subscription.getAudioConsumer() != null) {
                        if (subscription.getAudioTransceiver() == null) {
                            subscription.setAudioTransceiver(acquireTransceiver(MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO));
                            subscription.getAudioConsumer().onAudioBuffer(new MediaStreamAudioBuffer((AudioTrack) subscription.getAudioTransceiver().getReceivingTrack(), true));
                            subscription.getAudioTransceiver().setCodecPreferences(MindSDK.getAudioCodecs());
                        }
                    } else {
                        if (subscription.getAudioTransceiver() != null) {
                            releaseTransceiver(subscription.getAudioTransceiver());
                            subscription.setAudioTransceiver(null);
                        }
                    }
                    if (subscription.getVideoConsumer() != null) {
                        if (subscription.getVideoTransceiver() == null) {
                            subscription.setVideoTransceiver(acquireTransceiver(MediaStreamTrack.MediaType.MEDIA_TYPE_VIDEO));
                            subscription.getVideoConsumer().onVideoBuffer(new MediaStreamVideoBuffer((VideoTrack) subscription.getVideoTransceiver().getReceivingTrack(), true, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, 1, 1.0));
                            subscription.getVideoTransceiver().setCodecPreferences(MindSDK.getVideoCodecs());
                        }
                    } else {
                        if (subscription.getVideoTransceiver() != null) {
                            releaseTransceiver(subscription.getVideoTransceiver());
                            subscription.setVideoTransceiver(null);
                        }
                    }
                }
            }
            pc.onRenegotiationNeeded();
        } catch (Exception exception) {
            onFailed.accept(exception);
        }
    }

    void close() {
        destroyed = true;
        abort();
        if (statisticsUpdatingFuture != null) {
            statisticsUpdatingFuture.cancel(false);
            statisticsUpdatingFuture = null;
        }
        primaryPublication.setAudioBuffer(null);
        primaryPublication.setVideoBuffer(null);
        if (primaryPublication.getStream() != null) {
            primaryPublication.getStream().removeAudioConsumer(this);
            primaryPublication.getStream().removeVideoConsumer(this);
            primaryPublication.setStream(null);
        }
        secondaryPublication.setAudioBuffer(null);
        secondaryPublication.setVideoBuffer(null);
        if (secondaryPublication.getStream() != null) {
            secondaryPublication.getStream().removeAudioConsumer(this);
            secondaryPublication.getStream().removeVideoConsumer(this);
            secondaryPublication.setStream(null);
        }
        subscriptions.clear();
    }

    MediaStream getPrimaryMediaStream() {
        return primaryPublication.getStream();
    }

    void setPrimaryMediaStream(MediaStream primaryStream) {
        if (destroyed) {
            throw new IllegalStateException("WebRTC connection has been already destroyed");
        }
        if (primaryPublication.getStream() != primaryStream) {
            if (primaryPublication.getStream()!= null) {
                primaryPublication.getStream().removeAudioConsumer(this);
                primaryPublication.getStream().removeVideoConsumer(this);
            }
            primaryPublication.setStream(primaryStream);
            if (primaryStream != null) {
                primaryStream.addAudioConsumer(this, 1.0);
                primaryStream.addVideoConsumer(this);
            }
        }
    }

    boolean isSendingPrimaryAudio() {
        return primaryPublication.getAudioBuffer() != null;
    }

    boolean isSendingPrimaryVideo() {
        return primaryPublication.getVideoBuffer() != null;
    }

    MediaStream getSecondaryMediaStream() {
        return secondaryPublication.getStream();
    }

    void setSecondaryMediaStream(MediaStream secondaryStream) {
        if (destroyed) {
            throw new IllegalStateException("WebRTC connection has been already destroyed");
        }
        if (secondaryPublication.getStream() != secondaryStream) {
            if (secondaryPublication.getStream() != null) {
                secondaryPublication.getStream().removeAudioConsumer(this);
                secondaryPublication.getStream().removeVideoConsumer(this);
            }
            secondaryPublication.setStream(secondaryStream);
            if (secondaryStream != null) {
                secondaryStream.addAudioConsumer(this, 1.0);
                secondaryStream.addVideoConsumer(this);
            }
        }
    }

    boolean isSendingSecondaryAudio() {
        return secondaryPublication.getAudioBuffer() != null;
    }

    boolean isSendingSecondaryVideo() {
        return secondaryPublication.getVideoBuffer() != null;
    }

    @Override
    public void onAudioBuffer(MediaStreamAudioBuffer audioBuffer, MediaStream supplier) {
        if (!destroyed) {
            if (supplier == primaryPublication.getStream()) {
                if (primaryPublication.getAudioBuffer() != audioBuffer) {
                    primaryPublication.setAudioBuffer(audioBuffer);
                    open();
                }
            }
            if (supplier == secondaryPublication.getStream()) {
                if (secondaryPublication.getAudioBuffer() != audioBuffer) {
                    secondaryPublication.setAudioBuffer(audioBuffer);
                    open();
                }
            }
        }
    }

    @Override
    public void onVideoBuffer(MediaStreamVideoBuffer videoBuffer, MediaStream supplier) {
        if (!destroyed) {
            if (supplier == primaryPublication.getStream()) {
                if (primaryPublication.getVideoBuffer() != videoBuffer) {
                    primaryPublication.setVideoBuffer(videoBuffer);
                    open();
                }
            }
            if (supplier == secondaryPublication.getStream()) {
                if (secondaryPublication.getVideoBuffer() != videoBuffer) {
                    secondaryPublication.setVideoBuffer(videoBuffer);
                    open();
                }
            }
        }
    }

    @Override
    public void addAudioConsumer(MediaStream consumer) {
        if (!destroyed) {
            WebRtcSubscription subscription = subscriptions.get(consumer);
            if (subscription == null) {
                subscription = new WebRtcSubscription();
                subscriptions.put(consumer, subscription);
            }
            subscription.setAudioConsumer(consumer);
            distortionRegistry.addMtid(consumer.getLabel() + "#audio");
            open();
        }
    }

    @Override
    public void removeAudioConsumer(MediaStream consumer) {
        if (!destroyed) {
            WebRtcSubscription subscription = subscriptions.get(consumer);
            subscription.setAudioConsumer(null);
            distortionRegistry.removeMtid(consumer.getLabel() + "#audio");
            consumer.onAudioBuffer(null);
            open();
            if (subscription.getAudioConsumer() == null && subscription.getVideoConsumer() == null) {
                subscriptions.remove(consumer);
            }
        }
    }

    @Override
    public void addVideoConsumer(MediaStream consumer) {
        if (!destroyed) {
            WebRtcSubscription subscription = subscriptions.get(consumer);
            if (subscription == null) {
                subscription = new WebRtcSubscription();
                subscriptions.put(consumer, subscription);
            }
            subscription.setVideoConsumer(consumer);
            distortionRegistry.addMtid(consumer.getLabel() + "#video");
            open();
        }
    }

    @Override
    public void removeVideoConsumer(MediaStream consumer) {
        if (!destroyed) {
            WebRtcSubscription subscription = subscriptions.get(consumer);
            subscription.setVideoConsumer(null);
            distortionRegistry.removeMtid(consumer.getLabel() + "#video");
            consumer.onVideoBuffer(null);
            open();
            if (subscription.getAudioConsumer() == null && subscription.getVideoConsumer() == null) {
                subscriptions.remove(consumer);
            }
        }
    }

    private void abort() {
        if (negotiationFuture != null) {
            negotiationFuture.cancel(false);
            negotiationFuture = null;
        }
        negotiations = 0;
        if (dataChannel != null) {
            if (destroyed) {
                dataChannel.close();
            }
            dataChannel = null;
            dataChannelOpened = false;
        }
        if (pc != null) {
            PeerConnection pc = this.pc;
            BACKGROUND_EXECUTOR.execute(pc::dispose); // We dispose PeerConnection on the background thread (i.e. out of the main thread) because its disposing could take significant amount of time (several hundreds ms).
            this.pc = null;
            iceCandidatesGatheringCompleteFuture = null;
            primaryPublication.setAudioTransceiver(null);
            primaryPublication.setVideoTransceiver(null);
            secondaryPublication.setAudioTransceiver(null);
            secondaryPublication.setVideoTransceiver(null);
            for (WebRtcSubscription subscription : subscriptions.values()) {
                if (subscription.getAudioConsumer() != null) {
                    subscription.getAudioConsumer().onAudioBuffer(null);
                }
                if (subscription.getVideoConsumer() != null) {
                    subscription.getVideoConsumer().onVideoBuffer(null);
                }
                subscription.setAudioTransceiver(null);
                subscription.setVideoTransceiver(null);
            }
            inactiveAudioTransceivers.clear();
            inactiveVideoTransceivers.clear();
            webRtcTransceiverLastSdpSectionIndex = 1;
        }
        if (httpPost != null) {
            httpPost.cancel(true);
            httpPost = null;
        }
        lagging = false;
        if (oldStatistics != null) {
            if (primaryPublication.getStream() != null) {
                primaryPublication.getStream().resetStatistics();
            }
            if (secondaryPublication.getStream() != null) {
                secondaryPublication.getStream().resetStatistics();
            }
            for (MediaStream stream : subscriptions.keySet()) {
                stream.resetStatistics();
            }
            oldStatistics = null;
            updateStatistics();
        }
    }

    private void updateStatistics() {
        Map<String, Long> newStatistics = new HashMap<>();
        if (oldStatistics == null) {
            session.updateStatistics(null);
            if (primaryPublication.getStream() != null) {
                primaryPublication.getStream().updateStatistics(null);
            }
            if (secondaryPublication.getStream() != null) {
                secondaryPublication.getStream().updateStatistics(null);
            }
            for (MediaStream stream : subscriptions.keySet()) {
                stream.updateStatistics(null);
            }
            oldStatistics = newStatistics;
        } else {
            PeerConnection PC = pc;
            CompletableFuture<RTCStatsReport> gettingStatsFuture = new CompletableFuture<>();
            if (!dataChannelOpened) {
                gettingStatsFuture.complete(null);
            } else {
                pc.getStats((report) -> {
                    session.executeOnMainThread(() -> {
                        gettingStatsFuture.complete(report);
                    });
                });
            }
            gettingStatsFuture.thenAccept(report -> {
                if (pc == PC) {
                    List<RTCStats> sessionReport = new ArrayList<>();
                    Map<MediaStream, List<RTCStats>> streamReportMap = new HashMap<>();
                    if (report != null) {
                        if (primaryPublication.getStream() != null) {
                            streamReportMap.put(primaryPublication.getStream(), new ArrayList<>());
                        }
                        if (secondaryPublication.getStream() != null) {
                            streamReportMap.put(secondaryPublication.getStream(), new ArrayList<>());
                        }
                        for (MediaStream stream : subscriptions.keySet()) {
                            streamReportMap.put(stream, new ArrayList<>());
                        }
                        RTCStats selectedCandidatePair = null;
                        RTCStats localCandidate = null;
                        RTCStats remoteCandidate = null;
                        for (RTCStats stats : report.getStatsMap().values()) {
                            switch (stats.getMembers().get("kind") + "-" + stats.getType()) {
                                case "null-transport":
                                    if (stats.getMembers().containsKey("selectedCandidatePairId")) {
                                        selectedCandidatePair = report.getStatsMap().get(stats.getMembers().get("selectedCandidatePairId"));
                                        if (selectedCandidatePair != null) {
                                            localCandidate = report.getStatsMap().get(selectedCandidatePair.getMembers().get("localCandidateId"));
                                            remoteCandidate = report.getStatsMap().get(selectedCandidatePair.getMembers().get("remoteCandidateId"));
                                        }
                                    }
                                    break;
                                case "audio-media-source":
                                case "video-media-source":
                                case "audio-outbound-rtp":
                                case "video-outbound-rtp":
                                case "audio-remote-inbound-rtp":
                                case "video-remote-inbound-rtp":
                                case "audio-inbound-rtp":
                                case "video-inbound-rtp":
                                    MediaStream stream = getMediaStreamForStats(stats, report);
                                    if (stream != null) {
                                        streamReportMap.get(stream).add(stats);
                                    }
                                    break;

                            }
                        }
                        if (selectedCandidatePair != null) {
                            sessionReport.add(selectedCandidatePair);
                            sessionReport.add(localCandidate);
                            sessionReport.add(remoteCandidate);
                            for (List<RTCStats> stats : streamReportMap.values()) {
                                stats.add(selectedCandidatePair);
                            }
                        }
                    }
                    session.updateStatistics(sessionReport);
                    if (primaryPublication.getStream() != null) {
                        primaryPublication.getStream().updateStatistics(streamReportMap.get(primaryPublication.getStream()));
                    }
                    if (secondaryPublication.getStream() != null) {
                        secondaryPublication.getStream().updateStatistics(streamReportMap.get(secondaryPublication.getStream()));
                    }
                    for (MediaStream stream : subscriptions.keySet()) {
                        stream.updateStatistics(streamReportMap.get(stream));
                    }
                    if (report != null) {
                        for (WebRtcSubscription subscription : subscriptions.values()) {
                            if (subscription.getAudioConsumer() != null && subscription.getAudioConsumer().getAudioStatistics().getRate() < 40_000) {
                                distortionRegistry.registerDistortion(subscription.getAudioConsumer().getLabel() + "#audio");
                            }
                            if (subscription.getVideoConsumer() != null && subscription.getVideoConsumer().getVideoStatistics().getRate() < 1) {
                                distortionRegistry.registerDistortion(subscription.getVideoConsumer().getLabel() + "#video");
                            }
                        }
                    }
                    if (dataChannelOpened) {
                        distortionRegistry.report(dataChannel);
                    }
                    oldStatistics = newStatistics;
                }
            });
        }
    }

    private MediaStream getMediaStreamForStats(RTCStats stats, RTCStatsReport report) {
        String mid = (String) stats.getMembers().get("mid");
        if (mid != null) { // if the stats has an associated `mid` (e.g. `inbound-rtp` and `outbound-rtp`)
            int sdpSectionIndex = Integer.parseInt(mid) + 1;
            for (WebRtcPublication publication : List.of(primaryPublication, secondaryPublication)) {
                if (publication.getAudioTransceiver() != null && publication.getAudioTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return publication.getStream();
                }
                if (publication.getVideoTransceiver() != null && publication.getVideoTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return publication.getStream();
                }
            }
            for (WebRtcSubscription subscription : subscriptions.values()) {
                if (subscription.getAudioTransceiver() != null && subscription.getAudioTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return subscription.getAudioConsumer();
                }
                if (subscription.getVideoTransceiver() != null && subscription.getVideoTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return subscription.getVideoConsumer();
                }
            }
        }
        String trackIdentifier = (String) stats.getMembers().get("trackIdentifier");
        if (trackIdentifier != null) { // if the stats doesn't have an associated `mid` (i.e. `media-source`)
            for (WebRtcPublication publication : List.of(primaryPublication, secondaryPublication)) {
                if (publication.getAudioTransceiver() != null && publication.getAudioTransceiver().getSendingTrack() != null && Objects.equals(publication.getAudioTransceiver().getSendingTrack().id(), trackIdentifier)) {
                    return publication.getStream();
                }
                if (publication.getVideoTransceiver() != null && publication.getVideoTransceiver().getSendingTrack() != null && Objects.equals(publication.getVideoTransceiver().getSendingTrack().id(), trackIdentifier)) {
                    return publication.getStream();
                }
            }
            for (WebRtcSubscription subscription : subscriptions.values()) {
                if (subscription.getAudioTransceiver() != null && subscription.getAudioTransceiver().getReceivingTrack() != null && Objects.equals(subscription.getAudioTransceiver().getReceivingTrack().id(), trackIdentifier)) {
                    return subscription.getAudioConsumer();
                }
                if (subscription.getVideoTransceiver() != null && subscription.getVideoTransceiver().getReceivingTrack() != null && Objects.equals(subscription.getVideoTransceiver().getReceivingTrack().id(), trackIdentifier)) {
                    return subscription.getVideoConsumer();
                }
            }
        }
        String localId = (String) stats.getMembers().get("localId");
        if (localId != null) { // if the stats has only reference to another stats through `localId` (e.g. any `remote-inbound-rtp` stats has only a reference to corresponding `outbound-rtp` stats)
            return getMediaStreamForStats(report.getStatsMap().get(localId), report);
        }
        return null;
    }

    private WebRtcTransceiver acquireTransceiver(MediaStreamTrack.MediaType mediaType) {
        LinkedList<WebRtcTransceiver> inactiveTransceivers = mediaType == MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO ? inactiveAudioTransceivers : inactiveVideoTransceivers;
        if (!inactiveTransceivers.isEmpty()) {
            WebRtcTransceiver transceiver = inactiveTransceivers.pop();
            transceiver.setDirection(RtpTransceiver.RtpTransceiverDirection.RECV_ONLY);
            return transceiver;
        } else {
            webRtcTransceiverLastSdpSectionIndex += 1;
            return new WebRtcTransceiver(pc.addTransceiver(mediaType, new RtpTransceiver.RtpTransceiverInit(RtpTransceiver.RtpTransceiverDirection.RECV_ONLY)), mediaType, webRtcTransceiverLastSdpSectionIndex);
        }
    }

    private void releaseTransceiver(WebRtcTransceiver transceiver) {
        LinkedList<WebRtcTransceiver> inactiveTransceivers = transceiver.getMediaType() == MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO ? inactiveAudioTransceivers : inactiveVideoTransceivers;
        transceiver.setDirection(RtpTransceiver.RtpTransceiverDirection.INACTIVE);
        inactiveTransceivers.push(transceiver);
    }

    private RtpTransceiver.RtpTransceiverInit createAudioTransceiverInit() {
        RtpTransceiver.RtpTransceiverDirection direction = RtpTransceiver.RtpTransceiverDirection.INACTIVE;
        List<RtpParameters.Encoding> encodings = Collections.emptyList();
        return new RtpTransceiver.RtpTransceiverInit(direction, Collections.emptyList(), encodings);
    }

    private RtpTransceiver.RtpTransceiverInit createVideoTransceiverInit() {
        RtpTransceiver.RtpTransceiverDirection direction = RtpTransceiver.RtpTransceiverDirection.INACTIVE;
        if (useVP9) {
            RtpParameters.Encoding rtpEncodingParameters = new RtpParameters.Encoding(null, false, 1.0);
            List<RtpParameters.Encoding> encodings = List.of(rtpEncodingParameters);
            return new RtpTransceiver.RtpTransceiverInit(direction, Collections.emptyList(), encodings);
        } else {
            RtpParameters.Encoding lowRtpEncodingParameters = new RtpParameters.Encoding("l", false, 4.0);
            RtpParameters.Encoding mediumRtpEncodingParameters = new RtpParameters.Encoding("m", false, 2.0);
            RtpParameters.Encoding highRtpEncodingParameters = new RtpParameters.Encoding("h", false, 1.0);
            List<RtpParameters.Encoding> encodings = Arrays.asList(lowRtpEncodingParameters, mediumRtpEncodingParameters, highRtpEncodingParameters);
            return new RtpTransceiver.RtpTransceiverInit(direction, Collections.emptyList(), encodings);
        }
    }

    private DataChannel.Init createDataChannelInit() {
        DataChannel.Init init = new DataChannel.Init();
        init.ordered = true;
        init.negotiated = true;
        init.id = 1;
        return init;
    }

    private void configureAudioTransceiver(WebRtcTransceiver audioTransceiver, MediaStreamAudioBuffer audioBuffer, MediaStream stream) {}

    private void configureVideoTransceiver(WebRtcTransceiver videoTransceiver, MediaStreamVideoBuffer videoBuffer, MediaStream stream) {
        RtpParameters parameters = videoTransceiver.getSendingParameters();
        int[] adaptivityToBitrateFractionMap = new int[] { 1, 5, 21 }; // 1, 1 + 4, 1 + 4 + 16
        int[] adaptivityToResolutionScaleMap = new int[] { 4, 2, 1 };
        if (useVP9) {
            int maxBitrateFraction = stream.getMaxVideoBitrate() / (videoBuffer.getBitrate() / adaptivityToBitrateFractionMap[videoBuffer.getAdaptivity() - 1]);
            int spatialLayersCount = 1;
            for (int i = 1; i < videoBuffer.getAdaptivity(); i++) {
                if (adaptivityToBitrateFractionMap[i] <= maxBitrateFraction && videoBuffer.getWidth() * videoBuffer.getHeight() / ((int) Math.pow(adaptivityToResolutionScaleMap[i], 2)) <= stream.getMaxVideoFrameArea()) {
                    spatialLayersCount += 1;
                } else {
                    break;
                }
            }
            parameters.encodings.get(0).active = true;
            parameters.encodings.get(0).scaleResolutionDownBy = videoBuffer.getScale() * Math.pow(2, videoBuffer.getAdaptivity() - spatialLayersCount);
            parameters.encodings.get(0).maxBitrateBps = Math.min(videoBuffer.getBitrate(), stream.getMaxVideoBitrate());
            parameters.encodings.get(0).scalabilityMode = spatialLayersCount > 1 ? "L" + spatialLayersCount + "T3_KEY" : "L1T3";
            parameters.encodings.get(0).maxFramerate = stream.getMaxVideoFrameRate() == Integer.MAX_VALUE ? null : stream.getMaxVideoFrameRate();
        } else {
            for (int i = 0; i < parameters.encodings.size(); i++) {
                parameters.encodings.get(i).active = false;
                parameters.encodings.get(i).scaleResolutionDownBy = 1.0;
                parameters.encodings.get(i).maxBitrateBps = 0;
            }
            int totalMaxBitrate = 0;
            for (int i = 0; i < videoBuffer.getAdaptivity(); i++) {
                int maxBitrate = videoBuffer.getBitrate() / adaptivityToBitrateFractionMap[videoBuffer.getAdaptivity() - 1] * ((int) Math.pow(4, i));
                totalMaxBitrate += maxBitrate;
                if (i == 0 || (totalMaxBitrate <= stream.getMaxVideoBitrate() && videoBuffer.getWidth() * videoBuffer.getHeight() / ((int) Math.pow(adaptivityToResolutionScaleMap[i], 2)) <= stream.getMaxVideoFrameArea())) {
                    parameters.encodings.get(i).active = true;
                    parameters.encodings.get(i).scaleResolutionDownBy = videoBuffer.getScale() * Math.pow(2, videoBuffer.getAdaptivity() - 1 - i);
                    parameters.encodings.get(i).maxBitrateBps = maxBitrate;
                    parameters.encodings.get(i).scalabilityMode = "L1T3";
                    parameters.encodings.get(i).maxFramerate = stream.getMaxVideoFrameRate() == Integer.MAX_VALUE ? null : stream.getMaxVideoFrameRate();
                }
            }
        }
        // FIXME: In case of CPU overloading we prefer VP8 encoder to maintain resolution because otherwise if `scale`
        // didn't equal `1.0` the encoder would fail during the reinitialization due to
        // <https://webrtc.googlesource.com/src/+/master/modules/video_coding/utility/simulcast_utility.cc#42>.
        parameters.degradationPreference = RtpParameters.DegradationPreference.MAINTAIN_RESOLUTION;
        videoTransceiver.setSendingParameters(parameters);
    }

    private String fixLocalSdpBeforeSetting(String localSdp) {
        String[] localSdpSections = localSdp.replace("\r\nm=", "\r\n\0m=").split("\0");
        for (int i = 2; i < localSdpSections.length; i++) {
            // WebRTC library silently supports RTCP Extended Reports with Receiver Reference Time parameters. These
            // reports help non-senders to estimate their round trip time (see RFC3611). In order to enable such
            // reports, we have to add manually `a=rtcp-fb:* rrtr` line to each media section of the offer SDP befor
            // setting it to `PeerConnection`. Once enabled (and confirmed in the answer SDP), there is no need to add
            // such lines manually again because the library will keep them in the following offers.
            if (!localSdpSections[i].matches("(?s).*\r\na=rtcp-fb:[^ ]+ rrtr.*")) {
                localSdpSections[i] = localSdpSections[i] + "a=rtcp-fb:* rrtr\r\n";
            }
        }
        return TextUtils.join("", localSdpSections);
    }

    private String fixLocalSdpBeforeSending(String localSdp) {
        String[] localSdpSections = localSdp.replaceAll("\r\na=candidate:.*typ host.*", "").replace("\r\nm=", "\r\n\0m=").split("\0");
        if (dataChannelOpened) {
            for (int i = 2; i < 6 && i < localSdpSections.length; i++) {
                localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=msid:.*", "");
                if (i == 2 && primaryPublication.getAudioTransceiver().getSendingTrack() != null) {
                    localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=mid:.*", "$0\r\na=msid:me#primary " + primaryPublication.getAudioTransceiver().getSendingTrack().id());
                }
                if (i == 3 && primaryPublication.getVideoTransceiver().getSendingTrack() != null) {
                    localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=mid:.*", "$0\r\na=msid:me#primary " + primaryPublication.getVideoTransceiver().getSendingTrack().id());
                }
                if (i == 4 && secondaryPublication.getAudioTransceiver().getSendingTrack() != null) {
                    localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=mid:.*", "$0\r\na=msid:me#secondary " + secondaryPublication.getAudioTransceiver().getSendingTrack().id());
                }
                if (i == 5 && secondaryPublication.getVideoTransceiver().getSendingTrack() != null) {
                    localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=mid:.*", "$0\r\na=msid:me#secondary " + secondaryPublication.getVideoTransceiver().getSendingTrack().id());
                }
                if (!useVP9 && (i == 3 || i == 5)) {
                    // FIXME: Mind API doesn't support switching simulcast on/off on the fly, but WebRTC library doesn't
                    // add simulcast attributes to video sections which are initialized in `inactive` state, that's why we
                    // have to add all necessary simulcast attributes to the offer SDP, if they are missing.
                    if (!localSdpSections[i].contains("\r\na=simulcast")) {
                        localSdpSections[i] = localSdpSections[i] + "a=rid:l send\r\na=rid:m send\r\na=rid:h send\r\na=simulcast:send ~l;~m;h\r\n";
                    }
                }
            }
            for (int i = 6; i < localSdpSections.length; i++) {
                localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=msid:.*", "");
            }
            for (WebRtcSubscription subscription : subscriptions.values()) {
                if (subscription.getAudioConsumer() != null && subscription.getAudioTransceiver() != null) {
                    int i = subscription.getAudioTransceiver().getSdpSectionIndex();
                    if (i < localSdpSections.length) {
                        localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=mid:.*", "$0\r\na=msid:" + subscription.getAudioConsumer().getLabel() + ' ' + subscription.getAudioTransceiver().getReceivingTrack().id());
                    }
                }
                if (subscription.getVideoConsumer() != null && subscription.getVideoTransceiver() != null) {
                    int i = subscription.getVideoTransceiver().getSdpSectionIndex();
                    if (i < localSdpSections.length) {
                        localSdpSections[i] = localSdpSections[i].replaceFirst("\r\na=mid:.*", "$0\r\na=msid:" + subscription.getVideoConsumer().getLabel() + ' ' + subscription.getVideoTransceiver().getReceivingTrack().id());
                        // FIXME: Since WebRTC provides no API for choosing maximum bitrate of the receiving video, we have to
                        // munge the offer SDP and add `b=TIAS` attribute for video section manually to set the maximum bitrate
                        // of the video which we are going to receive.
                        if (subscription.getVideoConsumer().getMaxVideoBitrate() < Integer.MAX_VALUE) {
                            localSdpSections[i] = localSdpSections[i] + "b=TIAS:" + subscription.getVideoConsumer().getMaxVideoBitrate() + "\r\n";
                        }
                        // We enforce maximum frame area and frame rate of the receiving video through the custom
                        // video-level `x-preferences` attribute in our offer SDP.
                        localSdpSections[i] = localSdpSections[i] + "a=x-preferences:" + (subscription.getVideoConsumer().getMaxVideoFrameArea() == Integer.MAX_VALUE ? 0 : subscription.getVideoConsumer().getMaxVideoFrameArea()) + "@" +
                                                                                         (subscription.getVideoConsumer().getMaxVideoFrameRate() == Integer.MAX_VALUE ? 0 : subscription.getVideoConsumer().getMaxVideoFrameRate()) + "\r\n";
                    }
                }
            }
        }
        return TextUtils.join("", localSdpSections);
    }

    private String fixRemoteSdp(String remoteSdp, String localSdp) {
        if (dataChannelOpened) {
            String[] remoteSdpSections = remoteSdp.replace("\r\nm=", "\r\n\0m=").split("\0");
            String[] localSdpSections = localSdp.replace("\r\nm=", "\r\n\0m=").split("\0");
            for (int i = 2; i < 6 && i < remoteSdpSections.length; i++) {
                if (i == 3 || i == 5) {
                    Pattern[] patterns;
                    if (useVP9) {
                        patterns = new Pattern[] { VP9_RTPMAP_PATTERN };
                    } else {
                        // FIXME: Mind API doesn't support switching simulcast on/off on the fly, but WebRTC library doesn't
                        // add simulcast attributes to video sections which are initialized in `inactive` state, but if we
                        // added simulcast attributes to the offer SDP in our own (in `fixLocalSdp` method), it turned out
                        // that in such cases we have to remove all simulcast attributes from the answer SDP, otherwise the
                        // video from the camera will not being transmitted if it was switched on at very beginning (at
                        // least we encountered that problem in Mind Android and iOS SDKs which both uses the same WebRTC
                        // library).
                        if (!localSdpSections[i].contains("\r\na=simulcast")) {
                            remoteSdpSections[i] = remoteSdpSections[i].replaceAll("\r\na=(rid:[lmh]|simulcast).*", "");
                        }
                        patterns = new Pattern[] { VP8_RTPMAP_PATTERN };
                    }
                    // FIXME: For sending its video WebRTC library always uses the codec which corresponds to the first
                    // payload stated in corresponding `m=video` section of the answer SDP. In order to force it use
                    // VP9 or VP8 regardless of the order of the payload types, we change the answer SDP and move the
                    // payload type which corresponds to the desired codec in front of the others.
                    for (Pattern pattern : patterns) {
                        Matcher matcher = pattern.matcher(remoteSdpSections[i]);
                        if (matcher.find()) {
                            int payloadType = Integer.parseInt(matcher.group(1));
                            remoteSdpSections[i] = remoteSdpSections[i].replaceFirst("^(m=video [^ ]+ [^ ]+)(.*)( " + payloadType + ")( .*)?", "$1$3$2$4");
                        }
                    }
                    // FIXME: Without `transport-cc` (i.e. with REMB only) the phase of discovering of the available
                    // bandwidth can take a significant time. During that time WebRTC library can send video in a low
                    // resolution. That's why if we are connected to Mind API which doesn't support `transport-cc` (i.e.
                    // which supports REMB only), we add a corresponding `x-google-start-bitrate` FMTP parameter to
                    // each active `recvonly` section in the answer SDP in order to force WebRTC library to send video
                    // in the highest available resolution from very beginning.
                    MediaStreamVideoBuffer videoBuffer = i == 3 ? primaryPublication.getVideoBuffer() : secondaryPublication.getVideoBuffer();
                    Matcher matcher = Pattern.compile("^m=video [^ ]+ [^ ]+ ([0-9]+)").matcher(remoteSdpSections[i]);
                    if (videoBuffer != null && matcher.find()) {
                        int payloadType = Integer.parseInt(matcher.group(1));
                        if (!Pattern.compile("\r\na=rtcp-fb:" + payloadType + " transport-cc").matcher(remoteSdpSections[i]).find()) {
                            String fmtpRegex = "\r\na=fmtp:" + payloadType + ".*";
                            String startBitrateFmtpParameter = "x-google-start-bitrate=" + (videoBuffer.getBitrate() / 1000);
                            if (Pattern.compile(fmtpRegex).matcher(remoteSdpSections[i]).find()) {
                                remoteSdpSections[i] = remoteSdpSections[i].replaceFirst(fmtpRegex, "$0; " + startBitrateFmtpParameter);
                            } else {
                                remoteSdpSections[i] = remoteSdpSections[i] + "a=fmtp:" + payloadType + " " + startBitrateFmtpParameter + "\r\n";
                            }
                        }
                    }
                }
            }
            for (int i = 6; i < remoteSdpSections.length; i++) {
                if (remoteSdpSections[i].startsWith("m=video")) {
                    // FIXME: Any attempt to set remote SDP which contains `b=TIAS` line(s) in any `m=video` section (even
                    // if it corresponds to receiving or video) forces WebRTC library to pause (for a while) medium & high
                    // simulcast substreams of all videos which we are sending.
                    remoteSdpSections[i] = remoteSdpSections[i].replaceAll("\r\nb=TIAS:.*", "");
                }
            }
            return TextUtils.join("", remoteSdpSections);
        } else {
            return remoteSdp;
        }
    }

}
