package com.mind.api.sdk;

import androidx.annotation.UiThread;

public class Audio implements MediaStreamAudioConsumer {

    private MediaStream stream;
    private double volume = 1.0;

    @UiThread
    public Audio() {}

    /**
     * Sets {@code MediaStream} that should be used as a source of audio. The {@code null} value can be used for
     * stopping playing previously set {@code MediaStream}.
     */
    @UiThread
    public void setMediaStream(MediaStream stream) {
        if (this.stream != stream) {
            if (this.stream != null) {
                this.stream.removeAudioConsumer(this);
            }
            if (stream == null) {
                this.stream = null;
            } else {
                this.stream = stream;
                stream.addAudioConsumer(this, volume);
            }
        }
    }

    /**
     * Sets the volume of the audio. The volume can be in range between 0.0 and 1.0, inclusively, where 0.0 means the
     * absolute silence, and 1.0 means the highest volume. The default volume is 1.0.
     *
     * @param volume The volume of the audio.
     */
    @UiThread
    public void setVolume(double volume) {
        this.volume = volume;
        if (stream != null) {
            stream.addAudioConsumer(this, volume);
        }
    }

    @Override
    public void onAudioBuffer(MediaStreamAudioBuffer audioBuffer, MediaStream supplier) {}

}
