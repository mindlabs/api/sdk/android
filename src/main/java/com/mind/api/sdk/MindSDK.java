package com.mind.api.sdk;

import android.app.Application;
import android.util.Log;
import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import org.webrtc.DataChannel;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStreamTrack;
import org.webrtc.PeerConnection.IceServer;
import org.webrtc.PeerConnection.Observer;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RTCStatsCollectorCallback;
import org.webrtc.RtpCapabilities;
import org.webrtc.RtpReceiver;
import org.webrtc.RtpTransceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SoftwareVideoDecoderFactory;
import org.webrtc.SoftwareVideoEncoderFactory;
import org.webrtc.audio.AudioDeviceModule;
import org.webrtc.audio.JavaAudioDeviceModule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * MindSDK class is the entry point of Mind Android SDK. It contains static methods for {@link MindSDK#join(String,
 * String, SessionOptions) joining} and {@link MindSDK#exit2(Session) leaving} conferences, for getting {@link
 * MindSDK#getDeviceRegistry() device registry}, and for {@link MindSDK#createMediaStream(MediaStreamAudioSupplier,
 * MediaStreamVideoSupplier) creating local media streams}. But before you can do all this, the SDK should be
 * initialized:
 *
 * <pre>
 * public class MyApplication extends Application {
 *
 *     &#064;Override
 *     public void onCreate() {
 *         super.onCreate();
 *         MindSDKOptions options = new MindSDKOptions(this);
 *         MindSDK.initialize(options);
 *         // Initialization of Mind SDK is completed, now you can participate in conferences.
 *     }
 *
 * }
 * </pre>
 */
@MainThread
public class MindSDK {

    private static MindSDKOptions options;
    private static AudioDeviceModule audioDeviceModule;
    private static EglBase eglBase;
    private static EglBase.Context eglBaseContext;
    private static PeerConnectionFactory peerConnectionFactory;
    private static DeviceRegistry deviceRegistry;
    private static List<RtpCapabilities.CodecCapability> audioCodecs;
    private static List<RtpCapabilities.CodecCapability> videoCodecs;

    private MindSDK() {}

    /**
     * Initializes Mind Android SDK with the specified {@link MindSDKOptions configuration options}. The initialization
     * should be completed only once before calling any other method of the MindSDK class.
     *
     * @param options The configuration options for Mind Android SDK.
     */
    public static void initialize(MindSDKOptions options) {
        MindSDK.options = new MindSDKOptions(options);
        if (audioDeviceModule == null) {
            audioDeviceModule = JavaAudioDeviceModule.builder(options.getApplication()).createAudioDeviceModule();
        }
        if (eglBase == null) {
            eglBase = EglBase.create();
            eglBaseContext = eglBase.getEglBaseContext();
        }
        if (peerConnectionFactory == null) {
            PeerConnectionFactory.initialize(PeerConnectionFactory.InitializationOptions.builder(options.getApplication()).setFieldTrials("WebRTC-LegacySimulcastLayerLimit/Disabled/").createInitializationOptions());
            PeerConnectionFactory.Options peerConnectionFactoryOptions = new PeerConnectionFactory.Options();
            peerConnectionFactoryOptions.networkIgnoreMask |= PeerConnectionFactory.Options.ADAPTER_TYPE_LOOPBACK;
            peerConnectionFactory = PeerConnectionFactory.builder().setOptions(peerConnectionFactoryOptions)
                                                                   .setAudioDeviceModule(audioDeviceModule)
                                                                   .setVideoEncoderFactory(new SoftwareVideoEncoderFactory())
                                                                   .setVideoDecoderFactory(new SoftwareVideoDecoderFactory())
                                                                   .createPeerConnectionFactory();
        }
        if (audioCodecs == null) {
            audioCodecs = new ArrayList<>();
            for (RtpCapabilities.CodecCapability audioCodec : peerConnectionFactory.getRtpSenderCapabilities(MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO).codecs) {
                if (!audioCodec.mimeType.matches("audio/(G722|ILBC|PCMU|PCMA|CN|telephone-event)")) {
                    // WebRTC library doesn't activate RED for Opus by default, that's why we have to activate it
                    // explicitly through the `setCodecPreferences` (see `WebRtcConnection` class) by changing the
                    // order of codecs such that the RED payload appears before the Opus payload in the offer SDP.
                    if (audioCodec.mimeType.equals("audio/red")) {
                        audioCodecs.add(0, audioCodec);
                    } else {
                        audioCodecs.add(audioCodec);
                    }
                }
            }
        }
        if (videoCodecs == null) {
            videoCodecs = new ArrayList<>();
            for (RtpCapabilities.CodecCapability videoCodec : peerConnectionFactory.getRtpSenderCapabilities(MediaStreamTrack.MediaType.MEDIA_TYPE_VIDEO).codecs) {
                if (!videoCodec.mimeType.matches("video/(H264|AV1)")) {
                    videoCodecs.add(videoCodec);
                }
            }
        }
        if (deviceRegistry == null) {
            deviceRegistry = new DeviceRegistry(options.getApplication(), eglBaseContext, peerConnectionFactory);
        }
    }

    /**
     * @deprecated Use {@link #initialize(MindSDKOptions)} instead.
     */
    @Deprecated
    public static void initialize(Application application) {
        Log.w("MindSDK", MindSDK.class.getSimpleName() + ".initialize(Application) is deprecated and will be removed in the Mind Android SDK 6.0.0");
        MindSDK.initialize(new MindSDKOptions(application));
    }

    /**
     * Returns the {@link DeviceRegistry device registry} which provides access to all audio and video peripherals of
     * the Android device.
     *
     * @return The device registry.
     */
    public static DeviceRegistry getDeviceRegistry() {
        return deviceRegistry;
    }

    /**
     * @deprecated Use {@link DeviceRegistry#getMicrophone()} instead.
     */
    @Deprecated
    public static Microphone getMicrophone() {
        Log.w("MindSDK", MindSDK.class.getSimpleName() + ".getMicrophone() is deprecated and will be removed in the Mind Android SDK 6.0.0");
        return deviceRegistry.getMicrophone();
    }

    /**
     * @deprecated Use {@link DeviceRegistry#getCamera()} instead.
     */
    @Deprecated
    public static Camera getCamera() {
        Log.w("MindSDK", MindSDK.class.getSimpleName() + ".getCamera() is deprecated and will be removed in the Mind Android SDK 6.0.0");
        return deviceRegistry.getCamera();
    }

    /**
     * Creates {@link MediaStream local media stream} with audio and video from the specified suppliers. The `null`
     * value can be passed instead of one of the suppliers to create audio-only or video-only media stream. Even if
     * audio/video supplier wasn't `null` it doesn't mean that the result media stream would automatically contain
     * audio/video, e.g. {@link Microphone} (as a supplier of audio) and {@link Camera} (as a supplier of video) supply
     * no audio and no video, respectively, till they are acquired, and after they were released.
     *
     * @param audioSupplier The audio supplier or `null` to create video-only media stream.
     * @param videoSupplier The video supplier or `null` to create audio-only media stream.
     *
     * @return The created media stream.
     */
    public static MediaStream createMediaStream(MediaStreamAudioSupplier audioSupplier, MediaStreamVideoSupplier videoSupplier) {
        if (audioSupplier == null && videoSupplier == null) {
            throw new IllegalArgumentException("Can't create MediaStream of `" + audioSupplier + "` audio supplier and `" + videoSupplier + "` video supplier");
        }
        return new MediaStream("local", audioSupplier, videoSupplier);
    }

    /**
     * Establishes a participation session (aka joins the conference) on behalf of the participant with the specified
     * token. The establishment is an asynchronous operation, that's why this method returns a {@link CompletableFuture}
     * that completes (on the main thread) with either a {@link Session participation session} (if the operation
     * succeeded) or an exception (if the operation failed).
     *
     * @param uri The URI of the conference.
     * @param token The token of the participant on behalf of whom we are joining the conference.
     * @param options The configuration options for the participation session.
     *
     * @return The completable future that completes (on the main thread) with either a participation session or an
     *         exception.
     */
    public static CompletableFuture<Session> join(String uri, String token, SessionOptions options) {
        if (uri == null || !uri.matches("^https?://[^/]+/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/?$")) {
            throw new IllegalArgumentException("Conference URI is malformed");
        }
        return new Session(uri, token, null, options).open();
    }

    /**
     * @deprecated Use {@link MindSDK#join(String, String, SessionOptions)} instead.
     */
    @Deprecated
    public static CompletableFuture<Session> join2(String uri, String token, SessionListener listener) {
        Log.w("MindSDK", MindSDK.class.getSimpleName() + ".join2(String, String, SessionListener) is deprecated and will be removed in the Mind Android SDK 6.0.0");
        if (uri == null || !uri.matches("^https?://[^/]+/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/?$")) {
            throw new IllegalArgumentException("Conference URI is malformed");
        }
        SessionOptions options = new SessionOptions();
        options.setUseVp9ForSendingVideo(null);
        return new Session(uri, token, listener, options).open();
    }

    /**
     * Terminates an {@link MindSDK#join(String, String, SessionOptions) established participation session} (aka
     * leaves the conference). The termination is an idempotent synchronous operation. The session object itself and
     * all other objects related to the session are not functional after the leaving.
     *
     * @param session The participation session which should be terminated.
     */
    public static void exit2(Session session) {
        if (session != null) {
            session.close();
        }
    }

    static List<RtpCapabilities.CodecCapability> getAudioCodecs() {
        return Collections.unmodifiableList(audioCodecs);
    }

    static List<RtpCapabilities.CodecCapability> getVideoCodecs() {
        return Collections.unmodifiableList(videoCodecs);
    }

    static MindSDKOptions getOptions() {
        return options;
    }

    static EglBase.Context getEglBaseContext() {
        return eglBaseContext;
    }

    public static class PeerConnection implements Observer {

        private final org.webrtc.PeerConnection pc;

        public PeerConnection(List<IceServer> iceServers) {
            org.webrtc.PeerConnection.RTCConfiguration configuration = new org.webrtc.PeerConnection.RTCConfiguration(iceServers);
            configuration.sdpSemantics = org.webrtc.PeerConnection.SdpSemantics.UNIFIED_PLAN;
            this.pc = peerConnectionFactory.createPeerConnection(configuration, this);
        }

        public SessionDescription getLocalDescription() {
            return pc.getLocalDescription();
        }

        public SessionDescription getRemoteDescription() {
            return pc.getRemoteDescription();
        }

        public DataChannel createDataChannel(String label, DataChannel.Init init, DataChannel.Observer observer) {
            DataChannel dataChannel = pc.createDataChannel(label, init);
            dataChannel.registerObserver(observer);
            return dataChannel;
        }

        public void createOffer(SdpObserver observer, MediaConstraints constraints) {
            pc.createOffer(observer, constraints);
        }

        public void createAnswer(SdpObserver observer, MediaConstraints constraints) {
            pc.createAnswer(observer, constraints);
        }

        public void setLocalDescription(SdpObserver observer, SessionDescription sdp) {
            pc.setLocalDescription(observer, sdp);
        }

        public void setRemoteDescription(SdpObserver observer, SessionDescription sdp) {
            pc.setRemoteDescription(observer, sdp);
        }

        public boolean addIceCandidate(IceCandidate candidate) {
            return pc.addIceCandidate(candidate);
        }

        public boolean removeIceCandidates(IceCandidate[] candidates) {
            return pc.removeIceCandidates(candidates);
        }

        public List<RtpTransceiver> getTransceivers() {
            return pc.getTransceivers();
        }

        public RtpTransceiver addTransceiver(MediaStreamTrack track) {
            return pc.addTransceiver(track);
        }

        public RtpTransceiver addTransceiver(MediaStreamTrack track, RtpTransceiver.RtpTransceiverInit init) {
            return pc.addTransceiver(track, init);
        }

        public RtpTransceiver addTransceiver(MediaStreamTrack.MediaType mediaType) {
            return pc.addTransceiver(mediaType);
        }

        public RtpTransceiver addTransceiver(MediaStreamTrack.MediaType mediaType, RtpTransceiver.RtpTransceiverInit init) {
            return pc.addTransceiver(mediaType, init);
        }

        public void getStats(RTCStatsCollectorCallback callback) {
            pc.getStats(callback);
        }

        public org.webrtc.PeerConnection.SignalingState signalingState() {
            return pc.signalingState();
        }

        public org.webrtc.PeerConnection.IceConnectionState iceConnectionState() {
            return pc.iceConnectionState();
        }

        public org.webrtc.PeerConnection.PeerConnectionState connectionState() {
            return pc.connectionState();
        }

        public org.webrtc.PeerConnection.IceGatheringState iceGatheringState() {
            return pc.iceGatheringState();
        }

        public void close() {
            pc.close();
        }

        public void dispose() {
            pc.dispose();
        }

        @Override
        public void onSignalingChange(org.webrtc.PeerConnection.SignalingState signalingState) {}

        @Override
        public void onIceConnectionChange(org.webrtc.PeerConnection.IceConnectionState iceConnectionState) {}

        @Override
        public void onIceConnectionReceivingChange(boolean receiving) {}

        @Override
        public void onIceGatheringChange(org.webrtc.PeerConnection.IceGatheringState iceGatheringState) {}

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {}

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {}

        @Override
        public void onAddStream(org.webrtc.MediaStream mediaStream) {}

        @Override
        public void onRemoveStream(org.webrtc.MediaStream mediaStream) {}

        @Override
        public void onDataChannel(DataChannel dataChannel) {}

        @Override
        public void onRenegotiationNeeded() {}

        @Override
        public void onAddTrack(RtpReceiver rtpReceiver, org.webrtc.MediaStream[] mediaStreams) {}

    }

}
