package com.mind.api.sdk;

import androidx.annotation.MainThread;
import java9.util.concurrent.CompletableFuture;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

/**
 * Participant class is used for representing remote participants (each instance represents a single remote
 * participants). The participant on behalf of whom Android-application is participating in the conference (aka the
 * local participant) is represented with {@link Me} class. You can get representations of all remote participants and
 * a representation of a specific remote participant with {@link Conference#getParticipants() getParticipants} and
 * {@link Conference#getParticipantById(String) getParticipantById} methods of {@link Conference} class, respectively.
 * Participant class contains methods for getting and setting parameters of the remote participant and for getting its
 * primary and secondary media streams:
 *
 * <pre>
 * Video participantVideo = findViewById(R.id.primaryVideo);
 * Video participantSecondaryVideo = findViewById(R.id.secondaryVideo);
 *
 * Participant participant = conference.getParticipantById("&lt;PARTICIPANT_ID&gt;");
 *
 * MediaStream participantStream = participant.getMediaStream();
 * participantVideo.setMediaStream(participantStream);
 *
 * MediaStream participantSecondaryStream = participant.getSecondaryMediaStream();
 * participantSecondaryVideo.setMediaStream(participantSecondaryStream);
 * </pre>
 */
@MainThread
public class Participant {

    static Participant fromDTO(Session session, JSONObject dto) throws JSONException {
        JSONObject media = dto.getJSONObject("media");
        JSONObject secondaryMedia = dto.getJSONObject("secondaryMedia");
        return new Participant(session, dto.getString("id"), dto.getString("name"), dto.getDouble("priority"), ParticipantRole.fromString(dto.getString("role")), media.getBoolean("audio"), media.getBoolean("video"), secondaryMedia.getBoolean("audio"), secondaryMedia.getBoolean("video"));
    }

    // Thread-Safety: all methods of `session` except `executeOnMainThread` are accessed on the main thread only
    final Session session;

    private final String id;
    String name;
    double priority;
    ParticipantRole role;
    private final MediaStream mediaStream;
    private final MediaStream secondaryMediaStream;

    Participant(Session session, String id, String name, Double priority, ParticipantRole role, boolean audio, boolean video, boolean secondaryAudio, boolean secondaryVideo) {
        this.session = session;
        this.id = id;
        this.name = name;
        this.priority = priority;
        this.role = role;
        this.mediaStream = new MediaStream(id + "#primary", audio ? session.getWebRtcConnection() : null, video ? session.getWebRtcConnection() : null);
        this.secondaryMediaStream = new MediaStream(id + "#secondary", secondaryAudio ? session.getWebRtcConnection() : null, secondaryVideo ? session.getWebRtcConnection() : null);
    }

    /**
     * Return the ID of the remote participant. The ID is unique and never changes.
     *
     * @return The ID of the remote participant.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the current name of the remote participant. The name of the remote participant can be shown above his
     * video in the conference media stream and recording.
     *
     * @return The current name of the remote participant.
     */
    public String getName() {
        return name;
    }

    /**
     * Changes the name of the remote participant. The name of the remote participant can be shown above his video in
     * the conference media stream and recording. The name changing is an asynchronous operation, that's why this
     * method returns a {@link CompletableFuture} that completes (on the main thread) with either no value (if the
     * operation succeeds) or an exception (if the operation fails). The operation can succeed only if the {@link Me
     * local participant} plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param name The new name for the remote participant.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> setName(String name) {
        try {
            JSONObject requestDTO = new JSONObject().put("name", name);
            return session.newHttpPatch("/participants/" + id, requestDTO).thenAccept((responseDTO) -> {
                Participant.this.name = name;
            });
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    /**
     * Returns the current priority of the remote participant. The priority defines a place which remote participant
     * takes in conference media stream and recording.
     *
     * @return The current priority of the remote participant.
     */
    public double getPriority() {
        return priority;
    }

    /**
     * Changes the priority of the remote participant. The priority defines a place which remote participant takes in
     * conference media stream and recording. The priority changing is an asynchronous operation, that's why this
     * method returns a {@link CompletableFuture} that completes (on the main thread) with either no value (if the
     * operation succeeds) or an exception (if the operation fails). The operation can succeed only if the {@link Me
     * local participant} plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param priority The new priority for the remote participant.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> setPriority(double priority) {
        try {
            JSONObject requestDTO = new JSONObject().put("priority", priority);
            return session.newHttpPatch("/participants/" + id, requestDTO).thenAccept((responseDTO) -> {
                Participant.this.priority = priority;
            });
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    /**
     * Returns the current {@link ParticipantRole role} of the remote participant. The role defines a set of
     * permissions which the remote participant is granted.
     *
     * @return The current role of the remote participant.
     */
    public ParticipantRole getRole() {
        return role;
    }

    /**
     * Changes the {@link ParticipantRole role} of the remote participant. The role defines a set of permissions which
     * the remote participant is granted. The role changing is an asynchronous operation, that's why this method
     * returns a {@link CompletableFuture} that completes (on the main thread) with either no value (if the operation
     * succeeds) or an exception (if the operation fails). The operation can succeed only if the {@link Me local
     * participant} plays a role of a {@link ParticipantRole#MODERATOR moderator}.
     *
     * @param role The new role for the remote participant.
     *
     * @return The completable future that completes (on the main thread) with either no value or an exception.
     */
    public CompletableFuture<Void> setRole(ParticipantRole role) {
        try {
            JSONObject requestDTO = new JSONObject().put("role", role);
            return session.newHttpPatch("/participants/" + id, requestDTO).thenAccept((responseDTO) -> {
                Participant.this.role = role;
            });
        } catch (JSONException exception) {
            return CompletableFuture.failedFuture(exception);
        }
    }

    /**
     * Checks whether the remote participant is streaming primary audio (i.e. audio taken from his microphone). If both
     * this method and {@link Participant#isStreamingVideo() isStreamingVideo} return `false` then the participant is
     * not streaming the primary media stream at all.
     *
     * @return The boolean value which indicates if the remote participant is streaming primary audio or not.
     */
    public boolean isStreamingAudio() {
        return mediaStream.hasAudioSupplier();
    }

    /**
     * Checks whether the remote participant is streaming primary video (i.e. video taken from his camera). If both
     * this method and {@link Participant#isStreamingAudio() isStreamingAudio} return `false` then the participant is
     * not streaming the primary media stream at all.
     *
     * @return The boolean value which indicates if the remote participant is streaming primary video or not.
     */
    public boolean isStreamingVideo() {
        return mediaStream.hasVideoSupplier();
    }

    /**
     * Returns the {@link MediaStream primary media stream} of the remote participant. The primary media stream is
     * intended for streaming video and audio taken from a camera and a microphone of the participant's Android device,
     * respectively. You can get and play the primary media stream at any moment regardless of whether the participant
     * is streaming its primary video/audio or not: if the participant started or stopped streaming its primary video
     * or/and audio, the returned media stream would be updated automatically.
     *
     * @return The primary media stream of the remote participant.
     */
    public MediaStream getMediaStream() {
        return mediaStream;
    }

    /**
     * Checks whether the remote participant is streaming secondary audio (i.e. an arbitrary content with audio). If
     * both this method and {@link Participant#isStreamingSecondaryVideo() isStreamingSecondaryVideo} return `false`
     * then the participant is not streaming secondary media stream at all.
     *
     * @return The boolean value which indicates if the remote participant is streaming secondary audio or not.
     */
    public boolean isStreamingSecondaryAudio() {
        return secondaryMediaStream.hasAudioSupplier();
    }

    /**
     * Checks whether the remote participant is streaming secondary video (i.e. an arbitrary content with video). If
     * both this method and {@link Participant#isStreamingSecondaryAudio() isStreamingSecondaryAudio} return `false`
     * then the participant is not streaming secondary media stream at all.
     *
     * @return The boolean value which indicates if the remote participant is streaming secondary video or not.
     */
    public boolean isStreamingSecondaryVideo() {
        return secondaryMediaStream.hasVideoSupplier();
    }

    /**
     * Returns the {@link MediaStream secondary media stream} of the remote participant. The secondary media stream is
     * intended for streaming an arbitrary audio/video content, e.g. for sharing a screen of the participant's Android
     * device. You can get and play the secondary media stream at any moment regardless of whether the participant is
     * streaming its secondary video/audio or not: if the participant started or stopped streaming its secondary video
     * or/and audio, the returned media stream would be updated automatically.
     *
     * @return The secondary media stream of the remote participant.
     */
    public MediaStream getSecondaryMediaStream() {
        return secondaryMediaStream;
    }

    void update(JSONObject dto) throws JSONException {
        String name = dto.getString("name");
        if (!Objects.equals(this.name, name)) {
            this.name = name;
            session.fireOnParticipantNameChanged(this);
        }
        double priority = dto.getDouble("priority");
        if (this.priority != priority) {
            this.priority = priority;
            session.fireOnParticipantPriorityChanged(this);
        }
        ParticipantRole role = ParticipantRole.fromString(dto.getString("role"));
        if (this.role != role) {
            this.role = role;
            session.fireOnParticipantRoleChanged(this);
        }
        JSONObject media = dto.getJSONObject("media");
        if (mediaStream.hasAudioSupplier() != media.getBoolean("audio") || mediaStream.hasVideoSupplier() != media.getBoolean("video")) {
            mediaStream.setAudioSupplier(media.getBoolean("audio") ? session.getWebRtcConnection() : null);
            mediaStream.setVideoSupplier(media.getBoolean("video") ? session.getWebRtcConnection() : null);
            session.fireOnParticipantMediaChanged(this);
        }
        JSONObject secondaryMedia = dto.getJSONObject("secondaryMedia");
        if (secondaryMediaStream.hasAudioSupplier() != secondaryMedia.getBoolean("audio") || secondaryMediaStream.hasVideoSupplier() != secondaryMedia.getBoolean("video")) {
            secondaryMediaStream.setAudioSupplier(secondaryMedia.getBoolean("audio") ? session.getWebRtcConnection() : null);
            secondaryMediaStream.setVideoSupplier(secondaryMedia.getBoolean("video") ? session.getWebRtcConnection() : null);
            session.fireOnParticipantSecondaryMediaChanged(this);
        }
    }

    void destroy() {
        mediaStream.setAudioSupplier(null);
        mediaStream.setVideoSupplier(null);
        secondaryMediaStream.setAudioSupplier(null);
        secondaryMediaStream.setVideoSupplier(null);
    }

}
