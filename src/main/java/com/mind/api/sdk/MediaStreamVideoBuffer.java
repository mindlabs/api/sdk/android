package com.mind.api.sdk;

import org.webrtc.VideoTrack;

class MediaStreamVideoBuffer {

    private final VideoTrack track;
    private final boolean remote;
    private final int width;
    private final int height;
    private final int bitrate;
    private final int adaptivity;
    private final double scale;

    MediaStreamVideoBuffer(VideoTrack track, boolean remote, int width, int height, int bitrate, int adaptivity, double scale) {
        this.track = track;
        this.remote = remote;
        this.width = width;
        this.height = height;
        this.bitrate = bitrate;
        this.adaptivity = adaptivity;
        this.scale = scale;
    }

    VideoTrack getTrack() {
        return track;
    }

    boolean isRemote() {
        return remote;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }

    int getBitrate() {
        return bitrate;
    }

    int getAdaptivity() {
        return adaptivity;
    }

    double getScale() {
        return scale;
    }

}
