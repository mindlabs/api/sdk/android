package com.mind.api.sdk;

import android.app.Application;
import androidx.annotation.MainThread;
import org.webrtc.EglBase;
import org.webrtc.PeerConnectionFactory;

/**
 * DeviceRegistry class provides access to all audio and video peripherals of the Android device. It contains methods
 * for getting the {@link DeviceRegistry#getCamera() camera} and the {@link DeviceRegistry#getMicrophone() microphone}.
 */
@MainThread
public class DeviceRegistry {

    private final Microphone microphone;
    private final Camera camera;

    DeviceRegistry(Application application, EglBase.Context eglBaseContext, PeerConnectionFactory peerConnectionFactory) {
        this.microphone = new Microphone(application, peerConnectionFactory);
        this.camera = new Camera(application, peerConnectionFactory, eglBaseContext);
    }

    /**
     * Returns the {@link Microphone microphone} of the Android device.
     *
     * @return The microphone of the Android device.
     */
    public Microphone getMicrophone() {
        return microphone;
    }

    /**
     * Returns the {@link Camera camera} of the Android device.
     *
     * @return The camera of the Android device.
     */
    public Camera getCamera() {
        return camera;
    }

}
