package com.mind.api.sdk;

import androidx.annotation.AnyThread;

/**
 * MediaStreamVideoStatistics class is used for representing video statistics of a {@link MediaStream media stream}.
 * The statistics consists of instant measures of the network connection which is currently used for transmitting the
 * video. If no video is transmitted at the moment or if the media stream does not have a video at all or yet, the
 * values of all measures are reset to zeros, otherwise the values of all measures are updated about once a second. You
 * can always get the latest video statistics for any media stream with a help of
 * {@link MediaStream#getVideoStatistics() getVideoStatistics} method:
 *
 * <pre>
 * // We assume that microphone and camera have been already acquired
 * MediaStream myStream = MindSDK.createMediaStream(microphone, camera);
 * me.setMediaStream(myStream);
 * MediaStreamVideoStatistics myStreamVideoStatistics = myStream.getVideoStatistics();
 *
 * ...
 *
 * Video participantVideo = findViewById(R.id.participantVideo);
 * Participant participant = conference.getParticipantById("&lt;PARTICIPANT_ID&gt;");
 * if (participant != null) {
 *     MediaStream participantStream = participant.getMediaStream();
 *     participantVideo.setMediaStream(participantStream);
 *     MediaStreamVideoStatistics participantStreamVideoStatistics = participantStream.getVideoStatistics();
 * }
 *
 * ...
 *
 * Video conferenceVideo = findViewById(R.id.conferenceVideo);
 * MediaStream conferenceStream = conference.getMediaStream();
 * conferenceVideo.setMediaStream(conferenceStream);
 * MediaStreamVideoStatistics conferenceStreamVideoStatistics = conferenceStream.getVideoStatistics();
 * </pre>
 */
@AnyThread
public class MediaStreamVideoStatistics {

    private final long timestamp;
    private final int rtt;
    private final int bitrate;
    private final int delay;
    private final int losses;

    private final int width;
    private final int height;
    private final int rate;

    MediaStreamVideoStatistics(int rtt, double bitrate, double delay, double losses, int width, int height, double rate) {
        this.timestamp = System.currentTimeMillis();
        this.rtt = rtt;
        this.bitrate = integerify(bitrate);
        this.delay = integerify(delay);
        this.losses = integerify(losses);
        this.width = width;
        this.height = height;
        this.rate = integerify(rate);
    }

    /**
     * Returns the creation timestamp of the statistics. The creation timestamp of the statistics is the number of
     * milliseconds that have elapsed between 1 January 1970 00:00:00 UTC and the time at which the statistics was
     * created.
     *
     * @return The creation timestamp of the statistics.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the instant round-trip time of the video transmission. The instant round-trip time of the video
     * transmission is an average sum of transmission delays (in milliseconds) of the network connection in both
     * directions (from the Android device to Mind API and vice versa) during the latest observed second.
     *
     * @return The instant round-trip time of the video transmission.
     */
    public int getRtt() {
        return rtt;
    }

    /**
     * Returns the instant bitrate of the video transmission. The instant bitrate of the video transmission is a size
     * (in bits) of all video packets that have been transmitted over the network connection during the latest observed
     * second.
     *
     * @return The instant bitrate of the video transmission.
     */
    public int getBitrate() {
        return bitrate;
    }

    /**
     * Returns the instant delay of the video transmission. The instant delay of the video transmission is an average
     * number of milliseconds that video frames have spent buffered locally (on the Android device) during the latest
     * observed second before (in case of sending a local media stream) or after (in case of receiving a remote media
     * stream) being transmitted over the network connection.
     *
     * @return The instant delay of the video transmission.
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Returns the instant loss rate of the video transmission. The instant loss rate of the video transmission is a
     * percentage (i.e. an integer between 0 and 100 inclusively) of video packets that have been sent over the network
     * connection during the latest observed second but have not been received (i.e. have been lost).
     *
     * @return The instant loss rate of the video transmission.
     */
    public int getLosses() {
        return losses;
    }

    /**
     * Returns the instant frame width of the video transmission. The instant frame width of the video transmission is
     * a width (in pixels) of the last frame that have been successfully transmitted over the network connection during
     * the latest observed second.
     *
     * @return The instant frame width of the video transmission.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the instant frame height of the video transmission. The instant frame height of the video transmission
     * is a height (in pixels) of the last frame that have been successfully transmitted over the network connection
     * during the latest observed second.
     *
     * @return The instant frame height of the video transmission.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the instant frame rate of the video transmission. The instant frame rate of the video transmission is
     * a number of video frames that have been successfully transmitted over the network connection during the latest
     * observed second.
     *
     * @return The instant frame rate of the video transmission.
     */
    public int getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  timestamp: " + timestamp + ",\n" +
                "  rtt: " + rtt + "ms,\n" +
                "  bitrate: " + (bitrate / 1024) + "kbit/s,\n" +
                "  delay: " + delay + "ms,\n" +
                "  losses: " + losses + "%,\n" +
                "  width: " + width + "px,\n" +
                "  height: " + height +  "px,\n" +
                "  rate: " + rate + "fps\n" +
                "}";
    }

    private static int integerify(double value) {
        return Double.isNaN(value) || Double.isInfinite(value) ? 0 : (int) value;
    }

}