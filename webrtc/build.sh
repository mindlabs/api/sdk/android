#!/bin/bash

set -e

if [ "$(lsb_release -sci)" != $'Debian\nbookworm' ]; then
    echo "WebRTC library can be built on Debian Bookworm only" 1>&2
    exit 1
fi

WEBRTC_DIRECTORY="$PWD/$(dirname "$0")"

mkdir -p build/webrtc_android
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git build/depot_tools
export PATH="$PWD/build/depot_tools:$PATH"
cd build/webrtc_android
fetch --nohooks webrtc_android
cd src
git checkout branch-heads/6367 # Which corresponds to Chromium 124 <https://chromiumdash.appspot.com/branches>
gclient sync -D
./build/install-build-deps.sh --android
# Make `PeerConnectionFactory` class load native WebRTC library from `libwebrtc.so` file.
sed -Ei 's/jingle_peerconnection_so/webrtc/' sdk/android/api/org/webrtc/PeerConnectionFactory.java
# Add support of `scalabilityMode` for VP9 SVC.
patch -p1 << "EOF"
diff --git a/api/video_codecs/video_encoder_factory.h b/api/video_codecs/video_encoder_factory.h
index d28a2a4035..236aeea491 100644
--- a/api/video_codecs/video_encoder_factory.h
+++ b/api/video_codecs/video_encoder_factory.h
@@ -86,12 +86,9 @@ class VideoEncoderFactory {
       const SdpVideoFormat& format,
       absl::optional<std::string> scalability_mode) const {
     // Default implementation, query for supported formats and check if the
-    // specified format is supported. Returns false if scalability_mode is
-    // specified.
+    // specified format is supported.
     CodecSupport codec_support;
-    if (!scalability_mode) {
-      codec_support.is_supported = format.IsCodecInList(GetSupportedFormats());
-    }
+    codec_support.is_supported = format.IsCodecInList(GetSupportedFormats());
     return codec_support;
   }
 
diff --git a/sdk/android/api/org/webrtc/RtpParameters.java b/sdk/android/api/org/webrtc/RtpParameters.java
index 9ca8311610..44a1275fce 100644
--- a/sdk/android/api/org/webrtc/RtpParameters.java
+++ b/sdk/android/api/org/webrtc/RtpParameters.java
@@ -76,6 +76,9 @@ public class RtpParameters {
     // If non-null, scale the width and height down by this factor for video. If null,
     // implementation default scaling factor will be used.
     @Nullable public Double scaleResolutionDownBy;
+    // If non-null, identifier of the scalability mode for video. If null,
+    // implementation default scalability mode will be used.
+    @Nullable public String scalabilityMode;
     // SSRC to be used by this encoding.
     // Can't be changed between getParameters/setParameters.
     public Long ssrc;
@@ -93,7 +96,7 @@ public class RtpParameters {
     @CalledByNative("Encoding")
     Encoding(String rid, boolean active, double bitratePriority, @Priority int networkPriority,
         Integer maxBitrateBps, Integer minBitrateBps, Integer maxFramerate,
-        Integer numTemporalLayers, Double scaleResolutionDownBy, Long ssrc,
+        Integer numTemporalLayers, Double scaleResolutionDownBy, String scalabilityMode, Long ssrc,
         boolean adaptiveAudioPacketTime) {
       this.rid = rid;
       this.active = active;
@@ -104,6 +107,7 @@ public class RtpParameters {
       this.maxFramerate = maxFramerate;
       this.numTemporalLayers = numTemporalLayers;
       this.scaleResolutionDownBy = scaleResolutionDownBy;
+      this.scalabilityMode = scalabilityMode;
       this.ssrc = ssrc;
       this.adaptiveAudioPacketTime = adaptiveAudioPacketTime;
     }
@@ -160,6 +164,12 @@ public class RtpParameters {
       return scaleResolutionDownBy;
     }
 
+    @Nullable
+    @CalledByNative("Encoding")
+    String getScalabilityMode() {
+      return scalabilityMode;
+    }
+
     @CalledByNative("Encoding")
     Long getSsrc() {
       return ssrc;
diff --git a/sdk/android/src/jni/pc/rtp_parameters.cc b/sdk/android/src/jni/pc/rtp_parameters.cc
index 4bd9ee0e1d..391560c19b 100644
--- a/sdk/android/src/jni/pc/rtp_parameters.cc
+++ b/sdk/android/src/jni/pc/rtp_parameters.cc
@@ -53,6 +53,7 @@ ScopedJavaLocalRef<jobject> NativeToJavaRtpEncodingParameter(
       NativeToJavaInteger(env, encoding.max_framerate),
       NativeToJavaInteger(env, encoding.num_temporal_layers),
       NativeToJavaDouble(env, encoding.scale_resolution_down_by),
+      NativeToJavaString(env, encoding.scalability_mode),
       encoding.ssrc ? NativeToJavaLong(env, *encoding.ssrc) : nullptr,
       encoding.adaptive_ptime);
 }
@@ -116,6 +117,12 @@ RtpEncodingParameters JavaToNativeRtpEncodingParameters(
       Java_Encoding_getScaleResolutionDownBy(jni, j_encoding_parameters);
   encoding.scale_resolution_down_by =
       JavaToNativeOptionalDouble(jni, j_scale_resolution_down_by);
+  ScopedJavaLocalRef<jstring> j_scalability_mode =
+      Java_Encoding_getScalabilityMode(jni, j_encoding_parameters);
+  if (!IsNull(jni, j_scalability_mode)) {
+    encoding.scalability_mode =
+        JavaToNativeString(jni, j_scalability_mode);
+  }
   encoding.adaptive_ptime =
       Java_Encoding_getAdaptivePTime(jni, j_encoding_parameters);
   ScopedJavaLocalRef<jobject> j_ssrc =
diff --git a/sdk/android/src/jni/video_encoder_factory_wrapper.cc b/sdk/android/src/jni/video_encoder_factory_wrapper.cc
index 7df129b360..147469bdcf 100644
--- a/sdk/android/src/jni/video_encoder_factory_wrapper.cc
+++ b/sdk/android/src/jni/video_encoder_factory_wrapper.cc
@@ -85,6 +85,13 @@ VideoEncoderFactoryWrapper::VideoEncoderFactoryWrapper(
       Java_VideoEncoderFactory_getSupportedCodecs(jni, encoder_factory);
   supported_formats_ = JavaToNativeVector<SdpVideoFormat>(
       jni, j_supported_codecs, &VideoCodecInfoToSdpVideoFormat);
+  for (webrtc::SdpVideoFormat& format : supported_formats_) {
+    if (format.name == "VP9") {
+      for (const auto scalability_mode : kAllScalabilityModes) {
+          format.scalability_modes.push_back(scalability_mode);
+      }
+    }
+  }
   const ScopedJavaLocalRef<jobjectArray> j_implementations =
       Java_VideoEncoderFactory_getImplementations(jni, encoder_factory);
   implementations_ = JavaToNativeVector<SdpVideoFormat>(
EOF
./tools_webrtc/android/build_aar.py
unzip libwebrtc.aar
cp classes.jar "$WEBRTC_DIRECTORY/libwebrtc.jar"
cp jni/armeabi-v7a/libjingle_peerconnection_so.so "$WEBRTC_DIRECTORY/jni/armeabi-v7a/libwebrtc.so"
cp jni/arm64-v8a/libjingle_peerconnection_so.so "$WEBRTC_DIRECTORY/jni/arm64-v8a/libwebrtc.so"
cp jni/x86/libjingle_peerconnection_so.so "$WEBRTC_DIRECTORY/jni/x86/libwebrtc.so"
cp jni/x86_64/libjingle_peerconnection_so.so "$WEBRTC_DIRECTORY/jni/x86_64/libwebrtc.so"
rm -rf "$WEBRTC_DIRECTORY/build"
